<!doctype html>
<html lang="sp">
<head>
	<meta charset="UTF-8">
	<title>..: Registrar Estados Dentales :..</title>
</head>
<body>
	<?php
		require("../controller/ControllerOxmar.php");
		
		$marcasDAO = new ControllerOxmar();

		if($marcasDAO->fn_registraMarcas()) {
			echo "<div class='alertaCorrecto'>Estados dentales registrados</div>";
		} else {
			echo "<div class='alertaIncorrecto'>Error al tratar de registrar los estados dentales.</div>";
		}
	?>
</body>
</html>