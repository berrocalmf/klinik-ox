<?php 
	require_once("../connection/Conexion.php");
	require_once("../controller/ControllerOxmar.php");
	require_once("../controller/ControllerAte.php");
	require_once("../controller/ControllerPpto.php");

	if($_POST['codigoPaciente'] && $_POST['idOdontograma'] && $_POST['idAtencion']) {

			$arrMarcas = [];
			$arrPpo    = [];
			$arrPagos  = [];

			$conexion=new Conexion();
			$conn=$conexion->getConexion();

			$contMarcas = new ControllerOxmar();  	// Controllador de marcas (Mod. 2)
			$contAtenc  = new ControllerAte();		// Controlador del Archivo ATE (Información de Atenciones)
			$contPpto   = new ControllerPpto();     // Controlador del Archivo ppto (Información de presupuesto)

			$odonid = $_POST['idOdontograma'];
			$afiid  = $_POST['codigoPaciente'];	
			$ateid  = $_POST['idAtencion'];

			// Se carga en la RAM la información del Presupuesto del Tratamiento
			$arrPpo = $contAtenc->fn_getPresupuesto($ateid);   // <-- Array contenedor de objeto de tipo Ppto (Presupuesto)
			
	} else {	
			echo "<div class='alertaIncorrecto'> Parámetros IDAFILIADO o IDODONTOGRAMA con valores incorrectos o sin dato. </ div>";
			return;
	}
?>

<div>
	<!-- <header class=""> -->
	<section class="">	
			<h4 style="text-align: left;text-decoration: underline;">Odontograma</h4>

			<!-- Informations sur les Affiliés -->

			<?php 
					$sql    = "SELECT * FROM afi WHERE afiid=$afiid";
		            $query  = $conn->prepare($sql); 
		            $query->execute();  
		            $result = $query->fetchAll();
		          
		            foreach ($result as $row) {
		               $afiliado = $row["docidafiliado"] . " " . $row["papellido"] . " " . $row["sapellido"] . " " . $row["pnombre"] . " " . $row["snombre"]; 
		            }

		            echo "<h6>Paciente: " . $afiliado . "</h6>"; 
			?>

			<button class="btn btn-primary" onclick="prepararImpresion(); javascript:window.print(); terminarImpresion();">Imprimir Odontograma</button>
	</section>		
	<!-- </header> -->
	
	<br>

	<section>
		<!-- 1324 -->
		<section style="width: 1500px;">   
			
			<!-- [ Section pour table de traitement ] -->
			<section id="seccionTablaTratamientos" style="height: 600px;overflow-y: scroll;width: 300px;" class="displayInlineBlockTop sombraFormulario">
			</section>
			
			<!-- [ Section pour dessiner l'odontogramme ] -->
			<section id="seccionDientes" class="displayInlineBlockTop" style="padding: 10px;height: 600px;width: 1011px; background-color: white">
			</section>			
		
		</section>

		<br>
			
		<!-- Fenêtre modale pour l'enregistrement de la marque -->
		<!-- ( Fenêtre modale pour sélectionner des valeurs ) -->

		<div class="modal fade" style="" id="myModal" role="dialog">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">	

					<div class="modal-header">
							  <h6 class="modal-title">Registro de Estados Dentales</h6>
					          <button type="button" class="close" data-dismiss="modal">&times;</button>
			        </div>	

			        <div class="modal-body">

						<!-- SECCION PARA EL FORMULARIO DE TRATAMIENTOS -->
						<section id="seccionRegistrarTratamiento" class="textAlignLeft sombraFormulario" style="background-color: white;padding: 20px;width: auto;">
							
							
							<!-- SECCION PARA LA FIGURA DEL DIENTE GENERAL PARA REALIZAR LAS MARCAS DEL ODONTOGRAMA Y LA CAJA DE TEXTO PARA INDICAR LA CARA SELECCIONADA -->
							<section class="displayInlineBlockMiddle">
								<div class="dienteGeneral" id="dienteGeneral"><div id="C1" onclick="seleccionarCara(this.id);"></div><div id="C2" onclick="seleccionarCara(this.id);"></div><div id="C3" onclick="seleccionarCara(this.id);"></div><div id="C4" onclick="seleccionarCara(this.id);"></div><div id="C5" onclick="seleccionarCara(this.id);"></div><input type="text" id="txtIdentificadorDienteGeneral" name="txtIdentificadorDienteGeneral" value="DXX" readonly="readonly"></div>
							</section>



							<!-- SECCION PARA EL FORMULARIO CON EL CUAL SE CAPTURAN LOS ESTADOS DE UNA PIEZA DENTAL -->
							<section class="displayInlineBlockMiddle">
							
								<form class="formulario sombraFormulario labelPequenio" style="text-align: left;">
									
									<div class="tituloFormulario">Datos del Estado Dental</div>
								
									<div class="contenidoInterno">
										
										<label for="txtDienteTratado">Diente seleccionado:</label>
										<input type="text" id="txtDienteTratado" name="txtDienteTratado" class="textAlignCenter" size="4" readonly="readonly">
										
										<br>
										
										<label for="txtCaraTratada">Cara Seleccionada:</label>
										<input type="text" id="txtCaraTratada" name="txtCaraTratada" class="textAlignCenter" size="4" readonly="readonly">
										
										<br>
										
										<label for="cbxEstado">Marca:</label>
										<select id="cbxEstado" name="cbxEstado">
											<!-- <option value="1-DIENTE INTACTO">DIENTE INTACTO</option>
											<option value="2-DIENTE AUSENTE">DIENTE AUSENTE</option>
											<option value="3-REMANENTE RADICULAR">REMANENTE RADICULAR</option>
											<option value="4-EXTRUSION">EXTRUSION</option>
											<option value="5-INTRUSION">INTRUSION</option>
											<option value="6-GIROVERSION">GIROVERSION</option>
											<option value="7-MIGRASION">MIGRASION</option>
											<option value="8-MICRODONCIA">MICRODONCIA</option>
											<option value="9-MACRODONCIA">MACRODONCIA</option>
											<option value="10-ECTOPICO">ECTOPICO</option>
											<option value="11-TRANSPOSICION">TRANSPOSICION</option>
											<option value="12-CLAVIJA">CLAVIJA</option>
											<option value="13-FRACTURA">FRACTURA</option>
											<option value="14-DIENTE DISCROMICO">DIENTE DISCROMICO</option>
											<option value="15-GEMINACION">GEMINACION</option>
											<option value="16-CARIES">CARIES</option>
											<option value="17-OBTURACION TEMPORAL">OBTURACION TEMPORAL</option>
											<option value="18-AMALGAMA">AMALGAMA</option>
											<option value="19-RESINA">RESINA</option>
											<option value="20-INCRUSTACION">INCRUSTACION</option>
											<option value="21-ENDODONCIA">ENDODONCIA</option>
											<option value="22-DESGASTADO">DESGASTADO</option>
											<option value="23-DIASTEMA">DIASTEMA</option>
											<option value="24-MOVILIDAD">MOVILIDAD</option>
											<option value="25-CORONA TEMPORAL">CORONA TEMPORAL</option>
											<option value="26-CORONA COMPLETA">CORONA COMPLETA</option>
											<option value="27-CORONA VEENER">CORONA VEENER</option>
											<option value="28-CORONA FEXESTRADA">CORONA FEXESTRADA</option>
											<option value="29-CORONA TRES CUARTOS">CORONA TRES CUARTOS</option>
											<option value="30-CORONA PORCELANA">CORONA PORCELANA</option>
											<option value="31-PROTESIS FIJA">PROTESIS FIJA</option>
											<option value="32-PROTESIS REMOVIBLE">PROTESIS REMOVIBLE</option>
											<option value="33-ODONTULO TOTAL">ODONTULO TOTAL</option>
											<option value="34-APARAT. ORTO. FIJO">APARAT. ORTO. FIJO</option>
											<option value="35-APARAT. ORTO. REMOV.">APARAT. ORTO. REMOV.</option>
											<option value="36-IMPLANTE">IMPLANTE</option>
											<option value="37-SUPERNUMERARIO">SUPERNUMERARIO</option>
											<option value="38-DIENTE POR EXTRAER">DIENTE POR EXTRAER</option> -->
											<?php 
												$arrMarcas = $contMarcas->fn_listaMarcasTodas();

												if(sizeof($arrMarcas)>0) {
													foreach($arrMarcas as $key => $value) {
														echo "<option value='".$value->getMarid()."'>".$value->getDescripcion()."</option>";
													}
				    							
												} else {
													echo "<option value='NA'>Sin Marcas</option>";
												}
											?>

										</select>
										
										<!-- <input type="hidden" id="txtCodigoPaciente" name="txtCodigoPaciente" value="PACIENTE0000001"> -->
										
										<!-- Código del Odontograma -->
										<input type="hidden" id="txtCodigoOdx" name="txtCodigoOdx" value="<?=$odonid;?>">

										<!-- Id. del Presupuesto   -->
										<input type="hidden" id="txtIdPpto" name="txtIdPpto" value="<?=$arrPpo[0]->getPptoid();?>">
										
										<div class="seccionBotones">
											<input type="button" class="btn btn-primary" value="Agregar Tratamiento" onclick="agregarTratamiento($('#txtDienteTratado').val(), $('#txtCaraTratada').val(), $('#cbxEstado').val());">
										</div>
										
									</div>
								</form>
									
							</section>
								
							<hr>     <!-- Línea divisoria -->

							<!-- SECCION TABLA DE ORDENES  MEDICAS DENTALES INDICADAS -->
							<section class="displayInlineBlockTop textAlignCenter" style="margin-left: 10px;width: 230px;">
								
								<!-- SECCION PARA LA TABLA DE TRATAMIENTOS SELECCIONADOS POR EL ODONTOLOGO-->
								<div id="divTratamiento" class="displayInlineBlockTop sombraFormulario" style="width: 686px;height:130px;overflow-y: scroll;">
										<table class="table" id="tablaTratamiento" width="100%">
											<tbody></tbody>
										</table>
								</div>
								
								<!-- SECCION PARA EL CAMPO DE OBSERVACION O DESCRIPCION ADICIONAL A LA MARCA SELECCIONADA -->
								<div class="displayInlineBlockTop">
										<label>Descripci&oacute;n del estado dental</label>
										<textarea name="txtDescripcion" id="txtDescripcion" rows="1" cols="78" class="textArea" style="width: 686px;"></textarea>
								</div>
									
							</section>
								
							<hr>     <!-- Línea divisoria -->
								
							<!-- SECCION PARA LAS ACCIONES Y EL ALERT DEL RESULTADO -->
							<div>
									<!-- SECCION PARA EL RESULTADO DE LA PAGINA AJAX -->
									<section id="seccionPaginaAjax"></section>
										
									<!-- BOTON DE COMANDO PARA REALIZAR EL GUARDADO DEL ODONTOGRAMA -->
									<input type="button" class="btn btn-primary anchoCompleto" value="Guardar Tratamiento" onclick="guardarMarcas();">
							</div>
						
						</section>     <!-- Parte donde termina el formulario para registro de estados -->
				
					</div>

					<div class="modal-footer">
				          	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				    </div>
				</div>
			</div>
		</div>

		<section id="seccionOrdenesMed" style="background-color: white;padding: 0px;" >	 <!-- class="container-fluid" -->


			<div id="pestanas">
                <ul id=lista>
                    <li id="p1"><a href='javascript:cambiarPestanna(pestanas,p1);'><h6>Proc. Pendientes</h6></a></li>
                    <li id="p2"><a href='javascript:cambiarPestanna(pestanas,p2);'><h6>Proc. Realizados</h6></a></li>
                    <li id="p3"><a href='javascript:cambiarPestanna(pestanas,p3);'><h6>Odon. Actual</h6></a></li>
                    <li id="p4"><a href='javascript:cambiarPestanna(pestanas,p4);'><h6>Presupuesto</h6></a></li>
                    <li id="p5"><a href='javascript:cambiarPestanna(pestanas,p5);'><h6>Estado de Cuenta</h6></a></li>
                </ul>
            </div>

            <!-- <body onload="javascript:cambiarPestanna(pestanas,p1);"> -->

            <div id="contenidopestanas">
            	<!-- Panel Listado procedimientos pendientes por realizar -->
                <div id="cp1">
                    <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mOrdenes"> Agregar Procedimiento </button>
                    <br> <br> -->
                    <section id="seccionTablaProcedimientos" style="background-color: white;padding: 10px;width: 1273px;" class="displayInlineBlockTop sombraFormulario">	
					</section>
                </div>

                <!-- Panel Listado procedimientos realizados -->
                <div id="cp2">
                    <section id="seccionTablaProcedFin" style="background-color: white;padding: 10px;width: 1273px;" class="displayInlineBlockTop sombraFormulario">	
					</section>
                </div>

                <!-- Panel Listado procedimientos realizados en el odontograma actual de trabajo -->
                <div id="cp3">
                    <section id="seccionTablaProcedAct" style="background-color: white;padding: 10px;width: 1273px;" class="displayInlineBlockTop sombraFormulario">
					</section>     
                </div>

                <!-- Panel Listado todos los procedimientos presupuestados -->
                <div id="cp4">
                    <section id="seccionTablaProcedTodos" style="background-color: white;padding: 10px;width: 1273px;" class="displayInlineBlockTop sombraFormulario">	
					</section>
                </div>

                <!-- Panel Listado de pagos realizados por el paciente y estado del presupuesto -->
                <div id="cp5">
                    <section id="seccionTablaPagos" style="background-color: white;padding: 10px;width: 1273px;" class="displayInlineBlockTop sombraFormulario">	
						<?php
							$arrPagos = $contPpto->fn_ListarPagosPpto($arrPpo[0]->getPptoid());
							if( is_null($arrPagos) ) {
								$varHtml="";
								$varHtml="Estado de Cuenta Paciente";
								$varHtml.="<table class='table'>";
								$varHtml.="<thead style='font-size: 12px;'>";	
								$varHtml.="<th>Fecha</th>";
								$varHtml.="<th>Id. Concepto</th>";
								$varHtml.="<th>Detalle</th>";
								$varHtml.="<th>Valor</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
								$varHtml.="<th>Observaciones</th>";
								$varHtml.="<th>Estado</th>";
								$varHtml.="</thead>";
								$varHtml.="<tbody style='font-size: 12px;''>";
								$varHtml.="</tbody>";
								$varHtml.="</table>";
								$varHtml.="<td>Sin Registros</td>";
								echo $varHtml;
							} else {
								$varHtml="";
								$varHtml="Estado de Cuenta Paciente";
								$varHtml.="<table class='table'>";
								$varHtml.="<thead style='font-size: 12px;'>";	
								$varHtml.="<th>Fecha</th>";
								$varHtml.="<th>Id. Concepto</th>";
								$varHtml.="<th>Detalle</th>";
								$varHtml.="<th>Valor</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
								$varHtml.="<th>Observaciones</th>";
								$varHtml.="<th>Estado</th>";
								$varHtml.="</thead>";
								$varHtml.="<tbody style='font-size: 12px;''>";
								
								foreach($arrPagos as $key => $obj) {					
								//foreach($resPOdontograma as $row) {
									$varHtml.="<tr>";
									$varHtml.="<td>{$obj->getFecha()}</td>";
									$varHtml.="<td>{$obj->getIdconcepto()}</td>";
									$varHtml.="<td>{$obj->getIdconcepto()}</td>";
									$varHtml.="<td>{$obj->getVlrmovimiento()}</td>";
									$varHtml.="<td>{$obj->getObs()}</td>";
									$varHtml.="<td>{$obj->getEstado()}</td>";
									$varHtml.="</tr>";
								}

								$varHtml.="</tbody>";
								$varHtml.="</table>";
								echo $varHtml;
							}
                        ?>
					</section>           
                </div>

            </div>

		</section>

	</section>
	
	<!-- <footer></footer> -->

	<!-- <input type="hidden" id="txtIdPpto" name="txtIdPpto" value="<?=$arrPpo[0]->getPptoid();?>"> -->
	
</div>
<script>
	cargarTratamientos("seccionTablaTratamientos", "verodontograma.php", $('#txtCodigoOdx').val());
	cargarDientes("seccionDientes", "dientes.php", '', $('#txtCodigoOdx').val());
	cargarProcedimientos("seccionTablaProcedimientos", "1", "viewProcedimiento.php", $('#txtCodigoOdx').val(), $('#txtIdPpto').val());
	cargarProcedimientos("seccionTablaProcedFin", "2", "viewProcedimiento.php", $('#txtCodigoOdx').val(), $('#txtIdPpto').val());
	cargarProcedimientos("seccionTablaProcedAct", "3", "viewProcedimiento.php", $('#txtCodigoOdx').val(), $('#txtIdPpto').val());
	cargarProcedimientos("seccionTablaProcedTodos", "4", "viewProcedimiento.php", $('#txtCodigoOdx').val(), $('#txtIdPpto').val());
	cambiarPestanna(pestanas,p1);
</script>

<!-- </html> -->