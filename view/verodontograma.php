<?php
	require_once("../controller/ControllerOxodond.php");
	$controladorOdontograma=new ControllerOxodond();
	$resTOdontograma=$controladorOdontograma->fn_listarEstados($_POST["codigoOdx"]);
?>
Estados dentales del Paciente
<table class="table">
	<thead style="font-size: 12px;">
		<th>Diente</th>
		<th>Descripci&oacute;n</th>
		<th>Cara</th>
		<th class="widthDetalleTable">F. Marca</th>
		<!-- <th class="widthDetalleTable"></th> -->
	</thead>
	<tbody style="font-size: 12px;">
		
		<?php
			foreach($resTOdontograma as $key => $row) {
		?>
		
		<tr>
			<td><?=$row[5]?></td>
			<td><?=$row[3]?></td>
			<td><?=$row[7]?></td>
			<td><?=$row[6]?></td>
		</tr>

		<?php
			}
		?>

	</tbody>
</table>