<?php
	// require_once("../controller/ControllerOxproc.php");
	// $controladorProc=new ControllerOxproc();
	// require_once("../controller/ControllerOmed.php");
	// $cOmed = new ControllerOmed();
	require_once("../controller/ControllerOxodon.php");
	$cOxodon = new ControllerOxodon();
	$varHtml="";

	// $resPOdontograma=$controladorProc->fn_listaProcediminetosOdx($_POST["codigoOdx"]);
	$varHtml="Accion: " . $_POST["accion"] . " Codigo Odx: " . $_POST["codigoOdx"] . " Cod. Ppto: " . $_POST["codPpto"];
	if(isset($_POST["accion"]) && isset($_POST["codigoOdx"]) && isset($_POST["codPpto"])) {
		$command = $_POST["accion"];
		$varHtml="";

		switch($command) {
			case '1':  // Procedimientos Por realizar
				$resPOdontograma=$cOxodon->fn_listaProcediminetosOdx($_POST["codPpto"],'P');

				if($resPOdontograma==null) {
					$varHtml="";
					$varHtml="Procedimientos Pendientes";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
					$varHtml.="</tbody>";
					$varHtml.="</table>";
					$varHtml.="<td>Sin Procedimientos</td>";
					echo $varHtml;
				} else {
					$varHtml="";
					$varHtml="Procedimientos Pendientes";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
						
					 // foreach($resPOdontograma as $key => $row) {					
					foreach($resPOdontograma as $row) {
						$varHtml.="<tr>";
							$varHtml.="<td>" . $row['procid'] . "</td>";
							$varHtml.="<td>" . $row['fechaproc'] . "</td>";
							$varHtml.="<td>" . $row['serid'] . "</td>";
							$varHtml.="<td>" . $row['descripcion'] . "</td>";
							$varHtml.="<td>" . $row['fecharealizada'] . "</td>";
							$varHtml.="<td>" . $row['estado']  . "</td>";
						$varHtml.="</tr>";
					}

					$varHtml.="</tbody>";
					$varHtml.="</table>";
					echo $varHtml;
				}
				break;
			case '2':   // Procedimientos Realizados
				$resPOdontograma=$cOxodon->fn_listaProcediminetosOdx($_POST["codPpto"],'F');

				if($resPOdontograma==null) {
					$varHtml="";
					$varHtml="Procedimientos Realizados";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
					$varHtml.="</tbody>";
					$varHtml.="</table>";
					$varHtml.="<td>Sin Procedimientos</td>";
					echo $varHtml;
				} else {
					$varHtml="";
					$varHtml="Procedimientos Realizados";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
						
					 // foreach($resPOdontograma as $key => $row) {					
					foreach($resPOdontograma as $row) {
						$varHtml.="<tr>";
							$varHtml.="<td>" . $row['procid'] . "</td>";
							$varHtml.="<td>" . $row['fechaproc'] . "</td>";
							$varHtml.="<td>" . $row['serid'] . "</td>";
							$varHtml.="<td>" . $row['descripcion'] . "</td>";
							$varHtml.="<td>" . $row['fecharealizada'] . "</td>";
							$varHtml.="<td>" . $row['estado']  . "</td>";
						$varHtml.="</tr>";
					}

					$varHtml.="</tbody>";
					$varHtml.="</table>";
					echo $varHtml;
				}
				break;	
			case '3':   // Procedimientos odontograma Actual
				$resPOdontograma=$cOxodon->fn_listaProcedOdxAct($_POST["codigoOdx"]);

				if($resPOdontograma==null) {
					$varHtml="";
					$varHtml="Procedimientos Odontograma Actual";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
					$varHtml.="</tbody>";
					$varHtml.="</table>";
					$varHtml.="<td>Sin Procedimientos</td>";
					echo $varHtml;
				} else {
					$varHtml="";
					$varHtml="Procedimientos Odontograma Actual";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
						
					 // foreach($resPOdontograma as $key => $row) {					
					foreach($resPOdontograma as $row) {
						$varHtml.="<tr>";
							$varHtml.="<td>" . $row['procid'] . "</td>";
							$varHtml.="<td>" . $row['fechaproc'] . "</td>";
							$varHtml.="<td>" . $row['serid'] . "</td>";
							$varHtml.="<td>" . $row['descripcion'] . "</td>";
							$varHtml.="<td>" . $row['fecharealizada'] . "</td>";
							$varHtml.="<td>" . $row['estado']  . "</td>";
						$varHtml.="</tr>";
					}

					$varHtml.="</tbody>";
					$varHtml.="</table>";
					echo $varHtml;
				}
				break;	
			case '4':   // Procedimientos Presupuestados
				$resPOdontograma=$cOxodon->fn_listaProcediminetosPpto($_POST["codPpto"]);

				if($resPOdontograma==null) {
					$varHtml="";
					$varHtml="Procedimientos Presupuesto";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";       //$varHtml.="<th class='widthDetalleTable'>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
					$varHtml.="</tbody>";
					$varHtml.="</table>";
					$varHtml.="<td>Sin Procedimientos</td>";
					echo $varHtml;
				} else {
					$varHtml="";
					$varHtml="Procedimientos Presupuesto";
					$varHtml.="<table class='table'>";
					$varHtml.="<thead style='font-size: 12px;'>";	
					$varHtml.="<th>Id. Pr&aacute;ctica</th>";
					$varHtml.="<th>Fe. Programaci&oacute;n</th>";
					$varHtml.="<th>Id. Servicio</th>";
					$varHtml.="<th>Servicio</th>";
					$varHtml.="<th>Realizado</th>";
					$varHtml.="<th>Estado</th>";
					$varHtml.="</thead>";
					$varHtml.="<tbody style='font-size: 12px;''>";
						
					 // foreach($resPOdontograma as $key => $row) {					
					foreach($resPOdontograma as $row) {
						$varHtml.="<tr>";
							$varHtml.="<td>" . $row['procid'] . "</td>";
							$varHtml.="<td>" . $row['fechaproc'] . "</td>";
							$varHtml.="<td>" . $row['serid'] . "</td>";
							$varHtml.="<td>" . $row['descripcion'] . "</td>";
							$varHtml.="<td>" . $row['fecharealizada'] . "</td>";
							$varHtml.="<td>" . $row['estado']  . "</td>";
						$varHtml.="</tr>";
					}

					$varHtml.="</tbody>";
					$varHtml.="</table>";
					echo $varHtml;
				}
				break;	
			default:
				$varHtml.="<td>Sin estado</td>";
		}

	} else {
		$varHtml.="<td>Sin Procedimientos</td>";
		echo $varHtml;
	}

?>
