<?php
    include_once "viewOdontogramas.php"; // Programación funcional
    include_once "../includes/encabezado.php"; 
?>

	<!-- Layout de página -->
	<div class="container-fluid">
        
        <!-- Header -->
        <header class="">
            <a href="index.php"><img class="img-thumbnail" src="../public/img/Tooth.ico"></a>
            <div class="container-fluid">
                <h2>Historia Cl&iacute;nica Odontol&oacute;gica</h2>
            </div>
        </header>
        
        <!-- Área principal de la página -->
        <div class="row content">    


            	<!-- Menú lateral de navegación -->
                <!-- Menú configurable desde la Base de Datos (Se debe crear de una tabla de Base de Datos [menu]) -->
            	<div class="col-sm-2 sidenav">
                        <div class="round-border-topright"></div>
                        <h4 class="">Men&uacute;</h4>
                        <dl class="nav3-grid">
                            <dt><a href="index.php">Home</a></dt>
                            <dt><a href="logout.php">Salir</a></dt>
                    	</dl>                       
                </div>		

                <!-- Contenido principal de la página -->
                <div class="col-sm-9"  id="areaprincipal">
        				<h4 class="pagetitle">Sistema de Gesti&oacute;n Odontológica</h4>            			
                        
                        <!-- Listado de pacientes para seleccionar -->
                        <div class="content">
                            <form method="post" action="">
                                <p>
                                    <label>Seleccione un paciente : </label>
                                    <select class="select" name="afi">
                                        <option value="0">Pacientes</option> 
                                        <?php
                                            listarOdontograma();       // <-- Aquí se cargan los pacientes (No lista odontogramas)
                                        ?>
                                    </select>

                                    <input class="btn btn-primary" type="submit" name="submit" value="Buscar" />
                                </p>
                            </form>
                        </div>

                        <hr class="clear-contentunit" />

                        <!-- Listado de Odontogramas del paciente seleccionado -->
                        <div class="row">
                                <div class="col">
                                        <?php
                                              if(isset($_POST['submit'])){
                                                fn_listaOxodonAfiliado();     // <-- Genera el listado de odontogramas del Paciente
                                              }
                                        ?>
                                </div>
                        </div>    

                </div>

        </div>

<?php 
    include "../includes/footer.php"; 
?>