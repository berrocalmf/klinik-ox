<!doctype html>
<html lang="sp">
<head>
	<meta charset="UTF-8">
	<title>PRUEBA 1 PARA REGISTRO DE ODONTOGRAMA</title>
	<link rel="stylesheet" href="../public/css/cssDiente.css">
	<link rel="stylesheet" href="../public/css/cssDienteGeneral.css">
	<link rel="stylesheet" href="../public/css/cssFormulario.css">
	<link rel="stylesheet" href="../public/css/cssComponentes.css">
	<link rel="stylesheet" href="../public/css/cssComponentesPersonalizados.css">
	<link rel="stylesheet" href="../public/css/cssContenido.css">
	<script src="../public/js/jquery-2.0.3.min.js"></script>
	<script src="../public/js/jsAcciones.js"></script>
</head>
<body>
	<!-- Ejemplo de formulario para Registro de Odontogramas -->
	<form style="text-align: left;">
		<div class="tituloFormulario">INFORMACION DEL TRATAMIENTO</div>
		<div class="contenidoInterno">
			<label for=""><b>Id. Médico:</b></label>
			<input type="text" id="medid" name="medid">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Id. Tratamiento:</b></label>
			<input type="text" id="tratid" name="tratid">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Id. Paciente:</b></label>
			<input type="text" id="afiid" name="afiid">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Id. Historia:</b></label>
			<input type="text" id="hcpoxid" name="hcpoxid">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Id. Consecutivo:</b></label>
			<input type="text" id="consecutivo" name="consecutivo">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Denticion Temp:</b></label>
			<input type="text" id="denttemporal" name="denttemporal">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Indice CPOD:</b></label>
			<input type="text" id="indiceCPOD" name="indiceCPOD">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Indice CEOD:</b></label>
			<input type="text" id="indiceCEOD" name="indiceCEOD">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<label for=""><b>Costumer Id:</b></label>
			<input type="text" id="customerid" name="customerid">		<!-- class="textAlignCenter" size="4" readonly="readonly" -->
			<br><br>
			<div>
				<input type="button" class="button anchoCompleto" value="Guardar Odontograma" onclick="crearOdontograma();">
			</div>		
		</div>	
	</form>
	<div>
		<section id="seccionPaginaAjax"></section>
	</div> 
</body>
</html>