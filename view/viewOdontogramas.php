<?php
		require_once("../controller/ControllerOxodon.php");
		require_once("../connection/Conexion.php");

		$conexion=new Conexion();
		$conn=$conexion->getConexion();
		// $controladorOdontograma=new ControllerOxodon();

		/**
		 * Lista las atenciones odontológicas de un paciente
		 * Refactored: 06.04.2021 fberrocalm
		 */
		function fn_listaOxodonAfiliado() {
				$afiid = $_POST['afi'];
				$controladorOdontograma = new ControllerOxodon();
				global $conn;

				if( $afiid ) {
						$arrayTOdontograma = $controladorOdontograma->fn_listarOdonPaciente($afiid);

						$sql    = "SELECT * FROM pac WHERE pacid=$afiid";
			            $query  = $conn->prepare($sql); 
			            $query->execute();  
			            $result = $query->fetchAll();
			          
			            foreach ($result as $row) {
			               		$afiliado = $row["docidpaciente"] . " " . $row["papellido"] . " " . $row["sapellido"] . " " . $row["pnombre"] . " " . $row["snombre"]; 
			            } 

						$codhtml="";
						$codhtml="<h6>Paciente: " . $afiliado . "</h6>";
						$codhtml.="<hr>";
						$codhtml.="Listado de Odontogramas del paciente";
						$codhtml.="<table class='table'>";
						$codhtml.="<thead><tr>";
						$codhtml.="<th scope='col'>Id. Odon.</th><th scope='col'>No. Ficha</th><th scope='col'>Fecha</th><th scope='col'>Estado</th><th scope='col'>Desc. Estado</th><th scope='col'>Acciones</th>";
						$codhtml.="</tr></thead>";	
						$codhtml.="<tbody style='font-size: 13px;'>";

						if (!empty($arrayTOdontograma)) {
							foreach($arrayTOdontograma as $key => $value) {
									$codhtml.="<tr>";
									$codhtml.="<th scope='row'>".$value->getOdonid()."</th>";
									$codhtml.="<td>" . $value->getNroficha() . "</td>";
									$codhtml.="<td>" . $value->getFecha() . "</td>";
									$codhtml.="<td>" . $value->getEstado() . "</td>";

									switch($value->getEstado()) {
											case 'P':
												$codhtml.="<td>Abierto - En Tratamiento</td>";
												break;
											case 'F':
												$codhtml.="<td>Finalizado</td>";
												break;
											case 'C':
												$codhtml.="<td>Cerrado - En tratamiento</td>";
												break;	
											case 'A':
												$codhtml.="<td>Anulado</td>";
												break;
											default:
												$codhtml.="<td>Sin estado</td>";
									}	
									
									$codhtml.="<td><input id='".$value->getOdonid()."' type='button' class='btn btn-info' value='Trabajar' onclick=\"cargarOdontograma('areaprincipal','odontograma.php',".$value->getOdonid()."," . $afiid . ",".$value->getAteid().");\"></td>";
									$codhtml.="</tr>";
								
							}
						}	

						$codhtml.="</tbody></table>";
						echo $codhtml;

				} else {
						echo "<div class='alertaIncorrecto'> Variable IDAFILIADO, con valores incorrectos o sin dato. </ div>";
				}	

		}

		/**
		 * Descripción: Lista los pacientes para seleccionar
		 * Refactored: 06.04.2021 - fberrocalm
		 */
		function listarOdontograma() {
				global $conn;

		        $sql = "select * from pac";
		        $qry = $conn->prepare($sql);
		        $qry->execute();
		        $result = $qry->fetchAll();

		        foreach($result as $row) {
			            $idafi  = $row['pacid'];
			            $nombre = $row['papellido'] . " " . $row['sapellido'] . " " . $row['pnombre'] . " " . $row['snombre']; 
			            echo "<option value='" . $idafi . "'>" . $nombre .  "</option>";
		        } 
		}

?>