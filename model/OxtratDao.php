<?php

 /**
  * Oxtrat Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Oxtrat object instances. 
  */

class OxtratDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    public function __construct() {
        $this->resetCache();
    }
 
    public function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxtrat();
    }

    public function getObject(&$conn, $tratid) {

          $valueObject = $this->createValueObject();
          $valueObject->setTratid($tratid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    public function load(&$conn, &$valueObject) {

          if (!$valueObject->getTratid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM oxtrat WHERE (tratid = ".$valueObject->getTratid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    public function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM oxtrat ORDER BY tratid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    public function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO oxtrat ( constrat, motivotrat, ";
          $sql = $sql."enembarazo, mesesemb, probembaraz, ";
          $sql = $sql."fechainicio, fechafin, fechacierre, ";
          $sql = $sql."feproxcontrol, observaciones, estado, ";
          $sql = $sql."fechaestado, afiid, docidafiliado, ";
          $sql = $sql."terid, idtercero, medid, ";
          $sql = $sql."tratvalor, tratvlrsaldo, indiceplaca, ";
          $sql = $sql."tratcerrado, costumerid) VALUES (";
          $sql = $sql."'".$valueObject->getConstrat()."', ";
          $sql = $sql."'".$valueObject->getMotivotrat()."', ";
          $sql = $sql."".$valueObject->getEnembarazo().", ";
          $sql = $sql."".$valueObject->getMesesemb().", ";
          $sql = $sql."'".$valueObject->getProbembaraz()."', ";
          $sql = $sql."'".$valueObject->getFechainicio()."', ";
          $sql = $sql."'".$valueObject->getFechafin()."', ";
          $sql = $sql."'".$valueObject->getFechacierre()."', ";
          $sql = $sql."'".$valueObject->getFeproxcontrol()."', ";
          $sql = $sql."'".$valueObject->getObservaciones()."', ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."'".$valueObject->getFechaestado()."', ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."'".$valueObject->getDocidafiliado()."', ";
          $sql = $sql."".$valueObject->getTerid().", ";
          $sql = $sql."'".$valueObject->getIdtercero()."', ";
          $sql = $sql."".$valueObject->getMedid().", ";
          $sql = $sql."'".$valueObject->getTratvalor()."', ";
          $sql = $sql."'".$valueObject->getTratvlrsaldo()."', ";
          $sql = $sql."'".$valueObject->getIndiceplaca()."', ";
          $sql = $sql."".$valueObject->getTratcerrado().", ";
          $sql = $sql."".$valueObject->getCostumerid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {
              $valueObject->setTratid($row[0]); 
              $existe = true;
          }  

          return $existe;
    }

    public function save(&$conn, &$valueObject) {

          $sql = "UPDATE oxtrat SET constrat = '".$valueObject->getConstrat()."', ";
          $sql = $sql."motivotrat = '".$valueObject->getMotivotrat()."', ";
          $sql = $sql."enembarazo = ".$valueObject->getEnembarazo().", ";
          $sql = $sql."mesesemb = ".$valueObject->getMesesemb().", ";
          $sql = $sql."probembaraz = '".$valueObject->getProbembaraz()."', ";
          $sql = $sql."fechainicio = '".$valueObject->getFechainicio()."', ";
          $sql = $sql."fechafin = '".$valueObject->getFechafin()."', ";
          $sql = $sql."fechacierre = '".$valueObject->getFechacierre()."', ";
          $sql = $sql."feproxcontrol = '".$valueObject->getFeproxcontrol()."', ";
          $sql = $sql."observaciones = '".$valueObject->getObservaciones()."', ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."fechaestado = '".$valueObject->getFechaestado()."', ";
          $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
          $sql = $sql."docidafiliado = '".$valueObject->getDocidafiliado()."', ";
          $sql = $sql."terid = ".$valueObject->getTerid().", ";
          $sql = $sql."idtercero = '".$valueObject->getIdtercero()."', ";
          $sql = $sql."medid = ".$valueObject->getMedid().", ";
          $sql = $sql."tratvalor = '".$valueObject->getTratvalor()."', ";
          $sql = $sql."tratvlrsaldo = '".$valueObject->getTratvlrsaldo()."', ";
          $sql = $sql."indiceplaca = '".$valueObject->getIndiceplaca()."', ";
          $sql = $sql."tratcerrado = ".$valueObject->getTratcerrado().", ";
          $sql = $sql."costumerid = ".$valueObject->getCostumerid()."";
          $sql = $sql." WHERE (tratid = ".$valueObject->getTratid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    public function delete(&$conn, &$valueObject) {
          if (!$valueObject->getTratid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM oxtrat WHERE (tratid = ".$valueObject->getTratid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    public function deleteAll(&$conn) {

          $sql = "DELETE FROM oxtrat";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    public function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM oxtrat";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();  
          // $result = $conn->execute($sql);

          foreach($result as $row) {
              $allRows = $row[0];
          } 

          return $allRows;
    }

    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxtrat WHERE 1=1 ";

          if ($valueObject->getTratid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tratid = ".$valueObject->getTratid()." ";
          }

          if ($valueObject->getConstrat() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND constrat LIKE '".$valueObject->getConstrat()."%' ";
          }

          if ($valueObject->getMotivotrat() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivotrat LIKE '".$valueObject->getMotivotrat()."%' ";
          }

          if ($valueObject->getEnembarazo() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND enembarazo = ".$valueObject->getEnembarazo()." ";
          }

          if ($valueObject->getMesesemb() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mesesemb = ".$valueObject->getMesesemb()." ";
          }

          if ($valueObject->getProbembaraz() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND probembaraz LIKE '".$valueObject->getProbembaraz()."%' ";
          }

          if ($valueObject->getFechainicio() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechainicio = '".$valueObject->getFechainicio()."' ";
          }

          if ($valueObject->getFechafin() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechafin = '".$valueObject->getFechafin()."' ";
          }

          if ($valueObject->getFechacierre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechacierre = '".$valueObject->getFechacierre()."' ";
          }

          if ($valueObject->getFeproxcontrol() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND feproxcontrol = '".$valueObject->getFeproxcontrol()."' ";
          }

          if ($valueObject->getObservaciones() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND observaciones LIKE '".$valueObject->getObservaciones()."%' ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getFechaestado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechaestado = '".$valueObject->getFechaestado()."' ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getDocidafiliado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND docidafiliado LIKE '".$valueObject->getDocidafiliado()."%' ";
          }

          if ($valueObject->getTerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND terid = ".$valueObject->getTerid()." ";
          }

          if ($valueObject->getIdtercero() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND idtercero LIKE '".$valueObject->getIdtercero()."%' ";
          }

          if ($valueObject->getMedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medid = ".$valueObject->getMedid()." ";
          }

          if ($valueObject->getTratvalor() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tratvalor = '".$valueObject->getTratvalor()."' ";
          }

          if ($valueObject->getTratvlrsaldo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tratvlrsaldo = '".$valueObject->getTratvlrsaldo()."' ";
          }

          if ($valueObject->getIndiceplaca() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND indiceplaca LIKE '".$valueObject->getIndiceplaca()."%' ";
          }

          if ($valueObject->getTratcerrado() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tratcerrado = ".$valueObject->getTratcerrado()." ";
          }

          if ($valueObject->getCostumerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND costumerid = ".$valueObject->getCostumerid()." ";
          }


          $sql = $sql."ORDER BY tratid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    public function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    public function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){ 
          // if ($row = $conn->nextRow($result)) {
                   $valueObject->setTratid($row[0]); 
                   $valueObject->setConstrat($row[1]); 
                   $valueObject->setMotivotrat($row[2]); 
                   $valueObject->setEnembarazo($row[3]); 
                   $valueObject->setMesesemb($row[4]); 
                   $valueObject->setProbembaraz($row[5]); 
                   $valueObject->setFechainicio($row[6]); 
                   $valueObject->setFechafin($row[7]); 
                   $valueObject->setFechacierre($row[8]); 
                   $valueObject->setFeproxcontrol($row[9]); 
                   $valueObject->setObservaciones($row[10]); 
                   $valueObject->setEstado($row[11]); 
                   $valueObject->setFechaestado($row[12]); 
                   $valueObject->setAfiid($row[13]); 
                   $valueObject->setDocidafiliado($row[14]); 
                   $valueObject->setTerid($row[15]); 
                   $valueObject->setIdtercero($row[16]); 
                   $valueObject->setMedid($row[17]); 
                   $valueObject->setTratvalor($row[18]); 
                   $valueObject->setTratvlrsaldo($row[19]); 
                   $valueObject->setIndiceplaca($row[20]); 
                   $valueObject->setTratcerrado($row[21]); 
                   $valueObject->setCostumerid($row[22]);
                   $valida=true; 
                    // } else {
                    //      //print " Object Not Found!";
                    //      return false;
          }
          return $valida;
    }

    public function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
          // while ($row = $conn->nextRow($result)) {
               $temp = $this->createValueObject();

               $temp->setTratid($row[0]); 
               $temp->setConstrat($row[1]); 
               $temp->setMotivotrat($row[2]); 
               $temp->setEnembarazo($row[3]); 
               $temp->setMesesemb($row[4]); 
               $temp->setProbembaraz($row[5]); 
               $temp->setFechainicio($row[6]); 
               $temp->setFechafin($row[7]); 
               $temp->setFechacierre($row[8]); 
               $temp->setFeproxcontrol($row[9]); 
               $temp->setObservaciones($row[10]); 
               $temp->setEstado($row[11]); 
               $temp->setFechaestado($row[12]); 
               $temp->setAfiid($row[13]); 
               $temp->setDocidafiliado($row[14]); 
               $temp->setTerid($row[15]); 
               $temp->setIdtercero($row[16]); 
               $temp->setMedid($row[17]); 
               $temp->setTratvalor($row[18]); 
               $temp->setTratvlrsaldo($row[19]); 
               $temp->setIndiceplaca($row[20]); 
               $temp->setTratcerrado($row[21]); 
               $temp->setCostumerid($row[22]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
}

?>