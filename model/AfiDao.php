<?php
 /**
  * Afi Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Afi object instances. 
  * Ing. FMBM 02.NOV.2018
  */

class AfiDao {

    // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Afi();
    }

    function getObject(&$conn, $afiid) {
          $valueObject = $this->createValueObject();
          $valueObject->setAfiid($afiid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getAfiid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM afi WHERE (afiid = ".$valueObject->getAfiid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM afi ORDER BY afiid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO afi ( saas_ciaid, papellido, sapellido, ";
          $sql = $sql."pnombre, snombre, tipodoc, ";
          $sql = $sql."docidafiliado, email, celular, ";
          $sql = $sql."telefonores, telefonoofi, idalterna, ";
          $sql = $sql."fechanacimiento, gruposanguineo, estadocivil, ";
          $sql = $sql."grupoetnico, sexo, localidad, ";
          $sql = $sql."tipozona, direccion, cpostal, ";
          $sql = $sql."ciuid, estado, ocuid, ";
          $sql = $sql."tercid, nivel, ";
          $sql = $sql."incapacidadlaboral, tipodiscapacidad, gradodiscapacidad, ";
          $sql = $sql."tipoafiliado, nocaso, remitidopor, ";
          $sql = $sql."obs) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."'".$valueObject->getPapellido()."', ";
          $sql = $sql."'".$valueObject->getSapellido()."', ";
          $sql = $sql."'".$valueObject->getPnombre()."', ";
          $sql = $sql."'".$valueObject->getSnombre()."', ";
          $sql = $sql."'".$valueObject->getTipodoc()."', ";
          $sql = $sql."'".$valueObject->getDocidafiliado()."', ";
          $sql = $sql."'".$valueObject->getEmail()."', ";
          $sql = $sql."'".$valueObject->getCelular()."', ";
          $sql = $sql."'".$valueObject->getTelefonores()."', ";
          $sql = $sql."'".$valueObject->getTelefonoofi()."', ";
          $sql = $sql."'".$valueObject->getIdalterna()."', ";
          $sql = $sql."'".$valueObject->getFechanacimiento()."', ";
          $sql = $sql."'".$valueObject->getGruposanguineo()."', ";
          $sql = $sql."'".$valueObject->getEstadocivil()."', ";
          $sql = $sql."'".$valueObject->getGrupoetnico()."', ";
          $sql = $sql."'".$valueObject->getSexo()."', ";
          $sql = $sql."'".$valueObject->getLocalidad()."', ";
          $sql = $sql."".$valueObject->getTipozona().", ";
          $sql = $sql."'".$valueObject->getDireccion()."', ";
          $sql = $sql."'".$valueObject->getCpostal()."', ";
          $sql = $sql."".$valueObject->getCiuid().", ";
          $sql = $sql."".$valueObject->getEstado().", ";
          $sql = $sql."".$valueObject->getOcuid().", ";
          $sql = $sql."".$valueObject->getTercid().", ";
          $sql = $sql."'".$valueObject->getNivel()."', ";
          $sql = $sql."".$valueObject->getIncapacidadlaboral().", ";
          $sql = $sql."".$valueObject->getTipodiscapacidad().", ";
          $sql = $sql."".$valueObject->getGradodiscapacidad().", ";
          $sql = $sql."".$valueObject->getTipoafiliado().", ";
          $sql = $sql."".$valueObject->getNocaso().", ";
          $sql = $sql."'".$valueObject->getRemitidopor()."', ";
          $sql = $sql."'".$valueObject->getObs()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {
              $valueObject->setAfiid($row[0]);
              $existe = true;
          } 
          return $existe;
    }

    function save(&$conn, &$valueObject) {
        // Vlidar los campo llave foránea y aquellos que sepuedan guardar en NULL
          $sql = "UPDATE afi SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."papellido = '".$valueObject->getPapellido()."', ";
          $sql = $sql."sapellido = '".$valueObject->getSapellido()."', ";
          $sql = $sql."pnombre = '".$valueObject->getPnombre()."', ";
          $sql = $sql."snombre = '".$valueObject->getSnombre()."', ";
          $sql = $sql."tipodoc = '".$valueObject->getTipodoc()."', ";
          $sql = $sql."docidafiliado = '".$valueObject->getDocidafiliado()."', ";
          $sql = $sql."email = '".$valueObject->getEmail()."', ";
          $sql = $sql."celular = '".$valueObject->getCelular()."', ";
          $sql = $sql."telefonores = '".$valueObject->getTelefonores()."', ";
          $sql = $sql."telefonoofi = '".$valueObject->getTelefonoofi()."', ";
          $sql = $sql."idalterna = '".$valueObject->getIdalterna()."', ";
          $sql = $sql."fechanacimiento = '".$valueObject->getFechanacimiento()."', ";
          $sql = $sql."gruposanguineo = '".$valueObject->getGruposanguineo()."', ";
          $sql = $sql."estadocivil = '".$valueObject->getEstadocivil()."', ";
          $sql = $sql."grupoetnico = '".$valueObject->getGrupoetnico()."', ";
          $sql = $sql."sexo = '".$valueObject->getSexo()."', ";
          $sql = $sql."localidad = '".$valueObject->getLocalidad()."', ";
          $sql = $sql."tipozona = ".$valueObject->getTipozona().", ";
          $sql = $sql."direccion = '".$valueObject->getDireccion()."', ";
          $sql = $sql."cpostal = '".$valueObject->getCpostal()."', ";
          $sql = $sql."ciuid = ".$valueObject->getCiuid().", ";
          $sql = $sql."estado = ".$valueObject->getEstado().", ";
          $sql = $sql."ocuid = ".$valueObject->getOcuid().", ";
          $sql = $sql."tercid = ".$valueObject->getTercid().", ";
          $sql = $sql."nivel = '".$valueObject->getNivel()."', ";
          $sql = $sql."fechaultimavis = '".$valueObject->getFechaultimavis()."', ";
          $sql = $sql."incapacidadlaboral = ".$valueObject->getIncapacidadlaboral().", ";
          $sql = $sql."tipodiscapacidad = ".$valueObject->getTipodiscapacidad().", ";
          $sql = $sql."gradodiscapacidad = ".$valueObject->getGradodiscapacidad().", ";
          $sql = $sql."tipoafiliado = ".$valueObject->getTipoafiliado().", ";
          $sql = $sql."nocaso = ".$valueObject->getNocaso().", ";
          $sql = $sql."remitidopor = '".$valueObject->getRemitidopor()."', ";
          $sql = $sql."obs = '".$valueObject->getObs()."'";
          $sql = $sql." WHERE (afiid = ".$valueObject->getAfiid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {
          if (!$valueObject->getAfiid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM afi WHERE (afiid = ".$valueObject->getAfiid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM afi";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM afi";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll(); 

          foreach($result as $row) {
                $allRows = $row[0];
          }

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM afi WHERE 1=1 ";

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getPapellido() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND papellido LIKE '".$valueObject->getPapellido()."%' ";
          }

          if ($valueObject->getSapellido() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND sapellido LIKE '".$valueObject->getSapellido()."%' ";
          }

          if ($valueObject->getPnombre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pnombre LIKE '".$valueObject->getPnombre()."%' ";
          }

          if ($valueObject->getSnombre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND snombre LIKE '".$valueObject->getSnombre()."%' ";
          }

          if ($valueObject->getTipodoc() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipodoc LIKE '".$valueObject->getTipodoc()."%' ";
          }

          if ($valueObject->getDocidafiliado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND docidafiliado LIKE '".$valueObject->getDocidafiliado()."%' ";
          }

          if ($valueObject->getEmail() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND email LIKE '".$valueObject->getEmail()."%' ";
          }

          if ($valueObject->getCelular() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND celular LIKE '".$valueObject->getCelular()."%' ";
          }

          if ($valueObject->getTelefonores() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND telefonores LIKE '".$valueObject->getTelefonores()."%' ";
          }

          if ($valueObject->getTelefonoofi() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND telefonoofi LIKE '".$valueObject->getTelefonoofi()."%' ";
          }

          if ($valueObject->getIdalterna() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND idalterna LIKE '".$valueObject->getIdalterna()."%' ";
          }

          if ($valueObject->getFechanacimiento() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechanacimiento = '".$valueObject->getFechanacimiento()."' ";
          }

          if ($valueObject->getGruposanguineo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND gruposanguineo LIKE '".$valueObject->getGruposanguineo()."%' ";
          }

          if ($valueObject->getEstadocivil() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estadocivil LIKE '".$valueObject->getEstadocivil()."%' ";
          }

          if ($valueObject->getGrupoetnico() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND grupoetnico LIKE '".$valueObject->getGrupoetnico()."%' ";
          }

          if ($valueObject->getSexo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND sexo LIKE '".$valueObject->getSexo()."%' ";
          }

          if ($valueObject->getLocalidad() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND localidad LIKE '".$valueObject->getLocalidad()."%' ";
          }

          if ($valueObject->getTipozona() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tipozona = ".$valueObject->getTipozona()." ";
          }

          if ($valueObject->getDireccion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND direccion LIKE '".$valueObject->getDireccion()."%' ";
          }

          if ($valueObject->getCpostal() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND cpostal LIKE '".$valueObject->getCpostal()."%' ";
          }

          if ($valueObject->getCiuid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ciuid = ".$valueObject->getCiuid()." ";
          }

          if ($valueObject->getEstado() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND estado = ".$valueObject->getEstado()." ";
          }

          if ($valueObject->getOcuid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ocuid = ".$valueObject->getOcuid()." ";
          }

          if ($valueObject->getTercid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tercid = ".$valueObject->getTercid()." ";
          }

          if ($valueObject->getNivel() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND nivel LIKE '".$valueObject->getNivel()."%' ";
          }

          if ($valueObject->getFechaultimavis() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechaultimavis = '".$valueObject->getFechaultimavis()."' ";
          }

          if ($valueObject->getIncapacidadlaboral() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND incapacidadlaboral = ".$valueObject->getIncapacidadlaboral()." ";
          }

          if ($valueObject->getTipodiscapacidad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tipodiscapacidad = ".$valueObject->getTipodiscapacidad()." ";
          }

          if ($valueObject->getGradodiscapacidad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND gradodiscapacidad = ".$valueObject->getGradodiscapacidad()." ";
          }

          if ($valueObject->getTipoafiliado() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tipoafiliado = ".$valueObject->getTipoafiliado()." ";
          }

          if ($valueObject->getNocaso() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND nocaso = ".$valueObject->getNocaso()." ";
          }

          if ($valueObject->getRemitidopor() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND remitidopor LIKE '".$valueObject->getRemitidopor()."%' ";
          }

          if ($valueObject->getObs() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND obs LIKE '".$valueObject->getObs()."%' ";
          }


          $sql = $sql."ORDER BY afiid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){

                   $valueObject->setAfiid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setPapellido($row[2]); 
                   $valueObject->setSapellido($row[3]); 
                   $valueObject->setPnombre($row[4]); 
                   $valueObject->setSnombre($row[5]); 
                   $valueObject->setTipodoc($row[6]); 
                   $valueObject->setDocidafiliado($row[7]); 
                   $valueObject->setEmail($row[8]); 
                   $valueObject->setCelular($row[9]); 
                   $valueObject->setTelefonores($row[10]); 
                   $valueObject->setTelefonoofi($row[11]); 
                   $valueObject->setIdalterna($row[12]); 
                   $valueObject->setFechanacimiento($row[13]); 
                   $valueObject->setGruposanguineo($row[14]); 
                   $valueObject->setEstadocivil($row[15]); 
                   $valueObject->setGrupoetnico($row[16]); 
                   $valueObject->setSexo($row[17]); 
                   $valueObject->setLocalidad($row[18]); 
                   $valueObject->setTipozona($row[19]); 
                   $valueObject->setDireccion($row[20]); 
                   $valueObject->setCpostal($row[21]); 
                   $valueObject->setCiuid($row[22]); 
                   $valueObject->setEstado($row[23]); 
                   $valueObject->setOcuid($row[24]); 
                   $valueObject->setTercid($row[25]); 
                   $valueObject->setNivel($row[26]); 
                   $valueObject->setFechaultimavis($row[27]); 
                   $valueObject->setIncapacidadlaboral($row[28]); 
                   $valueObject->setTipodiscapacidad($row[29]); 
                   $valueObject->setGradodiscapacidad($row[30]); 
                   $valueObject->setTipoafiliado($row[31]); 
                   $valueObject->setNocaso($row[32]); 
                   $valueObject->setRemitidopor($row[33]); 
                   $valueObject->setObs($row[34]);
                   $valida=true;
        //          } else {
        //               //print " Object Not Found!";
        //               return false;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {    
          // while ($row = $conn->nextRow($result)) {
               $temp = $this->createValueObject();

               $temp->setAfiid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setPapellido($row[2]); 
               $temp->setSapellido($row[3]); 
               $temp->setPnombre($row[4]); 
               $temp->setSnombre($row[5]); 
               $temp->setTipodoc($row[6]); 
               $temp->setDocidafiliado($row[7]); 
               $temp->setEmail($row[8]); 
               $temp->setCelular($row[9]); 
               $temp->setTelefonores($row[10]); 
               $temp->setTelefonoofi($row[11]); 
               $temp->setIdalterna($row[12]); 
               $temp->setFechanacimiento($row[13]); 
               $temp->setGruposanguineo($row[14]); 
               $temp->setEstadocivil($row[15]); 
               $temp->setGrupoetnico($row[16]); 
               $temp->setSexo($row[17]); 
               $temp->setLocalidad($row[18]); 
               $temp->setTipozona($row[19]); 
               $temp->setDireccion($row[20]); 
               $temp->setCpostal($row[21]); 
               $temp->setCiuid($row[22]); 
               $temp->setEstado($row[23]); 
               $temp->setOcuid($row[24]); 
               $temp->setTercid($row[25]); 
               $temp->setNivel($row[26]); 
               $temp->setFechaultimavis($row[27]); 
               $temp->setIncapacidadlaboral($row[28]); 
               $temp->setTipodiscapacidad($row[29]); 
               $temp->setGradodiscapacidad($row[30]); 
               $temp->setTipoafiliado($row[31]); 
               $temp->setNocaso($row[32]); 
               $temp->setRemitidopor($row[33]); 
               $temp->setObs($row[34]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
    
    function fn_getAtenciones(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getObjecstAte($conn);
        return $searchResults;
    }
}

?>