<?php


 /**
  * Ser Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Ser object instances.
  * Créé par Ing. FMBM 2018.11.05
  * Revu et validé par Ing. FMBM  
  */

class SerDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    function SerDao() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Ser();
    }

    function getObject(&$conn, $serid) {

          $valueObject = $this->createValueObject();
          $valueObject->setSerid($serid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getSerid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM ser WHERE (serid = ".$valueObject->getSerid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM ser ORDER BY serid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO ser ( saas_ciaid, preid, codservicio, ";
          $sql = $sql."idalterna, nomservicio, nomgenerico, ";
          $sql = $sql."prescripcion, cantidad, sexo, ";
          $sql = $sql."edadinicial, edadfinal, cobrable, ";
          $sql = $sql."pyp, procedimiento, manualt, ";
          $sql = $sql."tratamiento, tipomedicamento, formafarma, ";
          $sql = $sql."concentracion, umedida, obs, ";
          $sql = $sql."rencrid, estado) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."".$valueObject->getPreid().", ";
          $sql = $sql."'".$valueObject->getCodservicio()."', ";
          $sql = $sql."'".$valueObject->getIdalterna()."', ";
          $sql = $sql."'".$valueObject->getNomservicio()."', ";
          $sql = $sql."'".$valueObject->getNomgenerico()."', ";
          $sql = $sql."'".$valueObject->getPrescripcion()."', ";
          $sql = $sql."".$valueObject->getCantidad().", ";
          $sql = $sql."'".$valueObject->getSexo()."', ";
          $sql = $sql."".$valueObject->getEdadinicial().", ";
          $sql = $sql."".$valueObject->getEdadfinal().", ";
          $sql = $sql."".$valueObject->getCobrable().", ";
          $sql = $sql."".$valueObject->getPyp().", ";
          $sql = $sql."".$valueObject->getProcedimiento().", ";
          $sql = $sql."".$valueObject->getManualt().", ";
          $sql = $sql."".$valueObject->getTratamiento().", ";
          $sql = $sql."".$valueObject->getTipomedicamento().", ";
          $sql = $sql."'".$valueObject->getFormafarma()."', ";
          $sql = $sql."'".$valueObject->getConcentracion()."', ";
          $sql = $sql."'".$valueObject->getUmedida()."', ";
          $sql = $sql."'".$valueObject->getObs()."', ";
          $sql = $sql."".$valueObject->getRencrid().", ";
          $sql = $sql."'".$valueObject->getEstado()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {

                   $valueObject->setSerid($row[0]); 
                   $existe = true;

          } 

          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE ser SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."preid = ".$valueObject->getPreid().", ";
          $sql = $sql."codservicio = '".$valueObject->getCodservicio()."', ";
          $sql = $sql."idalterna = '".$valueObject->getIdalterna()."', ";
          $sql = $sql."nomservicio = '".$valueObject->getNomservicio()."', ";
          $sql = $sql."nomgenerico = '".$valueObject->getNomgenerico()."', ";
          $sql = $sql."prescripcion = '".$valueObject->getPrescripcion()."', ";
          $sql = $sql."cantidad = ".$valueObject->getCantidad().", ";
          $sql = $sql."sexo = '".$valueObject->getSexo()."', ";
          $sql = $sql."edadinicial = ".$valueObject->getEdadinicial().", ";
          $sql = $sql."edadfinal = ".$valueObject->getEdadfinal().", ";
          $sql = $sql."cobrable = ".$valueObject->getCobrable().", ";
          $sql = $sql."pyp = ".$valueObject->getPyp().", ";
          $sql = $sql."procedimiento = ".$valueObject->getProcedimiento().", ";
          $sql = $sql."manualt = ".$valueObject->getManualt().", ";
          $sql = $sql."tratamiento = ".$valueObject->getTratamiento().", ";
          $sql = $sql."tipomedicamento = ".$valueObject->getTipomedicamento().", ";
          $sql = $sql."formafarma = '".$valueObject->getFormafarma()."', ";
          $sql = $sql."concentracion = '".$valueObject->getConcentracion()."', ";
          $sql = $sql."umedida = '".$valueObject->getUmedida()."', ";
          $sql = $sql."obs = '".$valueObject->getObs()."', ";
          $sql = $sql."rencrid = ".$valueObject->getRencrid().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."'";
          $sql = $sql." WHERE (serid = ".$valueObject->getSerid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getSerid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM ser WHERE (serid = ".$valueObject->getSerid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {

          $sql = "DELETE FROM ser";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM ser";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll(); 

          foreach($result as $row) {
                $allRows = $row[0];
          }      

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM ser WHERE 1=1 ";

          if ($valueObject->getSerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND serid = ".$valueObject->getSerid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getPreid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND preid = ".$valueObject->getPreid()." ";
          }

          if ($valueObject->getCodservicio() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND codservicio LIKE '".$valueObject->getCodservicio()."%' ";
          }

          if ($valueObject->getIdalterna() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND idalterna LIKE '".$valueObject->getIdalterna()."%' ";
          }

          if ($valueObject->getNomservicio() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND nomservicio LIKE '".$valueObject->getNomservicio()."%' ";
          }

          if ($valueObject->getNomgenerico() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND nomgenerico LIKE '".$valueObject->getNomgenerico()."%' ";
          }

          if ($valueObject->getPrescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND prescripcion LIKE '".$valueObject->getPrescripcion()."%' ";
          }

          if ($valueObject->getCantidad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cantidad = ".$valueObject->getCantidad()." ";
          }

          if ($valueObject->getSexo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND sexo LIKE '".$valueObject->getSexo()."%' ";
          }

          if ($valueObject->getEdadinicial() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND edadinicial = ".$valueObject->getEdadinicial()." ";
          }

          if ($valueObject->getEdadfinal() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND edadfinal = ".$valueObject->getEdadfinal()." ";
          }

          if ($valueObject->getCobrable() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cobrable = ".$valueObject->getCobrable()." ";
          }

          if ($valueObject->getPyp() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pyp = ".$valueObject->getPyp()." ";
          }

          if ($valueObject->getProcedimiento() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND procedimiento = ".$valueObject->getProcedimiento()." ";
          }

          if ($valueObject->getManualt() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND manualt = ".$valueObject->getManualt()." ";
          }

          if ($valueObject->getTratamiento() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tratamiento = ".$valueObject->getTratamiento()." ";
          }

          if ($valueObject->getTipomedicamento() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tipomedicamento = ".$valueObject->getTipomedicamento()." ";
          }

          if ($valueObject->getFormafarma() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND formafarma LIKE '".$valueObject->getFormafarma()."%' ";
          }

          if ($valueObject->getConcentracion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND concentracion LIKE '".$valueObject->getConcentracion()."%' ";
          }

          if ($valueObject->getUmedida() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND umedida LIKE '".$valueObject->getUmedida()."%' ";
          }

          if ($valueObject->getObs() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND obs LIKE '".$valueObject->getObs()."%' ";
          }

          if ($valueObject->getRencrid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND rencrid = ".$valueObject->getRencrid()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }


          $sql = $sql."ORDER BY serid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {

          $query  = $conn->prepare($sql);
          $result = $query->execute();

          $this->resetCache();

          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){

                   $valueObject->setSerid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setPreid($row[2]); 
                   $valueObject->setCodservicio($row[3]); 
                   $valueObject->setIdalterna($row[4]); 
                   $valueObject->setNomservicio($row[5]); 
                   $valueObject->setNomgenerico($row[6]); 
                   $valueObject->setPrescripcion($row[7]); 
                   $valueObject->setCantidad($row[8]); 
                   $valueObject->setSexo($row[9]); 
                   $valueObject->setEdadinicial($row[10]); 
                   $valueObject->setEdadfinal($row[11]); 
                   $valueObject->setCobrable($row[12]); 
                   $valueObject->setPyp($row[13]); 
                   $valueObject->setProcedimiento($row[14]); 
                   $valueObject->setManualt($row[15]); 
                   $valueObject->setTratamiento($row[16]); 
                   $valueObject->setTipomedicamento($row[17]); 
                   $valueObject->setFormafarma($row[18]); 
                   $valueObject->setConcentracion($row[19]); 
                   $valueObject->setUmedida($row[20]); 
                   $valueObject->setObs($row[21]); 
                   $valueObject->setRencrid($row[22]); 
                   $valueObject->setEstado($row[23]); 
                   $valida=true;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setSerid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setPreid($row[2]); 
               $temp->setCodservicio($row[3]); 
               $temp->setIdalterna($row[4]); 
               $temp->setNomservicio($row[5]); 
               $temp->setNomgenerico($row[6]); 
               $temp->setPrescripcion($row[7]); 
               $temp->setCantidad($row[8]); 
               $temp->setSexo($row[9]); 
               $temp->setEdadinicial($row[10]); 
               $temp->setEdadfinal($row[11]); 
               $temp->setCobrable($row[12]); 
               $temp->setPyp($row[13]); 
               $temp->setProcedimiento($row[14]); 
               $temp->setManualt($row[15]); 
               $temp->setTratamiento($row[16]); 
               $temp->setTipomedicamento($row[17]); 
               $temp->setFormafarma($row[18]); 
               $temp->setConcentracion($row[19]); 
               $temp->setUmedida($row[20]); 
               $temp->setObs($row[21]); 
               $temp->setRencrid($row[22]); 
               $temp->setEstado($row[23]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
}

?>