<?php
 /**
  * Omed Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Omed object instances. 
  * 
  * Créé par Ing. FMBM 2018.11.05
  * Revu et validé par Ing. FMBM 
  */

class OmedDao {

    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Omed();
    }

    function getObject(&$conn, $omedid) {
          $valueObject = $this->createValueObject();
          $valueObject->setOmedid($omedid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getOmedid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM omed WHERE (omedid = ".$valueObject->getOmedid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM omed ORDER BY omedid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          // Validar los valores cargados en los atributos del objeto
          // si tienen valor se colocan en la consulta si no no
          // Tal como el método searchmatching
          // Validaciones de claves foráneas y atributos que empiecen nulos

          $sql = "INSERT INTO omed ( omeid, noitem, serid, ";
          $sql = $sql."cantidad, impreso, comentarios, ";
          $sql = $sql."fechares, resultados, ";
          $sql = $sql."procid) VALUES (".$valueObject->getOmeid().", ";
          $sql = $sql."".$valueObject->getNoitem().", ";
          $sql = $sql."".$valueObject->getSerid().", ";
          $sql = $sql."".$valueObject->getCantidad().", ";
          $sql = $sql."".$valueObject->getImpreso().", ";
          $sql = $sql."'".$valueObject->getComentarios()."', ";
          $sql = $sql."'".$valueObject->getFechares()."', ";
          $sql = $sql."'".$valueObject->getResultados()."', ";
          $sql = $sql."".$valueObject->getProcid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {
                   $valueObject->setOmedid($row[0]); 
                   $existe = true;
          }

          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE omed SET omeid = ".$valueObject->getOmeid().", ";
          $sql = $sql."noitem = ".$valueObject->getNoitem().", ";
          $sql = $sql."serid = ".$valueObject->getSerid().", ";
          $sql = $sql."cantidad = ".$valueObject->getCantidad().", ";
          $sql = $sql."impreso = ".$valueObject->getImpreso().", ";
          $sql = $sql."comentarios = '".$valueObject->getComentarios()."', ";
          $sql = $sql."fechares = '".$valueObject->getFechares()."', ";
          $sql = $sql."resultados = '".$valueObject->getResultados()."', ";
          $sql = $sql."procid = ".$valueObject->getProcid()."";
          $sql = $sql." WHERE (omedid = ".$valueObject->getOmedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getOmedid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM omed WHERE (omedid = ".$valueObject->getOmedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {

          $sql = "DELETE FROM omed";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM omed";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll(); 

          foreach($result as $row) {
                $allRows = $row[0];
          }      

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM omed WHERE 1=1 ";

          if ($valueObject->getOmedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND omedid = ".$valueObject->getOmedid()." ";
          }

          if ($valueObject->getOmeid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND omeid = ".$valueObject->getOmeid()." ";
          }

          if ($valueObject->getNoitem() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND noitem = ".$valueObject->getNoitem()." ";
          }

          if ($valueObject->getSerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND serid = ".$valueObject->getSerid()." ";
          }

          if ($valueObject->getCantidad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cantidad = ".$valueObject->getCantidad()." ";
          }

          if ($valueObject->getImpreso() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND impreso = ".$valueObject->getImpreso()." ";
          }

          if ($valueObject->getComentarios() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND comentarios LIKE '".$valueObject->getComentarios()."%' ";
          }

          if ($valueObject->getFechares() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechares = '".$valueObject->getFechares()."' ";
          }

          if ($valueObject->getResultados() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND resultados LIKE '".$valueObject->getResultados()."%' ";
          }

          if ($valueObject->getProcid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND procid = ".$valueObject->getProcid()." ";
          }


          $sql = $sql."ORDER BY omedid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {

          $query  = $conn->prepare($sql);
          $result = $query->execute();

          $this->resetCache();

          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){ 

             $valueObject->setOmedid($row[0]); 
             $valueObject->setOmeid($row[1]); 
             $valueObject->setNoitem($row[2]); 
             $valueObject->setSerid($row[3]); 
             $valueObject->setCantidad($row[4]); 
             $valueObject->setImpreso($row[5]); 
             $valueObject->setComentarios($row[6]); 
             $valueObject->setFechares($row[7]); 
             $valueObject->setResultados($row[8]);  
             $valueObject->setProcid($row[9]); 
             $valida=true;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setOmedid($row[0]); 
               $temp->setOmeid($row[1]); 
               $temp->setNoitem($row[2]); 
               $temp->setSerid($row[3]); 
               $temp->setCantidad($row[4]); 
               $temp->setImpreso($row[5]); 
               $temp->setComentarios($row[6]); 
               $temp->setFechares($row[7]); 
               $temp->setResultados($row[8]); 
               $temp->setProcid($row[9]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }

    // Métodos relacionales

    // Relation avec la table ome
    function loadObjectOme(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectOme($conn);
        return $searchResults;
    }

    // Relation avec la table ser
    function loadObjectSer(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectSer($conn);
        return $searchResults;
    }

    // Relation avec la table oxpid
    // function loadObjectPieza(&$conn,&$valueObject) {
    //     $searchResults = $valueObject->getObjectPieza($conn);
    //     return $searchResults;
    // }

    // // Relation avec la table oxmar
    // function loadObjectMarca(&$conn,&$valueObject) {
    //     $searchResults = $valueObject->getObjectMarca($conn);
    //     return $searchResults;
    // }

    // // Relation avec la table med
    // function loadObjectMed(&$conn,&$valueObject) {
    //     $searchResults = $valueObject->getObjectMed($conn);
    //     return $searchResults;
    // }

    // // Relation avec la table oxmar
    // function loadObjectOdon(&$conn,&$valueObject) {
    //     $searchResults = $valueObject->getObjectOdon($conn);
    //     return $searchResults;
    // }

    // Consulta de campos de tablas relacionadas
    function getFechaOrdenM(&$conn,&$valueObject) {
        $fechaOme=null;
        $sql   = "SELECT fechaome FROM ome WHERE omeid=" . $valueObject->getOmeid();
        $query = $conn->prepare($sql); 
        $query->execute();  
        $result = $query->fetchAll();
              
        foreach ($result as $row) {
             $fechaOme = $row["fechaome"]; 
        }
        return $fechaOme; 
    }

    // Consulta de valores de tabla relacional
    function getDescServ(&$conn,&$valueObject) {
        $nomServicio = ""; 
        $sql   = "SELECT nomservicio FROM ser WHERE serid=" . $valueObject->getSerid();
        $query = $conn->prepare($sql); 
        $query->execute();  
        $result = $query->fetchAll();
              
        foreach ($result as $row) {
             $nomServicio = $row["nomservicio"]; 
        }
        return $nomServicio; 
    }

    // // Consulta de valores de tabla relacional
    // function getCodDiente(&$conn,&$valueObject) {
    //     $diente = ""; 
    //     $sql   = "SELECT codpieza FROM oxpid WHERE pidid=" . $valueObject->getPidid();
    //     $query = $conn->prepare($sql); 
    //     $query->execute();  
    //     $result = $query->fetchAll();
              
    //     foreach ($result as $row) {
    //          $diente = $row["codpieza"]; 
    //     }
    //     return $diente; 
    // }

    // // Consulta de valores de tabla relacional
    // function getCodMarca(&$conn,&$valueObject) {
    //     $codMarca = ""; 
    //     $sql   = "SELECT codmarca FROM oxmar WHERE marid=" . $valueObject->getMarid();
    //     $query = $conn->prepare($sql); 
    //     $query->execute();  
    //     $result = $query->fetchAll();
              
    //     foreach ($result as $row) {
    //          $codMarca = $row["codmarca"]; 
    //     }
    //     return $codMarca; 
    // }

    // // Consulta de valores de tabla relacional
    // function getNomMed(&$conn,&$valueObject) {
    //     $nombreM = ""; 
    //     $sql   = "SELECT nombre FROM med WHERE medid=" . $valueObject->getIdmedicorealiza();
    //     $query = $conn->prepare($sql); 
    //     $query->execute();  
    //     $result = $query->fetchAll();
              
    //     foreach ($result as $row) {
    //          $nombreM = $row["nombre"]; 
    //     }
    //     return $nombreM; 
    // } 

}

?>