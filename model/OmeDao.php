<?php

 /**
  * Ome Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Ome object instances. 
  * Créé par Ing. FMBM 2018.11.05
  * Revu et validé par Ing. FMBM 
  */

class OmeDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    function OmeDao() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Ome();
    }

    function getObject(&$conn, $omeid) {

          $valueObject = $this->createValueObject();
          $valueObject->setOmeid($omeid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getOmeid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM ome WHERE (omeid = ".$valueObject->getOmeid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM ome ORDER BY omeid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO ome ( saas_ciaid, consecutivo, ";
          $sql = $sql."edad, preid, urgencia, ";
          $sql = $sql."proid, fecharesultados, causaexterna, ";
          $sql = $sql."ambito, finalidad, imprimedx, ";
          $sql = $sql."impreso, afiid, citid, ";
          $sql = $sql."ateid, usrid) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."'".$valueObject->getConsecutivo()."', ";
          $sql = $sql."".$valueObject->getEdad().", ";
          $sql = $sql."".$valueObject->getPreid().", ";
          $sql = $sql."".$valueObject->getUrgencia().", ";
          $sql = $sql."".$valueObject->getProid().", ";
          $sql = $sql."'".$valueObject->getFecharesultados()."', ";
          $sql = $sql."'".$valueObject->getCausaexterna()."', ";
          $sql = $sql."'".$valueObject->getAmbito()."', ";
          $sql = $sql."'".$valueObject->getFinalidad()."', ";
          $sql = $sql."".$valueObject->getImprimedx().", ";
          $sql = $sql."".$valueObject->getImpreso().", ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."".$valueObject->getCitid().", ";
          $sql = $sql."".$valueObject->getAteid().", ";
          $sql = $sql."".$valueObject->getUsrid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {

                   $valueObject->setOmeid($row[0]); 
                   $existe = true;

          }

          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE ome SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."consecutivo = '".$valueObject->getConsecutivo()."', ";
          $sql = $sql."fechaome = '".$valueObject->getFechaome()."', ";
          $sql = $sql."edad = ".$valueObject->getEdad().", ";
          $sql = $sql."preid = ".$valueObject->getPreid().", ";
          $sql = $sql."urgencia = ".$valueObject->getUrgencia().", ";
          $sql = $sql."proid = ".$valueObject->getProid().", ";
          $sql = $sql."fecharesultados = '".$valueObject->getFecharesultados()."', ";
          $sql = $sql."causaexterna = '".$valueObject->getCausaexterna()."', ";
          $sql = $sql."ambito = '".$valueObject->getAmbito()."', ";
          $sql = $sql."finalidad = '".$valueObject->getFinalidad()."', ";
          $sql = $sql."imprimedx = ".$valueObject->getImprimedx().", ";
          $sql = $sql."impreso = ".$valueObject->getImpreso().", ";
          $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
          $sql = $sql."citid = ".$valueObject->getCitid().", ";
          $sql = $sql."ateid = ".$valueObject->getAteid().", ";
          $sql = $sql."usrid = ".$valueObject->getUsrid()."";
          $sql = $sql." WHERE (omeid = ".$valueObject->getOmeid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getOmeid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM ome WHERE (omeid = ".$valueObject->getOmeid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {

          $sql = "DELETE FROM ome";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM ome";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll(); 

          foreach($result as $row) {
                $allRows = $row[0];
          }      

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM ome WHERE 1=1 ";

          if ($valueObject->getOmeid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND omeid = ".$valueObject->getOmeid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getConsecutivo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND consecutivo LIKE '".$valueObject->getConsecutivo()."%' ";
          }

          if ($valueObject->getFechaome() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechaome = '".$valueObject->getFechaome()."' ";
          }

          if ($valueObject->getEdad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND edad = ".$valueObject->getEdad()." ";
          }

          if ($valueObject->getPreid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND preid = ".$valueObject->getPreid()." ";
          }

          if ($valueObject->getUrgencia() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND urgencia = ".$valueObject->getUrgencia()." ";
          }

          if ($valueObject->getProid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND proid = ".$valueObject->getProid()." ";
          }

          if ($valueObject->getFecharesultados() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecharesultados = '".$valueObject->getFecharesultados()."' ";
          }

          if ($valueObject->getCausaexterna() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND causaexterna LIKE '".$valueObject->getCausaexterna()."%' ";
          }

          if ($valueObject->getAmbito() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND ambito LIKE '".$valueObject->getAmbito()."%' ";
          }

          if ($valueObject->getFinalidad() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND finalidad LIKE '".$valueObject->getFinalidad()."%' ";
          }

          if ($valueObject->getImprimedx() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND imprimedx = ".$valueObject->getImprimedx()." ";
          }

          if ($valueObject->getImpreso() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND impreso = ".$valueObject->getImpreso()." ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getAteid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ateid = ".$valueObject->getAteid()." ";
          }

          if ($valueObject->getUsrid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND usrid = ".$valueObject->getUsrid()." ";
          }


          $sql = $sql."ORDER BY omeid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {

          $query  = $conn->prepare($sql);
          $result = $query->execute();

          $this->resetCache();

          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){ 

                   $valueObject->setOmeid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setConsecutivo($row[2]); 
                   $valueObject->setFechaome($row[3]); 
                   $valueObject->setEdad($row[4]); 
                   $valueObject->setPreid($row[5]); 
                   $valueObject->setUrgencia($row[6]); 
                   $valueObject->setProid($row[7]); 
                   $valueObject->setFecharesultados($row[8]); 
                   $valueObject->setCausaexterna($row[9]); 
                   $valueObject->setAmbito($row[10]); 
                   $valueObject->setFinalidad($row[11]); 
                   $valueObject->setImprimedx($row[12]); 
                   $valueObject->setImpreso($row[13]); 
                   $valueObject->setAfiid($row[14]); 
                   $valueObject->setAteid($row[15]); 
                   $valueObject->setUsrid($row[16]);
                   $valueObject->setCitid($row[17]);  
                   $valida=true;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setOmeid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setConsecutivo($row[2]); 
               $temp->setFechaome($row[3]); 
               $temp->setEdad($row[4]); 
               $temp->setPreid($row[5]); 
               $temp->setUrgencia($row[6]); 
               $temp->setProid($row[7]); 
               $temp->setFecharesultados($row[8]); 
               $temp->setCausaexterna($row[9]); 
               $temp->setAmbito($row[10]); 
               $temp->setFinalidad($row[11]); 
               $temp->setImprimedx($row[12]); 
               $temp->setImpreso($row[13]); 
               $temp->setAfiid($row[14]); 
               $temp->setAteid($row[15]); 
               $temp->setUsrid($row[16]); 
               $temp->setCitid($row[17]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
}

?>