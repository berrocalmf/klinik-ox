<?php
 /**
  * Ppto Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Ppto object instances. 
  */

class PptoDao {
    // Cache contents:
    var $cacheOk;
    var $cacheData;
    
    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Ppto();
    }

    function getObject(&$conn, $pptoid) {
          $valueObject = $this->createValueObject();
          $valueObject->setPptoid($pptoid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getPptoid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM ppto WHERE (pptoid = ".$valueObject->getPptoid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }
    
    function loadAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM ppto ORDER BY pptoid ASC ";
          $searchResults = $this->listQuery($conn, $sql);
          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;
          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO ppto ( cnspresupuesto, fechappto, afiid, ";
          $sql = $sql."ateid, obs, vlrtotalppto, ";
          $sql = $sql."vlrpptocopago, vlrrealppto, vlrrealcopago, ";
          $sql = $sql."vlrsaldoppto, vlrsaldocopago, citid, ";
          $sql = $sql."customerid, usrid, estado, ";
          $sql = $sql."motivoAnula) VALUES ('".$valueObject->getCnspresupuesto()."', ";
          $sql = $sql."'".$valueObject->getFechappto()."', ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."".$valueObject->getAteid().", ";
          $sql = $sql."'".$valueObject->getObs()."', ";
          $sql = $sql."'".$valueObject->getVlrtotalppto()."', ";
          $sql = $sql."'".$valueObject->getVlrpptocopago()."', ";
          $sql = $sql."'".$valueObject->getVlrrealppto()."', ";
          $sql = $sql."'".$valueObject->getVlrrealcopago()."', ";
          $sql = $sql."'".$valueObject->getVlrsaldoppto()."', ";
          $sql = $sql."'".$valueObject->getVlrsaldocopago()."', ";
          $sql = $sql."".$valueObject->getCitid().", ";
          $sql = $sql."".$valueObject->getCustomerid().", ";
          $sql = $sql."".$valueObject->getUsrid().", ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."'".$valueObject->getMotivoAnula()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $existe = false;

          foreach($result as $row) {
               $valueObject->setPptoid($row[0]); 
               $existe = true;
          }
          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE ppto SET cnspresupuesto = '".$valueObject->getCnspresupuesto()."', ";
          $sql = $sql."fechappto = '".$valueObject->getFechappto()."', ";
          $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
          $sql = $sql."ateid = ".$valueObject->getAteid().", ";
          $sql = $sql."obs = '".$valueObject->getObs()."', ";
          $sql = $sql."vlrtotalppto = '".$valueObject->getVlrtotalppto()."', ";
          $sql = $sql."vlrpptocopago = '".$valueObject->getVlrpptocopago()."', ";
          $sql = $sql."vlrrealppto = '".$valueObject->getVlrrealppto()."', ";
          $sql = $sql."vlrrealcopago = '".$valueObject->getVlrrealcopago()."', ";
          $sql = $sql."vlrsaldoppto = '".$valueObject->getVlrsaldoppto()."', ";
          $sql = $sql."vlrsaldocopago = '".$valueObject->getVlrsaldocopago()."', ";
          $sql = $sql."citid = ".$valueObject->getCitid().", ";
          $sql = $sql."customerid = ".$valueObject->getCustomerid().", ";
          $sql = $sql."usrid = ".$valueObject->getUsrid().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."motivoAnula = '".$valueObject->getMotivoAnula()."'";
          $sql = $sql." WHERE (pptoid = ".$valueObject->getPptoid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function delete(&$conn, &$valueObject) {
          if (!$valueObject->getPptoid()) {
               return false;
          }

          $sql = "DELETE FROM ppto WHERE (pptoid = ".$valueObject->getPptoid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM ppto";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM ppto";
          $allRows = 0;
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          
          foreach($result as $row) {
                $allRows = $row[0];
          }      
          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM ppto WHERE 1=1 ";

          if ($valueObject->getPptoid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pptoid = ".$valueObject->getPptoid()." ";
          }

          if ($valueObject->getCnspresupuesto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND cnspresupuesto LIKE '".$valueObject->getCnspresupuesto()."%' ";
          }

          if ($valueObject->getFechappto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechappto = '".$valueObject->getFechappto()."' ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getAteid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ateid = ".$valueObject->getAteid()." ";
          }

          if ($valueObject->getObs() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND obs LIKE '".$valueObject->getObs()."%' ";
          }

          if ($valueObject->getVlrtotalppto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrtotalppto = '".$valueObject->getVlrtotalppto()."' ";
          }

          if ($valueObject->getVlrpptocopago() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrpptocopago = '".$valueObject->getVlrpptocopago()."' ";
          }

          if ($valueObject->getVlrrealppto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrrealppto = '".$valueObject->getVlrrealppto()."' ";
          }

          if ($valueObject->getVlrrealcopago() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrrealcopago = '".$valueObject->getVlrrealcopago()."' ";
          }

          if ($valueObject->getVlrsaldoppto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrsaldoppto = '".$valueObject->getVlrsaldoppto()."' ";
          }

          if ($valueObject->getVlrsaldocopago() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrsaldocopago = '".$valueObject->getVlrsaldocopago()."' ";
          }

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getCustomerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND customerid = ".$valueObject->getCustomerid()." ";
          }

          if ($valueObject->getUsrid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND usrid = ".$valueObject->getUsrid()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getMotivoAnula() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivoAnula LIKE '".$valueObject->getMotivoAnula()."%' ";
          }

          $sql = $sql."ORDER BY pptoid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);
          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $valida = false;

          foreach($result as $row){ 
                   $valueObject->setPptoid($row[0]); 
                   $valueObject->setCnspresupuesto($row[1]); 
                   $valueObject->setFechappto($row[2]); 
                   $valueObject->setAfiid($row[3]); 
                   $valueObject->setAteid($row[4]); 
                   $valueObject->setObs($row[5]); 
                   $valueObject->setVlrtotalppto($row[6]); 
                   $valueObject->setVlrpptocopago($row[7]); 
                   $valueObject->setVlrrealppto($row[8]); 
                   $valueObject->setVlrrealcopago($row[9]); 
                   $valueObject->setVlrsaldoppto($row[10]); 
                   $valueObject->setVlrsaldocopago($row[11]); 
                   $valueObject->setCitid($row[12]); 
                   $valueObject->setCustomerid($row[13]); 
                   $valueObject->setUsrid($row[14]); 
                   $valueObject->setEstado($row[15]); 
                   $valueObject->setMotivoAnula($row[16]); 
                   $valida=true;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setPptoid($row[0]); 
               $temp->setCnspresupuesto($row[1]); 
               $temp->setFechappto($row[2]); 
               $temp->setAfiid($row[3]); 
               $temp->setAteid($row[4]); 
               $temp->setObs($row[5]); 
               $temp->setVlrtotalppto($row[6]); 
               $temp->setVlrpptocopago($row[7]); 
               $temp->setVlrrealppto($row[8]); 
               $temp->setVlrrealcopago($row[9]); 
               $temp->setVlrsaldoppto($row[10]); 
               $temp->setVlrsaldocopago($row[11]); 
               $temp->setCitid($row[12]); 
               $temp->setCustomerid($row[13]); 
               $temp->setUsrid($row[14]); 
               $temp->setEstado($row[15]); 
               $temp->setMotivoAnula($row[16]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectAte(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAte($conn);
        return $searchResults;
    }

    // Relación Muchos a Uno AFI
    function loadObjectAfi(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAfi($conn);
        return $searchResults;
    }
    
    // Relacion muchos a uno CIT
    function loadObjectCit(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectCit($conn);
        return $searchResults;
    }  
    
    // relaciones uno a muchos
    // Renvoie un tableau avec des objets pptop (relation d'association un à plusieurs) 
    // Ok. Ing. FMBM 2019/01/30
    
    // Relación uno a muchos pptop
    function loadPagosPpto(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getPagosPpto($conn);
        return $searchResults;
    }

}

?>