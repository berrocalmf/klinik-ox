<?php

 /**
  * Pptop Data Access Object (DAO).
  * Ing. FBERROCALM - 30/Nov/218
  */
 require_once("TgedDao.php");

class PptopDao {

    const TABLATGE = 1;                         // No. de la Tabla para las configuraciones de TGEN
    
    private $cacheOk;
    private $cacheData;
    private $ContTged;

    private $arrValoresTGED=[];                 // <-- Array con valores de la tabla TGED

    public function __construct() {
        $this->ContTged = new TgedDao();
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Pptop();
    }

    function getObject(&$conn, $pptopid) {
          $valueObject = $this->createValueObject();
          $valueObject->setPptopid($pptopid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getPptopid()) {
               return false;                      // <-- print "Can not select without Primary-Key!";
          }

          $sql = "SELECT * FROM pptop WHERE (pptopid = ".$valueObject->getPptopid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject)) {
               return true;
          } else {
               return false;
          }     
    }

    function loadAll(&$conn) {

          if ($this->cacheOk) {
              return $this->cacheData;            // <-- Check the cache status and use Cache if possible.
          }

          $sql = "SELECT * FROM pptop ORDER BY pptopid ASC ";
          $searchResults = $this->listQuery($conn, $sql);
          
          $this->cacheData = $searchResults;      // <-- Update cache and mark it ready.
          $this->cacheOk = true;
          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO pptop ( pptoid, fecha, idconcepto, ";
          $sql = $sql."detallemov, tipomov, vlrmovimiento, ";
          $sql = $sql."obs, citid, customerid, ";
          $sql = $sql."usrid, estado, motivoAnula) VALUES (".$valueObject->getPptoid().", ";
          $sql = $sql."'".$valueObject->getFecha()."', ";
          $sql = $sql."'".$valueObject->getIdconcepto()."', ";
          $sql = $sql."'".$valueObject->getDetallemov()."', ";
          $sql = $sql."'".$valueObject->getTipomov()."', ";
          $sql = $sql."'".$valueObject->getVlrmovimiento()."', ";
          $sql = $sql."'".$valueObject->getObs()."', ";
          $sql = $sql."".$valueObject->getCitid().", ";
          $sql = $sql."".$valueObject->getCustomerid().", ";
          $sql = $sql."".$valueObject->getUsrid().", ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."'".$valueObject->getMotivoAnula()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $existe = false;

          foreach($result as $row) {
                   $valueObject->setPptopid($row[0]); 
                   
                   $this->setPropertyDesc("idconcepto",$valueObject,$valueObject->getIdconcepto());
                   $this->setPropertyDesc("tipomov",$valueObject,$valueObject->getTipomov());
                   $this->setPropertyDesc("estado",$valueObject,$valueObject->getEstado());

                   $existe = true;
          }

          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE pptop SET pptoid = ".$valueObject->getPptoid().", ";
          $sql = $sql."fecha = '".$valueObject->getFecha()."', ";
          $sql = $sql."idconcepto = '".$valueObject->getIdconcepto()."', ";
          $sql = $sql."detallemov = '".$valueObject->getDetallemov()."', ";
          $sql = $sql."tipomov = '".$valueObject->getTipomov()."', ";
          $sql = $sql."vlrmovimiento = '".$valueObject->getVlrmovimiento()."', ";
          $sql = $sql."obs = '".$valueObject->getObs()."', ";
          $sql = $sql."citid = ".$valueObject->getCitid().", ";
          $sql = $sql."customerid = ".$valueObject->getCustomerid().", ";
          $sql = $sql."usrid = ".$valueObject->getUsrid().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."motivoAnula = '".$valueObject->getMotivoAnula()."'";
          $sql = $sql." WHERE (pptopid = ".$valueObject->getPptopid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;    //print "PrimaryKey Error when updating DB!";
          }

          $this->setPropertyDesc("idconcepto",$valueObject,$valueObject->getIdconcepto());
          $this->setPropertyDesc("tipomov",$valueObject,$valueObject->getTipomov());
          $this->setPropertyDesc("estado",$valueObject,$valueObject->getEstado());

          return true;
    }

    function delete(&$conn, &$valueObject) {
          if (!$valueObject->getPptopid()) {
               return false;                      // <-- print "Can not delete without Primary-Key!";
          }

          $sql = "DELETE FROM pptop WHERE (pptopid = ".$valueObject->getPptopid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;                      // <-- print "PrimaryKey Error when updating DB!";
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM pptop";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM pptop";
          $allRows = 0;
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }      
          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM pptop WHERE 1=1 ";

          if ($valueObject->getPptopid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pptopid = ".$valueObject->getPptopid()." ";
          }

          if ($valueObject->getPptoid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pptoid = ".$valueObject->getPptoid()." ";
          }

          if ($valueObject->getFecha() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecha = '".$valueObject->getFecha()."' ";
          }

          if ($valueObject->getIdconcepto() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND idconcepto LIKE '".$valueObject->getIdconcepto()."%' ";
          }

          if ($valueObject->getDetallemov() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND detallemov LIKE '".$valueObject->getDetallemov()."%' ";
          }

          if ($valueObject->getTipomov() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipomov LIKE '".$valueObject->getTipomov()."%' ";
          }

          if ($valueObject->getVlrmovimiento() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrmovimiento = '".$valueObject->getVlrmovimiento()."' ";
          }

          if ($valueObject->getObs() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND obs LIKE '".$valueObject->getObs()."%' ";
          }

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getCustomerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND customerid = ".$valueObject->getCustomerid()." ";
          }

          if ($valueObject->getUsrid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND usrid = ".$valueObject->getUsrid()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getMotivoAnula() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivoAnula LIKE '".$valueObject->getMotivoAnula()."%' ";
          }


          $sql = $sql."ORDER BY pptopid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);
          return $searchResults;
    }

    // Procedure for Update Data Base table [pptpp]

    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $valida = false;

          foreach($result as $row){
                   $valueObject->setPptopid($row[0]); 
                   $valueObject->setPptoid($row[1]); 
                   $valueObject->setFecha($row[2]); 
                   $valueObject->setIdconcepto($row[3]); 
                   $valueObject->setDetallemov($row[4]); 
                   $valueObject->setTipomov($row[5]); 
                   $valueObject->setVlrmovimiento($row[6]); 
                   $valueObject->setObs($row[7]); 
                   $valueObject->setCitid($row[8]); 
                   $valueObject->setCustomerid($row[9]); 
                   $valueObject->setUsrid($row[10]); 
                   $valueObject->setEstado($row[11]); 
                   $valueObject->setMotivoAnula($row[12]);
                   
                   $this->setPropertyDesc("idconcepto",$valueObject,$valueObject->getIdconcepto());
                   $this->setPropertyDesc("tipomov",$valueObject,$valueObject->getTipomov());
                   $this->setPropertyDesc("estado",$valueObject,$valueObject->getEstado());

                   $valida=true;
          }

          return $valida;
    }

    function listQuery(&$conn, &$sql) {
          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();
               $temp->setPptopid($row[0]); 
               $temp->setPptoid($row[1]); 
               $temp->setFecha($row[2]); 
               $temp->setIdconcepto($row[3]); 
               $temp->setDetallemov($row[4]); 
               $temp->setTipomov($row[5]); 
               $temp->setVlrmovimiento($row[6]); 
               $temp->setObs($row[7]); 
               $temp->setCitid($row[8]); 
               $temp->setCustomerid($row[9]); 
               $temp->setUsrid($row[10]); 
               $temp->setEstado($row[11]); 
               $temp->setMotivoAnula($row[12]);   

              // $this->setPropertyDesc("idconcepto",$temp,$temp->getIdconcepto());
              // $this->setPropertyDesc("tipomov"   ,$temp,$temp->getTipomov());
              // $this->setPropertyDesc("estado"    ,$temp,$temp->getEstado());

               array_push($searchResults, $temp);
          }

          foreach ($searchResults as $key => $value) {
              $this->setPropertyDesc("idconcepto",$value,$value->getIdconcepto());
              $this->setPropertyDesc("tipomov"   ,$value,$value->getTipomov());
              $this->setPropertyDesc("estado"    ,$value,$value->getEstado());
          }

          return $searchResults;
    }

    /* 
      Este proceso carga en Memoria los valores de configuracion TGED para la Tabla
      Ing. FMBM 2018/12/21
    */
    function setValoresTGED(&$conn) {
          $res = $this->ContTged->getTGEDValues($conn,self::TABLATGE);
          foreach($res as $row) {
               $this->arrValoresTGED[]=$row;
          }
    }

    /* 
      Método temporal 
    */
    function getValoresTGEDArr() {
          return $this->arrValoresTGED;
    }

    /*
      Se establecen porpiedades extendidas de la entidad de software actual (S) 
    */
    function setProperties(&$conn,&$valueObject) {
          $conceptoIn   =$this->ContTged->getTGEDValue($conn,1,"idconcepto",$valueObject->getIdconcepto());
          $descMovIn    =$this->ContTged->getTGEDValue($conn,1,"tipomov",$valueObject->getTipomov()); 
          $descEstadoIn =$this->ContTged->getTGEDValue($conn,1,"estado",$valueObject->getEstado());
          $valueObject->_setProperties($conceptoIn, $descMovIn, null, $descEstadoIn);
    }

    /* 
      Proceso para setear las Características adicionales de la Entidad
      Ing. FMBM 2018/12/21 HO 15:58
    */
    function setPropertyDesc($property,&$valueObject,$valorEval) {
        if( sizeof($this->arrValoresTGED) > 0 ) {
            for ($row = 0; $row < sizeof($this->arrValoresTGED); $row++) {
                if($this->arrValoresTGED[$row]['campo']==$property && $this->arrValoresTGED[$row]['codigo']==$valorEval) {
                    $valueObject->_setProperty($property, $this->arrValoresTGED[$row]['descripcion']);
                    break;
                }
            } 
        } 
    }

    /*
      Ok. Ing. FMBM 27.OCT.2018
    */
    public function consultasTabla(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $query->execute();
          // $result = $query->fetchAll(PDO::FETCH_ARRAY);          // Genera un Array con índices 
          $result = $query->fetchAll(PDO::FETCH_ASSOC);             // Genera un Array asociativo
          return $result;
    }

    // Métodos para el manejo de relaciones de la Entidad

    // Relacion muchos a uno CIT
    function loadObjectCit(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectCit($conn);
        return $searchResults;
    }

    // Relacion muchos a uno ppto
    function loadObjectPpto(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectPpto($conn);
        return $searchResults;
    }

}

?>