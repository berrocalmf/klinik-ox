<?php

class OxmarDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    // function OxmarDao() {
    //     $this->resetCache();
    // }

    public function resetCache() {
        $this->cacheOk   = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxmar();
    }

    public function getObject(&$conn, $marid) {
          $valueObject = $this->createValueObject();
          $valueObject->setMarid($marid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    public function load(&$conn, &$valueObject) {

          if (!$valueObject->getMarid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM oxmar WHERE (marid = ".$valueObject->getMarid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    public function loadAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM oxmar";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk   = true;

          return $searchResults;
    }

    public function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO oxmar ( codmarca, descripcion, textomarca, ";
          $sql = $sql."tipografica, colorrealizada, coloreporrealizar, ";
          $sql = $sql."colorlineades, mcaras, mtexto, mgrafica, sumaindiceplaca, ";
          $sql = $sql."sumaindicopceo, indcaries, estado) VALUES ('".$valueObject->getCodmarca()."', ";
          $sql = $sql."'".$valueObject->getDescripcion()."', ";
          $sql = $sql."'".$valueObject->getTextomarca()."', ";
          $sql = $sql."'".$valueObject->getTipografica()."', ";
          $sql = $sql."".$valueObject->getColorrealizada().", ";
          $sql = $sql."".$valueObject->getColoreporrealizar().", ";
          $sql = $sql."".$valueObject->getColorlineades().", ";
          $sql = $sql."".$valueObject->getMcaras().", ";
          $sql = $sql."".$valueObject->getMtexto().", ";
          $sql = $sql."".$valueObject->getMgrafica().", ";
          $sql = $sql."".$valueObject->getSumaindiceplaca().", ";
          $sql = $sql."".$valueObject->getSumaindicopceo().", ";
          $sql = $sql."".$valueObject->getIndcaries().", ";
          $sql = $sql."'".$valueObject->getEstado()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false; 

          foreach($result as $row) {
              $valueObject->setMarid($row[0]); 
              $existe = true;
          }

          return $existe;
    }

    public function save(&$conn, &$valueObject) {

          $sql = "UPDATE oxmar SET codmarca = '".$valueObject->getCodmarca()."', ";
          $sql = $sql."descripcion = '".$valueObject->getDescripcion()."', ";
          $sql = $sql."textomarca = '".$valueObject->getTextomarca()."', ";
          $sql = $sql."tipografica = '".$valueObject->getTipografica()."', ";
          $sql = $sql."colorrealizada = ".$valueObject->getColorrealizada().", ";
          $sql = $sql."coloreporrealizar = ".$valueObject->getColoreporrealizar().", ";
          $sql = $sql."colorlineades = ".$valueObject->getColorlineades().", ";
          $sql = $sql."mcaras = ".$valueObject->getMcaras().", ";
          $sql = $sql."mtexto = ".$valueObject->getMtexto().", ";
          $sql = $sql."mgrafica = ".$valueObject->getMgrafica().", ";
          $sql = $sql."sumaindiceplaca = ".$valueObject->getSumaindiceplaca().", ";
          $sql = $sql."sumaindicopceo = ".$valueObject->getSumaindicopceo().", ";
          $sql = $sql."indcaries = ".$valueObject->getIndcaries().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."'";
          $sql = $sql." WHERE (marid = ".$valueObject->getMarid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    public function delete(&$conn, &$valueObject) {

          if (!$valueObject->getMarid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM oxmar WHERE (marid = ".$valueObject->getMarid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    public function deleteAll(&$conn) {

          $sql = "DELETE FROM oxmar";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    public function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM oxmar";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();  
          // $result = $conn->execute($sql);

          foreach($result as $row) {
              $allRows = $row[0];
          }  

          // if ($row = $conn->nextRow($result))
          //       $allRows = $row[0];

          return $allRows;
    }

    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxmar WHERE 1=1 ";

          if ($valueObject->getMarid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND marid = ".$valueObject->getMarid()." ";
          }

          if ($valueObject->getCodmarca() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND codmarca LIKE '".$valueObject->getCodmarca()."%' ";
          }

          if ($valueObject->getDescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND descripcion LIKE '".$valueObject->getDescripcion()."%' ";
          }

          if ($valueObject->getTextomarca() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND textomarca LIKE '".$valueObject->getTextomarca()."%' ";
          }

          if ($valueObject->getTipografica() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipografica LIKE '".$valueObject->getTipografica()."%' ";
          }

          if ($valueObject->getColorrealizada() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND colorrealizada = ".$valueObject->getColorrealizada()." ";
          }

          if ($valueObject->getColoreporrealizar() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND coloreporrealizar = ".$valueObject->getColoreporrealizar()." ";
          }

          if ($valueObject->getColorlineades() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND colorlineades = ".$valueObject->getColorlineades()." ";
          }

          if ($valueObject->getMcaras() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mcaras = ".$valueObject->getMcaras()." ";
          }

          if ($valueObject->getMtexto() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mtexto = ".$valueObject->getMtexto()." ";
          }

          if ($valueObject->getMgrafica() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mgrafica = ".$valueObject->getMgrafica()." ";
          }

          if ($valueObject->getSumaindiceplaca() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND sumaindiceplaca = ".$valueObject->getSumaindiceplaca()." ";
          }

          if ($valueObject->getSumaindicopceo() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND sumaindicopceo = ".$valueObject->getSumaindicopceo()." ";
          }

          if ($valueObject->getIndcaries() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND indcaries = ".$valueObject->getIndcaries()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }


          $sql = $sql."ORDER BY marid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    public function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    public function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;  

          foreach($result as $row){ 
          //if ($row = $conn->nextRow($result)) {
                   $valueObject->setMarid($row[0]); 
                   $valueObject->setCodmarca($row[1]); 
                   $valueObject->setDescripcion($row[2]); 
                   $valueObject->setTextomarca($row[3]); 
                   $valueObject->setTipografica($row[4]); 
                   $valueObject->setColorrealizada($row[5]); 
                   $valueObject->setColoreporrealizar($row[6]); 
                   $valueObject->setColorlineades($row[7]); 
                   $valueObject->setMcaras($row[8]); 
                   $valueObject->setMtexto($row[9]); 
                   $valueObject->setMgrafica($row[10]);                    
                   $valueObject->setSumaindiceplaca($row[11]); 
                   $valueObject->setSumaindicopceo($row[12]); 
                   $valueObject->setIndcaries($row[13]); 
                   $valueObject->setEstado($row[14]); 
                   $valida=true;
                    // } else {
                    //      //print " Object Not Found!";
                    //      return false;
          }
          return $valida;
    }

    public function listQuery(&$conn, &$sql) {

          $searchResults = array();

          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();
          // $result = $conn->execute($sql);

          foreach ($result as $row) {
          // while ($row = $conn->nextRow($result)) {
               $temp = $this->createValueObject();

               $temp->setMarid($row[0]); 
               $temp->setCodmarca($row[1]); 
               $temp->setDescripcion($row[2]); 
               $temp->setTextomarca($row[3]); 
               $temp->setTipografica($row[4]); 
               $temp->setColorrealizada($row[5]); 
               $temp->setColoreporrealizar($row[6]); 
               $temp->setColorlineades($row[7]); 
               $temp->setMcaras($row[8]); 
               $temp->setMtexto($row[9]); 
               $temp->setMgrafica($row[10]);
               $temp->setSumaindiceplaca($row[11]); 
               $temp->setSumaindicopceo($row[12]); 
               $temp->setIndcaries($row[13]); 
               $temp->setEstado($row[14]); 
               $searchResults[] = $temp;
          }

          return $searchResults;
    }

    // Métodos adicionales de usuario
    public function getCodMarca(&$conn, $marid) {
        
        $codmarca="0";

        $sql = "select codmarca from oxmar where marid=" . $marid;
        $qry = $conn->prepare($sql);
        $qry->execute();
        $result = $qry->fetchAll();

        foreach($result as $row) {
              $codmarca = $row[0];
        }

        return $codmarca; 
    }

    // (13.SEP.2018 - FMBM) Retorna el Id de un código de marca enviado 
    public function getIdMarca(&$conn, $marCod) {
          $idMarca=0;

          $sql = "select marid from oxmar where codmarca='" . $marCod . "'";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $idMarca = $row[0];
          }

          return $idMarca;
    }

    // Registra las marcas estipuladas por el Odontólogo en la tabla oxodond
    // Ing. FMBM - Revisado: 2018-01-08 11:49

    public function registraMarcas(&$conn,$odonId,$arrEstados,$arrCaras,$descripcion,$codDiente) {     
          $sql = "CALL spk_crearMarcasOX(?,?,?,?,?,?)";
          $query=$conn->prepare($sql);
          $r = explode('_',$arrEstados);
          $noMarcas = (sizeof($r) - 1);

          $parametros=array (
            $odonId,
            $arrEstados,
            $arrCaras,
            $descripcion,
            $codDiente,
            $noMarcas
          );

          $query->execute($parametros);
          return true;
    }

}

?>