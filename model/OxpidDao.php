<?php

 /**
  * Oxpid Data Access Object (DAO).
  * Clase de acceso a datos del File oxpid.
  * 13.SEP.2018 - Ing. FMBM 
 */

class OxpidDao {

    // Variables para el manejo y contennido de Cache:
    private $cacheOk;
    private $cacheData;

    public function __construct() {
         $this->resetCache();
    }

    // function OxpidDao() {$this->resetCache();}

    public function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxpid();
    }

    public function getObject(&$conn, $pidid) {
          $valueObject = $this->createValueObject();
          $valueObject->setPidid($pidid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    public function load(&$conn, &$valueObject) {

          if (!$valueObject->getPidid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM oxpid WHERE (pidid = ".$valueObject->getPidid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    public function loadAll(&$conn) {
          
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM oxpid ORDER BY pidid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    public function crear(&$conn, &$valueObject) {

          $sql = "INSERT INTO oxpid ( codpieza, descripcion, tipoPid, ";
          $sql = $sql."pidcaracentro, pidcarasuperior, pidcarainferior, ";
          $sql = $sql."pidcaraizquierda, pidcaraderecha) VALUES ('".$valueObject->getCodpieza()."', ";
          $sql = $sql."'".$valueObject->getDescripcion()."', ";
          $sql = $sql."'".$valueObject->getTipoPid()."', ";
          $sql = $sql."'".$valueObject->getPidcaracentro()."', ";
          $sql = $sql."'".$valueObject->getPidcarasuperior()."', ";
          $sql = $sql."'".$valueObject->getPidcarainferior()."', ";
          $sql = $sql."'".$valueObject->getPidcaraizquierda()."', ";
          $sql = $sql."'".$valueObject->getPidcaraderecha()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";

          $qry    = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $existe = false;

          foreach($result as $row) {
              $valueObject->setPidid($row[0]); 
              $existe = true;
          }  

          // $result = $conn->execute($sql);
          // if ($row = $conn->nextRow($result)) {
          //          $valueObject->setPidid($row[0]); 
          // } else {
          //      return false;
          // }

          return $existe;
    }

    public function actualizar(&$conn, &$valueObject) {

          $sql = "UPDATE oxpid SET codpieza = '".$valueObject->getCodpieza()."', ";
          $sql = $sql."descripcion = '".$valueObject->getDescripcion()."', ";
          $sql = $sql."tipoPid = '".$valueObject->getTipoPid()."', ";
          $sql = $sql."pidcaracentro = '".$valueObject->getPidcaracentro()."', ";
          $sql = $sql."pidcarasuperior = '".$valueObject->getPidcarasuperior()."', ";
          $sql = $sql."pidcarainferior = '".$valueObject->getPidcarainferior()."', ";
          $sql = $sql."pidcaraizquierda = '".$valueObject->getPidcaraizquierda()."', ";
          $sql = $sql."pidcaraderecha = '".$valueObject->getPidcaraderecha()."'";
          $sql = $sql." WHERE (pidid = ".$valueObject->getPidid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;
          }

          return true;
    }

    public function eliminar(&$conn, &$valueObject) {

          if (!$valueObject->getPidid()) {
               return false;
          }

          $sql = "DELETE FROM oxpid WHERE (pidid = ".$valueObject->getPidid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;
          }
          return true;
    }

    public function eliminarTodo(&$conn) {

          $sql = "DELETE FROM oxpid";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    public function countAll(&$conn) {

          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM oxpid";
          $allRows = 0;

          $result = $conn->execute($sql);

          if ($row = $conn->nextRow($result))
                $allRows = $row[0];

          return $allRows;
    }

    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxpid WHERE 1=1 ";

          if ($valueObject->getPidid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pidid = ".$valueObject->getPidid()." ";
          }

          if ($valueObject->getCodpieza() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND codpieza LIKE '".$valueObject->getCodpieza()."%' ";
          }

          if ($valueObject->getDescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND descripcion LIKE '".$valueObject->getDescripcion()."%' ";
          }

          if ($valueObject->getTipoPid() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipoPid LIKE '".$valueObject->getTipoPid()."%' ";
          }

          if ($valueObject->getPidcaracentro() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pidcaracentro LIKE '".$valueObject->getPidcaracentro()."%' ";
          }

          if ($valueObject->getPidcarasuperior() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pidcarasuperior LIKE '".$valueObject->getPidcarasuperior()."%' ";
          }

          if ($valueObject->getPidcarainferior() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pidcarainferior LIKE '".$valueObject->getPidcarainferior()."%' ";
          }

          if ($valueObject->getPidcaraizquierda() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pidcaraizquierda LIKE '".$valueObject->getPidcaraizquierda()."%' ";
          }

          if ($valueObject->getPidcaraderecha() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND pidcaraderecha LIKE '".$valueObject->getPidcaraderecha()."%' ";
          }


          $sql = $sql."ORDER BY pidid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    public function databaseUpdate(&$conn, &$sql) {
          $qry = $conn->prepare($sql);
          $result = $qry->execute();
          $this->resetCache();
          return $result;
    }

    public function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $valida = false;  

          foreach($result as $row){   
          // if ($row = $conn->nextRow($result)) {
               $valueObject->setPidid($row[0]); 
               $valueObject->setCodpieza($row[1]); 
               $valueObject->setDescripcion($row[2]); 
               $valueObject->setTipoPid($row[3]); 
               $valueObject->setPidcaracentro($row[4]); 
               $valueObject->setPidcarasuperior($row[5]); 
               $valueObject->setPidcarainferior($row[6]); 
               $valueObject->setPidcaraizquierda($row[7]); 
               $valueObject->setPidcaraderecha($row[8]); 
               // } else {
               //      //print " Object Not Found!";
               //      return false;
               $valida=true;
          }
          return $valida;
    }

    public function listQuery(&$conn, &$sql) {

          $searchResults = array();

          $qry = $conn->prepare();
          $qry->execute();
          $result = $qry->fetchAll();

          foreach ($result as $row) { 
          // while ($row = $conn->nextRow($result)) {
               $temp = $this->createValueObject();

               $temp->setPidid($row[0]); 
               $temp->setCodpieza($row[1]); 
               $temp->setDescripcion($row[2]); 
               $temp->setTipoPid($row[3]); 
               $temp->setPidcaracentro($row[4]); 
               $temp->setPidcarasuperior($row[5]); 
               $temp->setPidcarainferior($row[6]); 
               $temp->setPidcaraizquierda($row[7]); 
               $temp->setPidcaraderecha($row[8]); 
               $searchResults[] = $temp;
          }

          return $searchResults;
    }
}

?>
