<?php
 /**
  * Us_cgs Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Us_cgs object instances. 
  * Ing. FMBM 02.NOV.2018
  */

class Us_cgsDao {

    // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Us_cgs();
    }

    function getObject(&$conn, $us_cgsid) {

          $valueObject = $this->createValueObject();
          $valueObject->setUs_cgsid($us_cgsid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getUs_cgsid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM us_cgs WHERE (us_cgsid = ".$valueObject->getUs_cgsid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM us_cgs ORDER BY us_cgsid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO us_cgs ( saas_ciaid, saas_ciasid, prefijo, ";
          $sql = $sql."consecutivo) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."".$valueObject->getSaas_ciasid().", ";
          $sql = $sql."'".$valueObject->getPrefijo()."', ";
          $sql = $sql."".$valueObject->getConsecutivo().") ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
        
          $ex = false;

          foreach($result as $row) {
              $valueObject->setOdonid($row[0]); 
              $ex = true;
          }

          return $ex;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE us_cgs SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."saas_ciasid = ".$valueObject->getSaas_ciasid().", ";
          $sql = $sql."prefijo = '".$valueObject->getPrefijo()."', ";
          $sql = $sql."consecutivo = ".$valueObject->getConsecutivo()."";
          $sql = $sql." WHERE (us_cgsid = ".$valueObject->getUs_cgsid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getUs_cgsid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM us_cgs WHERE (us_cgsid = ".$valueObject->getUs_cgsid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {

          $sql = "DELETE FROM us_cgs";
          $result = $this->databaseUpdate($conn, $sql);

          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM us_cgs";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }      

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM us_cgs WHERE 1=1 ";

          if ($valueObject->getUs_cgsid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND us_cgsid = ".$valueObject->getUs_cgsid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getSaas_ciasid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciasid = ".$valueObject->getSaas_ciasid()." ";
          }

          if ($valueObject->getPrefijo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND prefijo LIKE '".$valueObject->getPrefijo()."%' ";
          }

          if ($valueObject->getConsecutivo() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND consecutivo = ".$valueObject->getConsecutivo()." ";
          }


          $sql = $sql."ORDER BY us_cgsid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {

          $query  = $conn->prepare($sql);
          $result = $query->execute();

          $this->resetCache();

          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){  

                   $valueObject->setUs_cgsid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setSaas_ciasid($row[2]); 
                   $valueObject->setPrefijo($row[3]); 
                   $valueObject->setConsecutivo($row[4]); 
                    $valida=true;
          }

          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setUs_cgsid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setSaas_ciasid($row[2]); 
               $temp->setPrefijo($row[3]); 
               $temp->setConsecutivo($row[4]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }

    /* Retorna el consecutivo de un prefijo */
    public function noConsecutivo(&$conn, $prefijo) {
      
          $consecutivo=0;
          $cobId=0;
          $saas_ciaid=0;
          $saas_ciasid=0;

          // $sql="SELECT count(*) FROM cop WHERE prefijo='" . $prefijo . "'";
          $sql="SELECT us_cgsid, saas_ciaid, saas_ciasid, consecutivo FROM us_cgs WHERE prefijo='" . $prefijo . "'";

          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll(); 

          foreach($result as $row) {
              $cobId       = $row[0];
              $saas_ciaid  = $row[1];
              $saas_ciasid = $row[2];
              $consecutivo = $row[3];
          }

          if($consecutivo==0) {        // Si no existe el registro, se procede a crear el consecutivo
              $valueObject = $this->createValueObject();
              $valueObject->setPrefijo($prefijo);
              $valueObject->setConsecutivo(1);
              $valueObject->setSaas_ciaid(1);
              $valueObject->setSaas_ciasid(1);

              if($this->create($conn, $valueObject) == true ){
                  $consecutivo = $valueObject->getConsecutivo();
              } else {
                  $consecutivo = 0;  
              }

          } else {
 
              $consecutivo = $consecutivo + 1;
            
              $valueObject = $this->createValueObject();
              $valueObject->setCopid($cobId);
              $valueObject->setPrefijo($prefijo);
              $valueObject->setConsecutivo($consecutivo);
              $valueObject->setSaas_ciaid($saas_ciaid);
              $valueObject->setSaas_ciasid($saas_ciasid);

              if($this->save($conn,$valueObject) == true ){
                  $consecutivo = $valueObject->getConsecutivo();
              } else {
                  $consecutivo = 0;  
              }

          }
                
          return $consecutivo;
    }
}

?>