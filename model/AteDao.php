<?php

 /**
  * Ate Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Ate object instances. 
  * Ing. FMBM 02.NOV.2018
  */

class AteDao {

    // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Ate();
    }

    function getObject(&$conn, $ateid) {
          $valueObject = $this->createValueObject();
          $valueObject->setAteid($ateid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {
          if (!$valueObject->getAteid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM ate WHERE (ateid = ".$valueObject->getAteid().") "; 
          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM ate";
          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {
        
        // Validar aquiellos valores que pueden iniciar en NULL
        
          $sql = "INSERT INTO ate ( saas_ciaid, tipoatencion, consecutivo, ";
          $sql = $sql."afiid, terid, tercntid, ";
          $sql = $sql."aconombre, acoparentesco, ";
          $sql = $sql."acodocid, acodireccion, acotelefono, ";
          $sql = $sql."acociuid, resnombre, restelefono, ";
          $sql = $sql."resparentesco, resciuid, citid, ";
          $sql = $sql."motivoate, fechafin, feproxcontrol, ";
          $sql = $sql."estado, docidafiliado) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."".$valueObject->getTipoatencion().", ";
          $sql = $sql."'".$valueObject->getConsecutivo()."', ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."".$valueObject->getTerid().", ";
          $sql = $sql."".$valueObject->getTercntid().", ";
          $sql = $sql."'".$valueObject->getAconombre()."', ";
          $sql = $sql."'".$valueObject->getAcoparentesco()."', ";
          $sql = $sql."'".$valueObject->getAcodocid()."', ";
          $sql = $sql."'".$valueObject->getAcodireccion()."', ";
          $sql = $sql."'".$valueObject->getAcotelefono()."', ";
          $sql = $sql."".$valueObject->getAcociuid().", ";
          $sql = $sql."'".$valueObject->getResnombre()."', ";
          $sql = $sql."'".$valueObject->getRestelefono()."', ";
          $sql = $sql."'".$valueObject->getResparentesco()."', ";
          $sql = $sql."".$valueObject->getResciuid().", ";
          $sql = $sql."".$valueObject->getCitid().", ";
          $sql = $sql."'".$valueObject->getMotivoate()."', ";
          $sql = $sql."'".$valueObject->getFechafin()."', ";
          $sql = $sql."'".$valueObject->getFeproxcontrol()."', ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."'".$valueObject->getDocidafiliado()."') ";
          $result = $this->databaseUpdate($conn, $sql);
            
          $sql = "SELECT last_insert_id()";       
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
        
          $ex = false;

          foreach($result as $row) {
              $valueObject->setAteid($row[0]); 
              $ex = true;
          }

          return $ex;
    }

    function save(&$conn, &$valueObject) {
        // Validar aquiellos valores que pueden iniciar en NULL

        $sql = "UPDATE ate SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
        $sql = $sql."tipoatencion = ".$valueObject->getTipoatencion().", ";
        $sql = $sql."consecutivo = '".$valueObject->getConsecutivo()."', ";
        $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
        $sql = $sql."terid = ".$valueObject->getTerid().", ";
        $sql = $sql."tercntid = ".$valueObject->getTercntid().", ";
        $sql = $sql."fecha = '".$valueObject->getFecha()."', ";
        $sql = $sql."aconombre = '".$valueObject->getAconombre()."', ";
        $sql = $sql."acoparentesco = '".$valueObject->getAcoparentesco()."', ";
        $sql = $sql."acodocid = '".$valueObject->getAcodocid()."', ";
        $sql = $sql."acodireccion = '".$valueObject->getAcodireccion()."', ";
        $sql = $sql."acotelefono = '".$valueObject->getAcotelefono()."', ";
        $sql = $sql."acociuid = ".$valueObject->getAcociuid().", ";
        $sql = $sql."resnombre = '".$valueObject->getResnombre()."', ";
        $sql = $sql."restelefono = '".$valueObject->getRestelefono()."', ";
        $sql = $sql."resparentesco = '".$valueObject->getResparentesco()."', ";
        $sql = $sql."resciuid = ".$valueObject->getResciuid().", ";
        $sql = $sql."citid = ".$valueObject->getCitid().", ";
        $sql = $sql."motivoate = '".$valueObject->getMotivoate()."', ";
        $sql = $sql."fechafin = '".$valueObject->getFechafin()."', ";
        $sql = $sql."feproxcontrol = '".$valueObject->getFeproxcontrol()."', ";
        $sql = $sql."estado = '".$valueObject->getEstado()."', ";
        $sql = $sql."docidafiliado = '".$valueObject->getDocidafiliado()."'";
        $sql = $sql." WHERE (ateid = ".$valueObject->getAteid().") ";
        $result = $this->databaseUpdate($conn, $sql);

        if ($result != 1) {
           //print "PrimaryKey Error when updating DB!";
           return false;
        }

        return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getAteid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM ate WHERE (ateid = ".$valueObject->getAteid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM ate";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM ate";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM ate WHERE 1=1 ";

          if ($valueObject->getAteid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ateid = ".$valueObject->getAteid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getTipoatencion() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tipoatencion = ".$valueObject->getTipoatencion()." ";
          }

          if ($valueObject->getConsecutivo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND consecutivo LIKE '".$valueObject->getConsecutivo()."%' ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getTerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND terid = ".$valueObject->getTerid()." ";
          }

          if ($valueObject->getTercntid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tercntid = ".$valueObject->getTercntid()." ";
          }

          if ($valueObject->getFecha() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecha = '".$valueObject->getFecha()."' ";
          }

          if ($valueObject->getAconombre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND aconombre LIKE '".$valueObject->getAconombre()."%' ";
          }

          if ($valueObject->getAcoparentesco() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND acoparentesco LIKE '".$valueObject->getAcoparentesco()."%' ";
          }

          if ($valueObject->getAcodocid() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND acodocid LIKE '".$valueObject->getAcodocid()."%' ";
          }

          if ($valueObject->getAcodireccion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND acodireccion LIKE '".$valueObject->getAcodireccion()."%' ";
          }

          if ($valueObject->getAcotelefono() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND acotelefono LIKE '".$valueObject->getAcotelefono()."%' ";
          }

          if ($valueObject->getAcociuid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND acociuid = ".$valueObject->getAcociuid()." ";
          }

          if ($valueObject->getResnombre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND resnombre LIKE '".$valueObject->getResnombre()."%' ";
          }

          if ($valueObject->getRestelefono() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND restelefono LIKE '".$valueObject->getRestelefono()."%' ";
          }

          if ($valueObject->getResparentesco() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND resparentesco LIKE '".$valueObject->getResparentesco()."%' ";
          }

          if ($valueObject->getResciuid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND resciuid = ".$valueObject->getResciuid()." ";
          }

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getMotivoate() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivoate LIKE '".$valueObject->getMotivoate()."%' ";
          }

          if ($valueObject->getFechafin() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechafin = '".$valueObject->getFechafin()."' ";
          }

          if ($valueObject->getFeproxcontrol() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND feproxcontrol = '".$valueObject->getFeproxcontrol()."' ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getDocidafiliado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND docidafiliado LIKE '".$valueObject->getDocidafiliado()."%' ";
          }


          $sql = $sql."ORDER BY ateid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }


    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){  
          // if ($row = $conn->nextRow($result)) {
                   $valueObject->setAteid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setTipoatencion($row[2]); 
                   $valueObject->setConsecutivo($row[3]); 
                   $valueObject->setAfiid($row[4]); 
                   $valueObject->setTerid($row[5]); 
                   $valueObject->setTercntid($row[6]); 
                   $valueObject->setFecha($row[7]); 
                   $valueObject->setAconombre($row[8]); 
                   $valueObject->setAcoparentesco($row[9]); 
                   $valueObject->setAcodocid($row[10]); 
                   $valueObject->setAcodireccion($row[11]); 
                   $valueObject->setAcotelefono($row[12]); 
                   $valueObject->setAcociuid($row[13]); 
                   $valueObject->setResnombre($row[14]); 
                   $valueObject->setRestelefono($row[15]); 
                   $valueObject->setResparentesco($row[16]); 
                   $valueObject->setResciuid($row[17]); 
                   $valueObject->setCitid($row[18]); 
                   $valueObject->setMotivoate($row[19]); 
                   $valueObject->setFechafin($row[20]); 
                   $valueObject->setFeproxcontrol($row[21]); 
                   $valueObject->setEstado($row[22]); 
                   $valueObject->setDocidafiliado($row[23]);
                   $valida=true;
          }
          return $valida;
    }
    
    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setAteid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setTipoatencion($row[2]); 
               $temp->setConsecutivo($row[3]); 
               $temp->setAfiid($row[4]); 
               $temp->setTerid($row[5]); 
               $temp->setTercntid($row[6]); 
               $temp->setFecha($row[7]); 
               $temp->setAconombre($row[8]); 
               $temp->setAcoparentesco($row[9]); 
               $temp->setAcodocid($row[10]); 
               $temp->setAcodireccion($row[11]); 
               $temp->setAcotelefono($row[12]); 
               $temp->setAcociuid($row[13]); 
               $temp->setResnombre($row[14]); 
               $temp->setRestelefono($row[15]); 
               $temp->setResparentesco($row[16]); 
               $temp->setResciuid($row[17]); 
               $temp->setCitid($row[18]); 
               $temp->setMotivoate($row[19]); 
               $temp->setFechafin($row[20]); 
               $temp->setFeproxcontrol($row[21]); 
               $temp->setEstado($row[22]); 
               $temp->setDocidafiliado($row[23]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
    
    // Metodos de Asociación

    //Relation avec la table med
    function loadObjectAfi(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAfi($conn);
        return $searchResults;
    }

    //Relation avec la table cit
    function loadObjectCit(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectCit($conn);
        return $searchResults;
    }

    // Relation un à plusieurs avec la table oxodon
    function loadOdontogramas(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getOdontogramas($conn);
        return $searchResults;
    }

    // Relation un à plusieurs avec la table ppto
    // Ing. FMBM 2019/01/21
    function loadPresupuesto(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getPresupuesto($conn);
        return $searchResults;
    }

}

?>