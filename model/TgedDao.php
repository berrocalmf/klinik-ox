<?php

 /**
  * Tged Data Access Object (DAO).
  * Ing. FBERROCALM 30/11/2018
  */

class TgedDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Tged();
    }

    function getObject(&$conn, $tgedid) {
          $valueObject = $this->createValueObject();
          $valueObject->setTgedid($tgedid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getTgedid()) {
               return false;       //print "Can not select without Primary-Key!";
          }

          $sql = "SELECT * FROM tged WHERE (tgedid = ".$valueObject->getTgedid().") "; 
          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {
     
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM tged ORDER BY tgedid ASC ";
          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;
          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO tged ( tgeid, campo, codigo, ";
          $sql = $sql."descripcion) VALUES (".$valueObject->getTgeid().", ";
          $sql = $sql."'".$valueObject->getCampo()."', ";
          $sql = $sql."'".$valueObject->getCodigo()."', ";
          $sql = $sql."'".$valueObject->getDescripcion()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $existe = false;

          foreach($result as $row) {
                   $valueObject->setTgedid($row[0]); 
                   $existe = true;
          }

          return $existe;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE tged SET tgeid = ".$valueObject->getTgeid().", ";
          $sql = $sql."campo = '".$valueObject->getCampo()."', ";
          $sql = $sql."codigo = '".$valueObject->getCodigo()."', ";
          $sql = $sql."descripcion = '".$valueObject->getDescripcion()."'";
          $sql = $sql." WHERE (tgedid = ".$valueObject->getTgedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {

          if (!$valueObject->getTgedid()) {
               return false;      //print "Can not delete without Primary-Key!";
          }

          $sql = "DELETE FROM tged WHERE (tgedid = ".$valueObject->getTgedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;      //print "PrimaryKey Error when updating DB!";
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM tged";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM tged";
          $allRows = 0;
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }      
          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM tged WHERE 1=1 ";

          if ($valueObject->getTgedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tgedid = ".$valueObject->getTgedid()." ";
          }

          if ($valueObject->getTgeid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tgeid = ".$valueObject->getTgeid()." ";
          }

          if ($valueObject->getCampo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND campo LIKE '".$valueObject->getCampo()."%' ";
          }

          if ($valueObject->getCodigo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND codigo LIKE '".$valueObject->getCodigo()."%' ";
          }

          if ($valueObject->getDescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND descripcion LIKE '".$valueObject->getDescripcion()."%' ";
          }

          $sql = $sql."ORDER BY tgedid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);
          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {
          
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $valida = false;

          foreach($result as $row){
                   $valueObject->setTgedid($row[0]); 
                   $valueObject->setTgeid($row[1]); 
                   $valueObject->setCampo($row[2]); 
                   $valueObject->setCodigo($row[3]); 
                   $valueObject->setDescripcion($row[4]);
                   $valida=true; 
          }

          return $valida;
    }

    function listQuery(&$conn, &$sql) {
          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();
               $temp->setTgedid($row[0]); 
               $temp->setTgeid($row[1]); 
               $temp->setCampo($row[2]); 
               $temp->setCodigo($row[3]); 
               $temp->setDescripcion($row[4]); 
               array_push($searchResults, $temp);
          }
          return $searchResults;
    }

    function getTGEDValue(&$conn,$tablaIn,$campoIn,$codigoIn) {
          $desccaract="";
          $sql="select descripcion from tged where tgeid=".$tablaIn." and campo='".$campoIn."' and condigo='".$codigoIn."'"; 

          $query=$conn->prepare($sql);
          $query->execute();
          $result=$query->fetchAll();

          foreach($result as $row) {
              $desccaract=$row[0];
          } 
          return $desccaract;
    }

    // Proceso que retorna todos los valores de configuracion en la tabla TGED para cualquier tabla
    // Ing. FBMB 21/dic/2018

    function getTGEDValues(&$conn,$tablaIn) {
          $sql="select tgeid,campo,codigo,descripcion from tged where tgeid=" . $tablaIn; 
          $query=$conn->prepare($sql);
          $query->execute();
          $result=$query->fetchAll(PDO::FETCH_ASSOC);
          return $result;
    }

}

?>
