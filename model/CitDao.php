<?php
 /**
  * Cit Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Cit object instances. 
  * Ing. FMBM 02.NOV.2018
  */

class CitDao {

    // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Cit();
    }

    function getObject(&$conn, $citid) {
          $valueObject = $this->createValueObject();
          $valueObject->setCitid($citid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {
          if (!$valueObject->getCitid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM cit WHERE (citid = ".$valueObject->getCitid().") "; 
          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM cit ORDER BY citid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO cit ( saas_ciaid, medid, fecha_sol, ";
          $sql = $sql."horallegada, fechacita, sitio, ";
          $sql = $sql."horaatencion, afiid, mtcid, ";
          $sql = $sql."tiposolicitud, tipoorden, serid, ";
          $sql = $sql."pvez, urgencia, valortotal, ";
          $sql = $sql."valorcopa, valorexedente, impreso, ";
          $sql = $sql."cumplida, facturado, enviadocob, ";
          $sql = $sql."telaviso, obs, estado, ";
          $sql = $sql."fechafin, tercid, afivid, ";
          $sql = $sql."asignada, fechaultimavis) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."".$valueObject->getMedid().", ";
          $sql = $sql."'".$valueObject->getFecha_sol()."', ";
          $sql = $sql."'".$valueObject->getHorallegada()."', ";
          $sql = $sql."'".$valueObject->getFechacita()."', ";
          $sql = $sql."".$valueObject->getSitio().", ";
          $sql = $sql."'".$valueObject->getHoraatencion()."', ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."".$valueObject->getMtcid().", ";
          $sql = $sql."'".$valueObject->getTiposolicitud()."', ";
          $sql = $sql."'".$valueObject->getTipoorden()."', ";
          $sql = $sql."".$valueObject->getSerid().", ";
          $sql = $sql."".$valueObject->getPvez().", ";
          $sql = $sql."".$valueObject->getUrgencia().", ";
          $sql = $sql."'".$valueObject->getValortotal()."', ";
          $sql = $sql."'".$valueObject->getValorcopa()."', ";
          $sql = $sql."'".$valueObject->getValorexedente()."', ";
          $sql = $sql."".$valueObject->getImpreso().", ";
          $sql = $sql."".$valueObject->getCumplida().", ";
          $sql = $sql."".$valueObject->getFacturado().", ";
          $sql = $sql."".$valueObject->getEnviadocob().", ";
          $sql = $sql."'".$valueObject->getTelaviso()."', ";
          $sql = $sql."'".$valueObject->getObs()."', ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."'".$valueObject->getFechafin()."', ";
          $sql = $sql."".$valueObject->getTercid().", ";
          $sql = $sql."".$valueObject->getAfivid().", ";
          $sql = $sql."".$valueObject->getAsignada().", ";
          $sql = $sql."'".$valueObject->getFechaultimavis()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
        
          $ex = false;

          foreach($result as $row) {
              $valueObject->setCitid($row[0]);
              $ex = true;

          }
          return $ex;
    }
    
    function save(&$conn, &$valueObject) {

          $sql = "UPDATE cit SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."medid = ".$valueObject->getMedid().", ";
          $sql = $sql."fecha_sol = '".$valueObject->getFecha_sol()."', ";
          $sql = $sql."horallegada = '".$valueObject->getHorallegada()."', ";
          $sql = $sql."fechacita = '".$valueObject->getFechacita()."', ";
          $sql = $sql."sitio = ".$valueObject->getSitio().", ";
          $sql = $sql."horaatencion = '".$valueObject->getHoraatencion()."', ";
          $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
          $sql = $sql."mtcid = ".$valueObject->getMtcid().", ";
          $sql = $sql."tiposolicitud = '".$valueObject->getTiposolicitud()."', ";
          $sql = $sql."tipoorden = '".$valueObject->getTipoorden()."', ";
          $sql = $sql."serid = ".$valueObject->getSerid().", ";
          $sql = $sql."pvez = ".$valueObject->getPvez().", ";
          $sql = $sql."urgencia = ".$valueObject->getUrgencia().", ";
          $sql = $sql."valortotal = '".$valueObject->getValortotal()."', ";
          $sql = $sql."valorcopa = '".$valueObject->getValorcopa()."', ";
          $sql = $sql."valorexedente = '".$valueObject->getValorexedente()."', ";
          $sql = $sql."impreso = ".$valueObject->getImpreso().", ";
          $sql = $sql."cumplida = ".$valueObject->getCumplida().", ";
          $sql = $sql."facturado = ".$valueObject->getFacturado().", ";
          $sql = $sql."enviadocob = ".$valueObject->getEnviadocob().", ";
          $sql = $sql."telaviso = '".$valueObject->getTelaviso()."', ";
          $sql = $sql."obs = '".$valueObject->getObs()."', ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."fechafin = '".$valueObject->getFechafin()."', ";
          $sql = $sql."tercid = ".$valueObject->getTercid().", ";
          $sql = $sql."afivid = ".$valueObject->getAfivid().", ";
          $sql = $sql."asignada = ".$valueObject->getAsignada().", ";
          $sql = $sql."fechaultimavis = '".$valueObject->getFechaultimavis()."'";
          $sql = $sql." WHERE (citid = ".$valueObject->getCitid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {
          if (!$valueObject->getCitid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM cit WHERE (citid = ".$valueObject->getCitid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM cit";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM cit";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM cit WHERE 1=1 ";

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getMedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medid = ".$valueObject->getMedid()." ";
          }

          if ($valueObject->getFecha_sol() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecha_sol = '".$valueObject->getFecha_sol()."' ";
          }

          if ($valueObject->getHorallegada() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND horallegada = '".$valueObject->getHorallegada()."' ";
          }

          if ($valueObject->getFechacita() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechacita = '".$valueObject->getFechacita()."' ";
          }

          if ($valueObject->getSitio() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND sitio = ".$valueObject->getSitio()." ";
          }

          if ($valueObject->getHoraatencion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND horaatencion = '".$valueObject->getHoraatencion()."' ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getMtcid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mtcid = ".$valueObject->getMtcid()." ";
          }

          if ($valueObject->getTiposolicitud() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tiposolicitud LIKE '".$valueObject->getTiposolicitud()."%' ";
          }

          if ($valueObject->getTipoorden() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipoorden LIKE '".$valueObject->getTipoorden()."%' ";
          }

          if ($valueObject->getSerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND serid = ".$valueObject->getSerid()." ";
          }

          if ($valueObject->getPvez() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pvez = ".$valueObject->getPvez()." ";
          }

          if ($valueObject->getUrgencia() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND urgencia = ".$valueObject->getUrgencia()." ";
          }

          if ($valueObject->getValortotal() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND valortotal = '".$valueObject->getValortotal()."' ";
          }

          if ($valueObject->getValorcopa() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND valorcopa = '".$valueObject->getValorcopa()."' ";
          }

          if ($valueObject->getValorexedente() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND valorexedente = '".$valueObject->getValorexedente()."' ";
          }

          if ($valueObject->getImpreso() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND impreso = ".$valueObject->getImpreso()." ";
          }

          if ($valueObject->getCumplida() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cumplida = ".$valueObject->getCumplida()." ";
          }

          if ($valueObject->getFacturado() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND facturado = ".$valueObject->getFacturado()." ";
          }

          if ($valueObject->getEnviadocob() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND enviadocob = ".$valueObject->getEnviadocob()." ";
          }

          if ($valueObject->getTelaviso() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND telaviso LIKE '".$valueObject->getTelaviso()."%' ";
          }

          if ($valueObject->getObs() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND obs LIKE '".$valueObject->getObs()."%' ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getFechafin() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechafin = '".$valueObject->getFechafin()."' ";
          }

          if ($valueObject->getTercid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND tercid = ".$valueObject->getTercid()." ";
          }

          if ($valueObject->getAfivid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afivid = ".$valueObject->getAfivid()." ";
          }

          if ($valueObject->getAsignada() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND asignada = ".$valueObject->getAsignada()." ";
          }

          if ($valueObject->getFechaultimavis() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechaultimavis = '".$valueObject->getFechaultimavis()."' ";
          }


          $sql = $sql."ORDER BY citid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }


    function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){      
          // if ($row = $conn->nextRow($result)) {

                   $valueObject->setCitid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setMedid($row[2]); 
                   $valueObject->setFecha_sol($row[3]); 
                   $valueObject->setHorallegada($row[4]); 
                   $valueObject->setFechacita($row[5]); 
                   $valueObject->setSitio($row[6]); 
                   $valueObject->setHoraatencion($row[7]); 
                   $valueObject->setAfiid($row[8]); 
                   $valueObject->setMtcid($row[9]); 
                   $valueObject->setTiposolicitud($row[10]); 
                   $valueObject->setTipoorden($row[11]); 
                   $valueObject->setSerid($row[12]); 
                   $valueObject->setPvez($row[13]); 
                   $valueObject->setUrgencia($row[14]); 
                   $valueObject->setValortotal($row[15]); 
                   $valueObject->setValorcopa($row[16]); 
                   $valueObject->setValorexedente($row[17]); 
                   $valueObject->setImpreso($row[18]); 
                   $valueObject->setCumplida($row[19]); 
                   $valueObject->setFacturado($row[20]); 
                   $valueObject->setEnviadocob($row[21]); 
                   $valueObject->setTelaviso($row[22]); 
                   $valueObject->setObs($row[23]); 
                   $valueObject->setEstado($row[24]); 
                   $valueObject->setFechafin($row[25]); 
                   $valueObject->setTercid($row[26]); 
                   $valueObject->setAfivid($row[27]); 
                   $valueObject->setAsignada($row[28]); 
                   $valueObject->setFechaultimavis($row[29]); 
                   $valida=true;
        //          } else {
        //               //print " Object Not Found!";
        //               return false;
          }
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();


          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setCitid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setMedid($row[2]); 
               $temp->setFecha_sol($row[3]); 
               $temp->setHorallegada($row[4]); 
               $temp->setFechacita($row[5]); 
               $temp->setSitio($row[6]); 
               $temp->setHoraatencion($row[7]); 
               $temp->setAfiid($row[8]); 
               $temp->setMtcid($row[9]); 
               $temp->setTiposolicitud($row[10]); 
               $temp->setTipoorden($row[11]); 
               $temp->setSerid($row[12]); 
               $temp->setPvez($row[13]); 
               $temp->setUrgencia($row[14]); 
               $temp->setValortotal($row[15]); 
               $temp->setValorcopa($row[16]); 
               $temp->setValorexedente($row[17]); 
               $temp->setImpreso($row[18]); 
               $temp->setCumplida($row[19]); 
               $temp->setFacturado($row[20]); 
               $temp->setEnviadocob($row[21]); 
               $temp->setTelaviso($row[22]); 
               $temp->setObs($row[23]); 
               $temp->setEstado($row[24]); 
               $temp->setFechafin($row[25]); 
               $temp->setTercid($row[26]); 
               $temp->setAfivid($row[27]); 
               $temp->setAsignada($row[28]); 
               $temp->setFechaultimavis($row[29]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
}

?>