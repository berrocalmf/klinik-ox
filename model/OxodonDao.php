  <?php

class OxodonDao {
    
    // Cache contents:
    var $cacheOk;
    var $cacheData;
    
    public function __construct() {
        $this->resetCache();
    }
    
    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxodon();
    }

    public function getObject(&$conn, $odonid) {
          $valueObject = $this->createValueObject();
          $valueObject->setOdonid($odonid);
          $this->load($conn,$valueObject);
          return $valueObject;
    }

    public function load(&$conn, &$valueObject) {

          if (!$valueObject->getOdonid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "select * from oxodon where (odonid=".$valueObject->getOdonid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }


    public function loadAll(&$conn) {
        // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }
        
          $sql = "select * from oxodon";
          $searchResults = $this->listQuery($conn, $sql);
        
        // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;
        
          return $searchResults;
    }

    public function create(&$conn, &$valueObject) {

          $sql = "insert into oxodon ( noodontogram, nroficha, ";
          $sql = $sql."medid, ateid, afiid, ";
          $sql = $sql."estado, ";
          $sql = $sql."denttemporal, indiceCPOD, indiceCEOD, ";
          $sql = $sql."customerid, citid, odxinicial, motivoAnula) VALUES ('".$valueObject->getNoodontogram()."', ";
          $sql = $sql."".$valueObject->getNroficha().", ";
          $sql = $sql."".$valueObject->getMedid().", ";
          $sql = $sql."".$valueObject->getAteid().", ";
          $sql = $sql."".$valueObject->getAfiid().", ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."".$valueObject->getDenttemporal().", ";
          $sql = $sql."'".$valueObject->getIndiceCPOD()."', ";
          $sql = $sql."'".$valueObject->getIndiceCEOD()."', ";
          $sql = $sql."'".$valueObject->getCustomerid()."', ";
          $sql = $sql."'".$valueObject->getCitid()."', ";
          $sql = $sql."".$valueObject->getOdxinicial().", ";
          $sql = $sql."'".$valueObject->getMotivoanula()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql    = "select last_insert_id()";

          $qry    = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          
          // $result = $conn->execute($sql);
          $existe = false;

          foreach($result as $row) {
              $valueObject->setOdonid($row[0]); 
              $existe = true;
          }

          return $existe;
    }

    public function save(&$conn, &$valueObject) {

          $sql = "update oxodon set noodontogram = '".$valueObject->getNoodontogram()."', ";
          $sql = $sql."nroficha = ".$valueObject->getNroficha().", ";
          $sql = $sql."fecha = '".$valueObject->getFecha()."', ";
          $sql = $sql."medid = ".$valueObject->getMedid().", ";
          $sql = $sql."ateid = ".$valueObject->getAteid().", ";
          $sql = $sql."afiid = ".$valueObject->getAfiid().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."denttemporal = ".$valueObject->getDenttemporal().", ";
          $sql = $sql."indiceCPOD = '".$valueObject->getIndiceCPOD()."', ";
          $sql = $sql."indiceCEOD = '".$valueObject->getIndiceCEOD()."', ";
          $sql = $sql."customerid = ".$valueObject->getCustomerid().", ";
          $sql = $sql."citid = ".$valueObject->getCitid().", ";
          $sql = $sql."odxinicial = ".$valueObject->getOdxinicial().", ";
          $sql = $sql."motivoAnula = '".$valueObject->getMotivoanula()."'";
          $sql = $sql." WHERE (odonid = ".$valueObject->getOdonid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    // Delete a record from Oxodon.  Ing. FBERROCALM
    
    public function delete(&$conn, &$valueObject) {

          if (!$valueObject->getOdonid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "delete from oxodon where (odonid = ".$valueObject->getOdonid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    // Delete all from table Oxodon.  Ing. FBERROCALM

    public function deleteAll(&$conn) {
          $sql = "delete from oxodon";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }



    public function countAll(&$conn) {
        // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM oxodon";
          $allRows = 0;
        
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
              $allRows = $row[0];
          }

          return $allRows;
    }

    // Query constructor analizing the object type Oxodon
    // Ing. FBERROCALM 

    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxodon WHERE 1=1 ";

          if ($valueObject->getOdonid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND odonid = ".$valueObject->getOdonid()." ";
          }

          if ($valueObject->getNoodontogram() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND noodontogram LIKE '".$valueObject->getNoodontogram()."%' ";
          }

          if ($valueObject->getNroficha() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND nroficha = ".$valueObject->getNroficha()." ";
          }

          if ($valueObject->getFecha() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecha = '".$valueObject->getFecha()."' ";
          }

          if ($valueObject->getMedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medid = ".$valueObject->getMedid()." ";
          }

          if ($valueObject->getAteid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ateid = ".$valueObject->getAteid()." ";
          }

          if ($valueObject->getAfiid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND afiid = ".$valueObject->getAfiid()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getDenttemporal() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND denttemporal = ".$valueObject->getDenttemporal()." ";
          }

          if ($valueObject->getIndiceCPOD() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND indiceCPOD LIKE '".$valueObject->getIndiceCPOD()."%' ";
          }

          if ($valueObject->getIndiceCEOD() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND indiceCEOD LIKE '".$valueObject->getIndiceCEOD()."%' ";
          }

          if ($valueObject->getCustomerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND customerid = ".$valueObject->getCustomerid()." ";
          }

          if ($valueObject->getCitid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND citid = ".$valueObject->getCitid()." ";
          }

          if ($valueObject->getOdxinicial() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND odxinicial = ".$valueObject->getOdxinicial()." ";
          }

          if ($valueObject->getMotivoanula() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivoAnula LIKE '".$valueObject->getMotivoanula()."%' ";
          }

          $sql = $sql."ORDER BY odonid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    // Applying changes on Database table Oxodon
    // Ing. FBERROCALM

    public function databaseUpdate(&$conn, &$sql) {
        $query  = $conn->prepare($sql);
        $result = $query->execute();
        $this->resetCache();
        return $result;
    }

    // Basic query on Database Table Oxodon.  A single Row
    // Ing. FBERROCALM

    public function singleQuery(&$conn, &$sql, &$valueObject) {
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();
          $valida = false;  

          foreach($result as $row){ 
               $valueObject->setOdonid($row[0]); 
               $valueObject->setNoodontogram($row[1]); 
               $valueObject->setNroficha($row[2]); 
               $valueObject->setFecha($row[3]); 
               $valueObject->setMedid($row[4]); 
               $valueObject->setAteid($row[5]); 
               $valueObject->setAfiid($row[6]); 
               $valueObject->setEstado($row[7]); 
               $valueObject->setDenttemporal($row[8]); 
               $valueObject->setIndiceCPOD($row[9]); 
               $valueObject->setIndiceCEOD($row[10]); 
               $valueObject->setCitid($row[11]); 
               $valueObject->setCustomerid($row[12]); 
               $valueObject->setOdxinicial($row[13]); 
               $valueObject->setMotivoanula($row[14]); 
               $valida=true;
          }
          return $valida;
    }

    // Basic query on Database Table Oxodon.  A List of Rows
    // Ing. FBERROCALM

    public function listQuery(&$conn, &$sql) {
        $searchResults = [];            // <-- array();
        $query         = $conn->prepare($sql);
          
        $query->execute();
          
        $result        = $query->fetchAll();

        foreach ($result as $row) {  
            $temp = $this->createValueObject();
            $temp->setOdonid($row[0]); 
            $temp->setNoodontogram($row[1]); 
            $temp->setNroficha($row[2]); 
            $temp->setFecha($row[3]); 
            $temp->setMedid($row[4]); 
            $temp->setAteid($row[5]); 
            $temp->setAfiid($row[6]); 
            $temp->setEstado($row[7]); 
            $temp->setDenttemporal($row[8]); 
            $temp->setIndiceCPOD($row[9]); 
            $temp->setIndiceCEOD($row[10]); 
            $temp->setCitid($row[11]); 
            $temp->setCustomerid($row[12]);
            $temp->setOdxinicial($row[13]);
            $temp->setMotivoanula($row[14]); 
            $searchResults[] = $temp; 
        }
        return $searchResults;
    }

    // consultarTodoPorCodigoPaciente: Retorna todos los odontogramas de un Paciente
    public function loadAllPorIdAfi(&$conn, $afiid) {             
          $query="select * from oxodon where afiid=".$afiid;
          $arrayOxodon = $this->listQuery($conn, $query);
          return $arrayOxodon;
    }

    // consultarUltimo: Trae el último Odontograma realizado a un Paciente
    public function loadUltimoOdonAfi(&$conn, $afiid) {
          $query="select * from oxodon where afiid=".$afiid." order by odonid desc limit 1";
          $arrayOxodon = $this->listQuery($conn, $query);
          return $arrayOxodon;
    }

    /* Contar los Odontogramas de un Paciente */
    public function noFichas(&$conn, $afiid) {
          $sql    = "SELECT count(*) FROM oxodon WHERE afiid=" . $afiid;
          $noRows = 0;
          $query  = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll(); 

          foreach($result as $row) {
              $noRows = $row[0];
          }
          return $noRows;
    }
    
    // Relación muchos a uno ATE

    function loadObjectAte(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAte($conn);
        return $searchResults;
    }
    
    // Relación Muchos a Uno AFI

    function loadObjectAfi(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAfi($conn);
        return $searchResults;
    }
    
    // Relacion muchos a uno CIT

    function loadObjectCit(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectCit($conn);
        return $searchResults;
    }

     // Relacion muchos a uno MED

    function loadObjectMed(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectMed($conn);
        return $searchResults;
    }

    // Relaciones de asociación de uno a muchos tabla Oxodon

    // Relación uno a Muchos OXODOND
    function loadDetallesOdond(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getDetallesOdond($conn);
        return $searchResults;
    }

     // Relación uno a muchos OXPROC
    function loadListaProc(&$conn,&$valueObject) {
        $searchResults = array();
        $searchResults = $valueObject->getListaProc($conn);
        return $searchResults;
    }

    // retorna registros detallados de sus marcas odontológicas
    function loadMarcasArray(&$conn,&$valueObject) {
        // $searchResults = array();
        $searchResults = $valueObject->getMarcasArray($conn);
        return $searchResults;
    }

    // Copier des états ou des marques entre odontogrammes frères
    // Ing. FMBM 2018/11/05
    public function copiaMarcas(&$conn, $odonidorg, $odoniddest) {
          $sql = "CALL spk_copiarMarcasOX(?,?)";
          $query=$conn->prepare($sql);
          $parametros=array (
            $odonidorg,
            $odoniddest
          );

          $query->execute($parametros);       
          return true;
     }

     /*Ok. Ing. FMBM 27.OCT.2018*/
     // Proceso que ejecuta cualquier consulta sobre la tabla y retorna un array
     // Asociativo con los datos

    public function consultasTabla(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll(PDO::FETCH_ASSOC);    // Genera un Array asociativo
          return $result;
    }

    // LISTADOS relaciones uno a muchos para GRILLAS (TABLAS):

    public function listadoProcediminetos(&$conn,$idodontograma) {
        $sql="";
        $sql="SELECT x.procid,x.codproced,x.ateid,x.odonid,x.serid,x.descripcion,x.pidid,d.codpieza,x.caraTXT,x.procarapieza,x.procsector,x.marid,m.codmarca,x.estado,x.fechaproc,x.fecharealizada,";
        $sql.="x.cantidad,x.procvalor,x.procvalortotal,x.vlrcopagopres,x.procautorizar,x.observaciones,x.medidprog,x.medidrealiz,z.nombre,x.cobid,x.conscob,x.finalidad,x.ambito,x.costumerid,x.omedid,x.pptoid,x.motivoAnula,x.personal_at";
        $sql.=" FROM oxproc x "; 
        $sql.=" LEFT JOIN oxpid d on x.pidid=d.pidid";
        $sql.=" LEFT JOIN oxmar m on x.marid=m.marid";
        $sql.=" LEFT JOIN med z on x.medidrealiz=z.medid ";
        $sql.="where x.odonid=" . $idodontograma . " and (x.estado='P' or x.estado='F')";
        
        $query  = $conn->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);    // Genera un Array asociativo
        return $result;
    }

   public function listDetallesOdx(&$conn,$valueObject) {          
        $sql="";  
        $sql.="SELECT oxodond.odondid,oxodond.odonid,oxodond.marid,oxmar.codmarca,";
        $sql.="oxodond.pidid,oxpid.codpieza,oxodond.caras,oxodond.carasmarca,";
        $sql.="oxodond.estado,oxodond.oxpraid,oxodond.descripcion,oxodond.fregistro, ";
        $sql.="oxmar.textomarca ";
        $sql.=" FROM oxodond ";
        $sql.="left join oxmar on oxodond.marid=oxmar.marid ";
        $sql.="left join oxpid on oxodond.pidid=oxpid.pidid ";
        $sql.="where odonid=".$valueObject->getOdonid();

        //$sql="select * from oxodond where odonid=".$odonid;
        $query  = $conn->prepare($sql);
        $query->execute();
        // $result = $query->fetchAll();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        // PDO::FETCH_ASSOC
        return $result;
     }

}

?>