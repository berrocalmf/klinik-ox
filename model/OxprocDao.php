<?php

 /**
  * Oxproc Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Oxproc object instances.
  * 
  * Créé par Ing. FMBM
  * Revu et validé par Ing. FMBM 2018.10.27
  */

class OxprocDao {

    // Cache contents:
    private $cacheOk;
    private $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    public function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxproc();
    }

    public function getObject(&$conn, $procid) {
          $valueObject = $this->createValueObject();
          $valueObject->setProcid($procid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    // Ok. Ing. FMBM 27.OCT.2018
    public function load(&$conn, &$valueObject) {
          if (!$valueObject->getProcid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM oxproc WHERE (procid = ".$valueObject->getProcid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    public function loadAll(&$conn) {

          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM oxproc";
          $searchResults = $this->listQuery($conn, $sql);
          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;
          return $searchResults;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO oxproc ( codproced, ateid, odonid, ";
          $sql = $sql."serid, descripcion, pidid, ";
          $sql = $sql."caraTXT, procarapieza, procsector, ";
          $sql = $sql."marid, estado, ";
          $sql = $sql."cantidad, procvalor, ";
          $sql = $sql."procvalortotal, vlrcopagopres, procautorizar, ";
          $sql = $sql."observaciones, medidprog, ";
          $sql = $sql."cobid, conscob, finalidad, ";
          $sql = $sql."ambito, costumerid, omedid,  pptoid, ";
          $sql = $sql."motivoAnula, personal_at) VALUES ('".$valueObject->getCodproced()."', ";
          $sql = $sql."".$valueObject->getAteid().", ";
          $sql = $sql."".$valueObject->getOdonid().", ";
          $sql = $sql."".$valueObject->getSerid().", ";
          $sql = $sql."'".$valueObject->getDescripcion()."', ";
          $sql = $sql."".$valueObject->getPidid().", ";
          $sql = $sql."'".$valueObject->getCaraTXT()."', ";
          $sql = $sql."'".$valueObject->getProcarapieza()."', ";
          $sql = $sql."'".$valueObject->getProcsector()."', ";
          $sql = $sql."".$valueObject->getMarid().", ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."".$valueObject->getCantidad().", ";
          $sql = $sql."'".$valueObject->getProcvalor()."', ";
          $sql = $sql."'".$valueObject->getProcvalortotal()."', ";
          $sql = $sql."'".$valueObject->getVlrcopagopres()."', ";
          $sql = $sql."".$valueObject->getProcautorizar().", ";
          $sql = $sql."'".$valueObject->getObservaciones()."', ";
          $sql = $sql."".$valueObject->getMedidprog().", ";
          $sql = $sql."".$valueObject->getCobid().", ";
          $sql = $sql."'".$valueObject->getConscob()."', ";
          $sql = $sql."'".$valueObject->getFinalidad()."', ";
          $sql = $sql."'".$valueObject->getAmbito()."', ";
          $sql = $sql."".$valueObject->getCostumerid().", ";
          $sql = $sql."".$valueObject->getOmedid().", "; 
          $sql = $sql."".$valueObject->getPptoid().", ";
          $sql = $sql."'".$valueObject->getMotivoAnula()."', ";
          $sql = $sql."'".$valueObject->getPersonal_at()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $existe = false;

          foreach($result as $row) {
              $valueObject->setProcid($row[0]); 
              $existe = true;
          }  
          return $existe;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function save(&$conn, &$valueObject) {

          $sql = "UPDATE oxproc SET codproced = '".$valueObject->getCodproced()."', ";
          $sql = $sql."ateid = ".$valueObject->getAteid().", ";
          $sql = $sql."odonid = ".$valueObject->getOdonid().", ";
          $sql = $sql."serid = ".$valueObject->getSerid().", ";
          $sql = $sql."descripcion = '".$valueObject->getDescripcion()."', ";
          $sql = $sql."pidid = ".$valueObject->getPidid().", ";
          $sql = $sql."caraTXT = '".$valueObject->getCaraTXT()."', ";
          $sql = $sql."procarapieza = '".$valueObject->getProcarapieza()."', ";
          $sql = $sql."procsector = '".$valueObject->getProcsector()."', ";
          $sql = $sql."marid = ".$valueObject->getMarid().", ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."fechaproc = '".$valueObject->getFechaproc()."', ";
          //if($valueObject->getFecharealizada()!=""){ 
          $sql = $sql."fecharealizada = '".$valueObject->getFecharealizada()."', ";
          //}
          $sql = $sql."cantidad = ".$valueObject->getCantidad().", ";
          $sql = $sql."procvalor = '".$valueObject->getProcvalor()."', ";
          $sql = $sql."procvalortotal = '".$valueObject->getProcvalortotal()."', ";
          $sql = $sql."vlrcopagopres = '".$valueObject->getVlrcopagopres()."', ";
          $sql = $sql."procautorizar = ".$valueObject->getProcautorizar().", ";
          $sql = $sql."observaciones = '".$valueObject->getObservaciones()."', ";
          $sql = $sql."medidprog = ".$valueObject->getMedidprog().", ";
          //if($valueObject->getMedidrealiz()){
          $sql = $sql."medidrealiz = ".$valueObject->getMedidrealiz().", ";
          //}
          $sql = $sql."cobid = ".$valueObject->getCobid().", ";
          $sql = $sql."conscob = '".$valueObject->getConscob()."', ";
          $sql = $sql."finalidad = '".$valueObject->getFinalidad()."', ";
          $sql = $sql."ambito = '".$valueObject->getAmbito()."', ";
          //if($valueObject->getCostumerid()!=""){ 
          $sql = $sql."costumerid = ".$valueObject->getCostumerid()."";
          //}
          $sql = $sql."omedid = ".$valueObject->getOmedid().", ";
          $sql = $sql."pptoid = ".$valueObject->getPptoid().", ";
          $sql = $sql."motivoAnula = '".$valueObject->getMotivoAnula()."', ";
          $sql = $sql."personal_at = '".$valueObject->getPersonal_at()."', ";
          $sql = $sql." WHERE (procid = ".$valueObject->getProcid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function delete(&$conn, &$valueObject) {
          if (!$valueObject->getProcid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM oxproc WHERE (procid = ".$valueObject->getProcid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function deleteAll(&$conn) {
          $sql = "DELETE FROM oxproc";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }


    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM oxproc";
          $allRows = 0;
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();  
          // $result = $conn->execute($sql);

          foreach($result as $row) {
              $allRows = $row[0];
          } 
          return $allRows;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function countxOdx(&$conn,$idodontograma) {

          $sql = "SELECT count(*) FROM oxproc WHERE odonid=".$idodontograma;
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();  
          // $result = $conn->execute($sql);

          foreach($result as $row) {
              $allRows = $row[0];
          } 

          return $allRows;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxproc WHERE 1=1 ";

          if ($valueObject->getProcid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND procid = ".$valueObject->getProcid()." ";
          }

          if ($valueObject->getCodproced() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND codproced LIKE '".$valueObject->getCodproced()."%' ";
          }

          if ($valueObject->getAteid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ateid = ".$valueObject->getAteid()." ";
          }

          if ($valueObject->getOdonid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND odonid = ".$valueObject->getOdonid()." ";
          }

          if ($valueObject->getSerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND serid = ".$valueObject->getSerid()." ";
          }

          if ($valueObject->getDescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND descripcion LIKE '".$valueObject->getDescripcion()."%' ";
          }

          if ($valueObject->getPidid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pidid = ".$valueObject->getPidid()." ";
          }

          if ($valueObject->getCaraTXT() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND caraTXT LIKE '".$valueObject->getCaraTXT()."%' ";
          }

          if ($valueObject->getProcarapieza() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND procarapieza LIKE '".$valueObject->getProcarapieza()."%' ";
          }

          if ($valueObject->getProcsector() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND procsector LIKE '".$valueObject->getProcsector()."%' ";
          }

          if ($valueObject->getMarid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND marid = ".$valueObject->getMarid()." ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getFechaproc() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fechaproc = '".$valueObject->getFechaproc()."' ";
          }

          if ($valueObject->getFecharealizada() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fecharealizada = '".$valueObject->getFecharealizada()."' ";
          }

          if ($valueObject->getCantidad() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cantidad = ".$valueObject->getCantidad()." ";
          }

          if ($valueObject->getProcvalor() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND procvalor = '".$valueObject->getProcvalor()."' ";
          }

          if ($valueObject->getProcvalortotal() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND procvalortotal = '".$valueObject->getProcvalortotal()."' ";
          }

          if ($valueObject->getVlrcopagopres() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND vlrcopagopres = '".$valueObject->getVlrcopagopres()."' ";
          }

          if ($valueObject->getProcautorizar() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND procautorizar = ".$valueObject->getProcautorizar()." ";
          }

          if ($valueObject->getObservaciones() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND observaciones LIKE '".$valueObject->getObservaciones()."%' ";
          }

          if ($valueObject->getMedidprog() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medidprog = ".$valueObject->getMedidprog()." ";
          }

          if ($valueObject->getMedidrealiz() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medidrealiz = ".$valueObject->getMedidrealiz()." ";
          }

          if ($valueObject->getCobid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND cobid = ".$valueObject->getCobid()." ";
          }

          if ($valueObject->getConscob() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND conscob LIKE '".$valueObject->getConscob()."%' ";
          }

          if ($valueObject->getFinalidad() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND finalidad LIKE '".$valueObject->getFinalidad()."%' ";
          }

          if ($valueObject->getAmbito() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND ambito LIKE '".$valueObject->getAmbito()."%' ";
          }

          if ($valueObject->getCostumerid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND costumerid = ".$valueObject->getCostumerid()." ";
          }

          if ($valueObject->getOmedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND omedid = ".$valueObject->getOmedid()." ";
          }

          if ($valueObject->getPptoid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pptoid = ".$valueObject->getPptoid()." ";
          }

          if ($valueObject->getMotivoAnula() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND motivoAnula LIKE '".$valueObject->getMotivoAnula()."%' ";
          }

          if ($valueObject->getPersonal_at() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND personal_at LIKE '".$valueObject->getPersonal_at()."%' ";
          }

          $sql = $sql."ORDER BY procid ASC ";
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);
          return $searchResults;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function singleQuery(&$conn, &$sql, &$valueObject) {
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
          $valida = false;

          foreach($result as $row){ 
                   $valueObject->setProcid($row[0]); 
                   $valueObject->setCodproced($row[1]); 
                   $valueObject->setAteid($row[2]); 
                   $valueObject->setOdonid($row[3]); 
                   $valueObject->setSerid($row[4]); 
                   $valueObject->setDescripcion($row[5]); 
                   $valueObject->setPidid($row[6]); 
                   $valueObject->setCaraTXT($row[7]); 
                   $valueObject->setProcarapieza($row[8]); 
                   $valueObject->setProcsector($row[9]); 
                   $valueObject->setMarid($row[10]); 
                   $valueObject->setEstado($row[11]); 
                   $valueObject->setFechaproc($row[12]); 
                   $valueObject->setFecharealizada($row[13]); 
                   $valueObject->setCantidad($row[14]); 
                   $valueObject->setProcvalor($row[15]); 
                   $valueObject->setProcvalortotal($row[16]); 
                   $valueObject->setVlrcopagopres($row[17]); 
                   $valueObject->setProcautorizar($row[18]); 
                   $valueObject->setObservaciones($row[19]); 
                   $valueObject->setMedidprog($row[20]); 
                   $valueObject->setMedidrealiz($row[21]); 
                   $valueObject->setCobid($row[22]); 
                   $valueObject->setConscob($row[23]); 
                   $valueObject->setFinalidad($row[24]); 
                   $valueObject->setAmbito($row[25]); 
                   $valueObject->setCostumerid($row[26]); 
                   $valueObject->setOmedid($row[27]); 
                   $valueObject->setPptoid($row[28]); 
                   $valueObject->setMotivoAnula($row[29]); 
                   $valueObject->setPersonal_at($row[30]); 
                   $valida=true;
          }
          return $valida;
    }

    /*Rev. Ing. FMBM 27.OCT.2018*/
    public function listQuery(&$conn, &$sql) {
          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setProcid($row[0]); 
               $temp->setCodproced($row[1]); 
               $temp->setAteid($row[2]); 
               $temp->setOdonid($row[3]); 
               $temp->setSerid($row[4]); 
               $temp->setDescripcion($row[5]); 
               $temp->setPidid($row[6]); 
               $temp->setCaraTXT($row[7]); 
               $temp->setProcarapieza($row[8]); 
               $temp->setProcsector($row[9]); 
               $temp->setMarid($row[10]); 
               $temp->setEstado($row[11]); 
               $temp->setFechaproc($row[12]); 
               $temp->setFecharealizada($row[13]); 
               $temp->setCantidad($row[14]); 
               $temp->setProcvalor($row[15]); 
               $temp->setProcvalortotal($row[16]); 
               $temp->setVlrcopagopres($row[17]); 
               $temp->setProcautorizar($row[18]); 
               $temp->setObservaciones($row[19]); 
               $temp->setMedidprog($row[20]); 
               $temp->setMedidrealiz($row[21]); 
               $temp->setCobid($row[22]); 
               $temp->setConscob($row[23]); 
               $temp->setFinalidad($row[24]); 
               $temp->setAmbito($row[25]); 
               $temp->setCostumerid($row[26]); 
               $valueObject->setOmedid($row[27]); 
               $valueObject->setPptoid($row[28]); 
               $valueObject->setMotivoAnula($row[29]); 
               $valueObject->setPersonal_at($row[30]);
               array_push($searchResults, $temp);
          }
          return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectAte(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectAte($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function getOdontograma(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectOdon($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectSer(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectSer($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectPieza(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectPieza($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectMarca(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectMarca($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectMedidprog(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectMedidprog($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectMedidrealiz(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectMedidrealiz($conn);
        return $searchResults;
    }

     /*Ok. Ing. FMBM 27.OCT.2018*/
    public function loadObjectOmed(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectOmed($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 2018/11/18*/
    public function loadObjectPpto(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectPpto($conn);
        return $searchResults;
    }

    /*Ok. Ing. FMBM 27.OCT.2018*/
    public function consultasTabla(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $query->execute();
          // $result = $query->fetchAll(PDO::FETCH_ARRAY); // Genera un Array con índices 
          $result = $query->fetchAll(PDO::FETCH_ASSOC);    // Genera un Array asociativo
          return $result;
    } 
    
}

?>