<?php
 /**
  * Med Data Access Object (DAO).
  * This class contains all database handling that is needed to 
  * permanently store and retrieve Med object instances. 
  * Ing. FMBM 02.NOV.2018
  */

class MedDao {

    // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    function createValueObject() {
          return new Med();
    }

    function getObject(&$conn, $medid) {

          $valueObject = $this->createValueObject();
          $valueObject->setMedid($medid);
          $this->load($conn, $valueObject);
          return $valueObject;
    }

    function load(&$conn, &$valueObject) {

          if (!$valueObject->getMedid()) {
               //print "Can not select without Primary-Key!";
               return false;
          }

          $sql = "SELECT * FROM med WHERE (medid = ".$valueObject->getMedid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    function loadAll(&$conn) {


          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM med ORDER BY medid ASC ";

          $searchResults = $this->listQuery($conn, $sql);

          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;

          return $searchResults;
    }

    function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO med ( saas_ciaid, idmedico, tipo, ";
          $sql = $sql."nombre, mesid, registromedico, ";
          $sql = $sql."direccion, celular, telefonos, ";
          $sql = $sql."beeper, ciuid, sigla, ";
          $sql = $sql."consultorio, estado) VALUES (".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."'".$valueObject->getIdmedico()."', ";
          $sql = $sql."'".$valueObject->getTipo()."', ";
          $sql = $sql."'".$valueObject->getNombre()."', ";
          $sql = $sql."".$valueObject->getMesid().", ";
          $sql = $sql."'".$valueObject->getRegistromedico()."', ";
          $sql = $sql."'".$valueObject->getDireccion()."', ";
          $sql = $sql."'".$valueObject->getCelular()."', ";
          $sql = $sql."'".$valueObject->getTelefonos()."', ";
          $sql = $sql."'".$valueObject->getBeeper()."', ";
          $sql = $sql."".$valueObject->getCiuid().", ";
          $sql = $sql."'".$valueObject->getSigla()."', ";
          $sql = $sql."'".$valueObject->getConsultorio()."', ";
          $sql = $sql."".$valueObject->getEstado().") ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql = "SELECT last_insert_id()";
          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();
        
          $ex = false;

          foreach($result as $row) {
              $valueObject->setMedid($row[0]);
              $ex = true; 
          }

          return $ex;
    }

    function save(&$conn, &$valueObject) {

          $sql = "UPDATE med SET saas_ciaid = ".$valueObject->getSaas_ciaid().", ";
          $sql = $sql."idmedico = '".$valueObject->getIdmedico()."', ";
          $sql = $sql."tipo = '".$valueObject->getTipo()."', ";
          $sql = $sql."nombre = '".$valueObject->getNombre()."', ";
          $sql = $sql."mesid = ".$valueObject->getMesid().", ";
          $sql = $sql."registromedico = '".$valueObject->getRegistromedico()."', ";
          $sql = $sql."direccion = '".$valueObject->getDireccion()."', ";
          $sql = $sql."celular = '".$valueObject->getCelular()."', ";
          $sql = $sql."telefonos = '".$valueObject->getTelefonos()."', ";
          $sql = $sql."beeper = '".$valueObject->getBeeper()."', ";
          $sql = $sql."ciuid = ".$valueObject->getCiuid().", ";
          $sql = $sql."sigla = '".$valueObject->getSigla()."', ";
          $sql = $sql."consultorio = '".$valueObject->getConsultorio()."', ";
          $sql = $sql."estado = ".$valueObject->getEstado()."";
          $sql = $sql." WHERE (medid = ".$valueObject->getMedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }

          return true;
    }

    function delete(&$conn, &$valueObject) {


          if (!$valueObject->getMedid()) {
               //print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM med WHERE (medid = ".$valueObject->getMedid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               //print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    function deleteAll(&$conn) {
          $sql = "DELETE FROM med";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    function countAll(&$conn) {

          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return count($this->cacheData);
          }

          $sql = "SELECT count(*) FROM med";
          $allRows = 0;

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          foreach($result as $row) {
                $allRows = $row[0];
          }

          return $allRows;
    }

    function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM med WHERE 1=1 ";

          if ($valueObject->getMedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND medid = ".$valueObject->getMedid()." ";
          }

          if ($valueObject->getSaas_ciaid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND saas_ciaid = ".$valueObject->getSaas_ciaid()." ";
          }

          if ($valueObject->getIdmedico() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND idmedico LIKE '".$valueObject->getIdmedico()."%' ";
          }

          if ($valueObject->getTipo() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND tipo LIKE '".$valueObject->getTipo()."%' ";
          }

          if ($valueObject->getNombre() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND nombre LIKE '".$valueObject->getNombre()."%' ";
          }

          if ($valueObject->getMesid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND mesid = ".$valueObject->getMesid()." ";
          }

          if ($valueObject->getRegistromedico() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND registromedico LIKE '".$valueObject->getRegistromedico()."%' ";
          }

          if ($valueObject->getDireccion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND direccion LIKE '".$valueObject->getDireccion()."%' ";
          }

          if ($valueObject->getCelular() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND celular LIKE '".$valueObject->getCelular()."%' ";
          }

          if ($valueObject->getTelefonos() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND telefonos LIKE '".$valueObject->getTelefonos()."%' ";
          }

          if ($valueObject->getBeeper() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND beeper LIKE '".$valueObject->getBeeper()."%' ";
          }

          if ($valueObject->getCiuid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND ciuid = ".$valueObject->getCiuid()." ";
          }

          if ($valueObject->getSigla() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND sigla LIKE '".$valueObject->getSigla()."%' ";
          }

          if ($valueObject->getConsultorio() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND consultorio LIKE '".$valueObject->getConsultorio()."%' ";
          }

          if ($valueObject->getEstado() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND estado = ".$valueObject->getEstado()." ";
          }


          $sql = $sql."ORDER BY medid ASC ";

          // Prevent accidential full table results.
          // Use loadAll if all rows must be returned.
          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);

          return $searchResults;
    }

    function databaseUpdate(&$conn, &$sql) {

          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();

          return $result;
    }

    function singleQuery(&$conn, &$sql, &$valueObject) {

          $qry = $conn->prepare($sql);
          $qry->execute();
          $result = $qry->fetchAll();

          $valida = false;

          foreach($result as $row){  

                   $valueObject->setMedid($row[0]); 
                   $valueObject->setSaas_ciaid($row[1]); 
                   $valueObject->setIdmedico($row[2]); 
                   $valueObject->setTipo($row[3]); 
                   $valueObject->setNombre($row[4]); 
                   $valueObject->setMesid($row[5]); 
                   $valueObject->setRegistromedico($row[6]); 
                   $valueObject->setDireccion($row[7]); 
                   $valueObject->setCelular($row[8]); 
                   $valueObject->setTelefonos($row[9]); 
                   $valueObject->setBeeper($row[10]); 
                   $valueObject->setCiuid($row[11]); 
                   $valueObject->setSigla($row[12]); 
                   $valueObject->setConsultorio($row[13]); 
                   $valueObject->setEstado($row[14]); 
                   $valida=true;
          } 
          return $valida;
    }

    function listQuery(&$conn, &$sql) {

          $searchResults = array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setMedid($row[0]); 
               $temp->setSaas_ciaid($row[1]); 
               $temp->setIdmedico($row[2]); 
               $temp->setTipo($row[3]); 
               $temp->setNombre($row[4]); 
               $temp->setMesid($row[5]); 
               $temp->setRegistromedico($row[6]); 
               $temp->setDireccion($row[7]); 
               $temp->setCelular($row[8]); 
               $temp->setTelefonos($row[9]); 
               $temp->setBeeper($row[10]); 
               $temp->setCiuid($row[11]); 
               $temp->setSigla($row[12]); 
               $temp->setConsultorio($row[13]); 
               $temp->setEstado($row[14]); 
               array_push($searchResults, $temp);
          }

          return $searchResults;
    }
}

?>