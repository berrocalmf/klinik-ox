<?php

class OxodondDao {

  // Cache contents:
    var $cacheOk;
    var $cacheData;

    public function __construct() {
        $this->resetCache();
    }

    function resetCache() {
        $this->cacheOk = false;
        $this->cacheData = null;
    }

    public function createValueObject() {
          return new Oxodond();
    }

    public function getObject(&$conn, $odondid) {
          $valueObject = $this->createValueObject();
          $valueObject->setOdondid($odondid);          
          $this->load($conn, $valueObject);
          return $valueObject;
    }

   public function load(&$conn, &$valueObject) {

          if (!$valueObject->getOdondid()) {
               return false;
          }

          $sql = "SELECT * FROM oxodond WHERE (odondid = ".$valueObject->getOdondid().") "; 

          if ($this->singleQuery($conn, $sql, $valueObject))
               return true;
          else
               return false;
    }

    public function loadAll(&$conn) {
          // Check the cache status and use Cache if possible.
          if ($this->cacheOk) {
              return $this->cacheData;
          }

          $sql = "SELECT * FROM oxodond ORDER BY odondid ASC ";
          $searchResults = $this->listQuery($conn, $sql);
          // Update cache and mark it ready.
          $this->cacheData = $searchResults;
          $this->cacheOk = true;
          return $searchResults;
    }

    public function create(&$conn, &$valueObject) {

          $sql = "INSERT INTO oxodond ( odonid, marid, pidid, ";
          $sql = $sql."caras, carasmarca, sectormarca, estado, ";
          $sql = $sql."omedid, descripcion) VALUES (".$valueObject->getOdonid().", ";
          $sql = $sql."".$valueObject->getMarid().", ";
          $sql = $sql."".$valueObject->getPidid().", ";
          $sql = $sql."'".$valueObject->getCaras()."', ";
          $sql = $sql."'".$valueObject->getCarasmarca()."', ";
          $sql = $sql."'".$valueObject->getSectormarca()."', ";
          $sql = $sql."'".$valueObject->getEstado()."', ";
          $sql = $sql."".$valueObject->getOmedid().", ";
          $sql = $sql."'".$valueObject->getDescripcion()."') ";
          $result = $this->databaseUpdate($conn, $sql);

          $sql    ="SELECT last_insert_id()";
          $query  =$conn->prepare($sql); 
          $query->execute();  
          $result =$query->fetchAll();
          $existe =false;  

          foreach ($result as $row) {
              $valueObject->setOdondid($row["odondid"]);
              $existe=true;
          }  
          return $existe;
    }

    public function save(&$conn, &$valueObject) {

          $sql = "UPDATE oxodond SET odonid = ".$valueObject->getOdonid().", ";
          $sql = $sql."marid = ".$valueObject->getMarid().", ";
          $sql = $sql."pidid = ".$valueObject->getPidid().", ";
          $sql = $sql."caras = '".$valueObject->getCaras()."', ";
          $sql = $sql."carasmarca = '".$valueObject->getCarasmarca()."', ";
          $sql = $sql."sectormarca = '".$valueObject->getSectormarca()."', ";
          $sql = $sql."estado = '".$valueObject->getEstado()."', ";
          $sql = $sql."omedid = ".$valueObject->getOmedid().", ";
          $sql = $sql."fregistro = '".$valueObject->getFregistro()."'";
          $sql = $sql."descripcion = '".$valueObject->getDescripcion()."'";
          $sql = $sql." WHERE (odondid = ".$valueObject->getOdondid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {
               return false;
          }
          return true;
    }

    public function delete(&$conn, &$valueObject) {

          if (!$valueObject->getOdondid()) {      // <-- print "Can not delete without Primary-Key!";
               return false;
          }

          $sql = "DELETE FROM oxodond WHERE (odondid = ".$valueObject->getOdondid().") ";
          $result = $this->databaseUpdate($conn, $sql);

          if ($result != 1) {       // <-- print "PrimaryKey Error when updating DB!";
               return false;
          }
          return true;
    }

    public function deleteAll(&$conn) {
          $sql = "DELETE FROM oxodond";
          $result = $this->databaseUpdate($conn, $sql);
          return true;
    }

    public function countAll(&$conn) {

          $sql = "SELECT count(*) FROM oxodond";
          $allRows = 0;
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();

          foreach ($result as $row) {
             $allRows = $row[0]; 
          }  

          return $allRows;
    }

    public function searchMatching(&$conn, &$valueObject) {

          $first = true;
          $sql = "SELECT * FROM oxodond WHERE 1=1 ";

          if ($valueObject->getOdondid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND odondid = ".$valueObject->getOdondid()." ";
          }

          if ($valueObject->getOdonid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND odonid = ".$valueObject->getOdonid()." ";
          }

          if ($valueObject->getMarid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND marid = ".$valueObject->getMarid()." ";
          }

          if ($valueObject->getPidid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND pidid = ".$valueObject->getPidid()." ";
          }

          if ($valueObject->getCaras() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND caras LIKE '".$valueObject->getCaras()."%' ";
          }

          if ($valueObject->getCarasmarca() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND carasmarca LIKE '".$valueObject->getCarasmarca()."%' ";
          }

          if ($valueObject->getSectormarca() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND sectormarca LIKE '".$valueObject->getSectormarca()."%' ";
          }

          if ($valueObject->getEstado() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND estado LIKE '".$valueObject->getEstado()."%' ";
          }

          if ($valueObject->getOmedid() != 0) {
              if ($first) { $first = false; }
              $sql = $sql."AND omedid = ".$valueObject->getOmedid()." ";
          }

          if ($valueObject->getFregistro() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND fregistro = '".$valueObject->getFregistro()."' ";
          }

          if ($valueObject->getDescripcion() != "") {
              if ($first) { $first = false; }
              $sql = $sql."AND descripcion = '".$valueObject->getDescripcion()."' ";
          }

          $sql = $sql."ORDER BY odondid ASC ";

          if ($first)
               return array();

          $searchResults = $this->listQuery($conn, $sql);
          return $searchResults;
    }

    public function databaseUpdate(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $result = $query->execute();
          $this->resetCache();
          return $result;
    }

    // Para cargar datos en un objeto de tipo Oxodond (retorna especificamente un objeto)
    public function singleQuery(&$conn, &$sql, &$valueObject) {
          
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();  
          $existe = false; 

          foreach ($result as $row) {
          // if ($row = $conn->nextRow($result)) {
                   $valueObject->setOdondid($row[0]); 
                   $valueObject->setOdonid($row[1]); 
                   $valueObject->setMarid($row[2]); 
                   $valueObject->setPidid($row[3]); 
                   $valueObject->setCaras($row[4]); 
                   $valueObject->setCarasmarca($row[5]); 
                   $valueObject->setSectormarca($row[6]); 
                   $valueObject->setEstado($row[7]); 
                   $valueObject->setOmedid($row[8]); 
                   $valueObject->setDescripcion($row[9]); 
                   $valueObject->setFregistro($row[10]); 
                   $existe=true;
          }
          return $existe;
    }

    public function listQuery(&$conn, &$sql) {

          $searchResults = []; //array();
          $query = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll(); 

          foreach ($result as $row) {
               $temp = $this->createValueObject();

               $temp->setOdondid($row[0]); 
               $temp->setOdonid($row[1]); 
               $temp->setMarid($row[2]); 
               $temp->setPidid($row[3]); 
               $temp->setCaras($row[4]); 
               $temp->setCarasmarca($row[5]); 
               $temp->setSectormarca($row[6]);
               $temp->setEstado($row[7]); 
               $temp->setOmedid($row[8]);               
               $temp->setDescripcion($row[9]); 
               $temp->setFregistro($row[10]);
               $searchResults[] = $temp;
          }
          return $searchResults;
    }

    // Relacion muchos a uno OXODON
    function loadObjectOxodon(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectOdon($conn);
        return $searchResults;
    }

    // Relacion muchos a uno OXMAR
    function loadObjectOxmar(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectMarca($conn);
        return $searchResults;
    }

    // Relacion muchos a uno OXPID
    function loadObjectOxpid(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectPieza($conn);
        return $searchResults;
    }

    // Relacion muchos a uno omed
    function loadObjectOmed(&$conn,&$valueObject) {
        $searchResults = $valueObject->getObjectOmed($conn);
        return $searchResults;
    }

    // Retorna todos los Detalles de un Odontograma
    // Relación uno a muchos con oxodon (Copia implementada en la clase
    // OxodonDao, donde en realidad se debe implementar)
    // Ing. FMBM

     public function loadAllDetallesOdx(&$conn, $odonid) {      
          $query="select * from oxodond where odonid=".$odonid;
          $arrayOxodon = $this->listQuery($conn, $query);
          return $arrayOxodon;
     } 

     // Copiar todas las marcas de un odontograma o otro
     // Ing. FMBM: Tarea realizada por la clase OXODOND 
     // Debería ser realizada por la clase Oxodon

     public function copyMarcas(&$conn, $odonidorg, $odoniddest) {
          $sql = "CALL spk_copiarMarcasOX(?,?)";
          $query=$conn->prepare($sql);
          $parametros=array (
            $odonidorg,
            $odoniddest
          );

          $query->execute($parametros);
          return true;
     }

     // Ing. FMBM: Consultas generales sobre la tabla oxodond

     public function consultasTabla(&$conn, &$sql) {
          $query  = $conn->prepare($sql);
          $query->execute();
          $result = $query->fetchAll();  
          return $result;
     }

     // Ing. FMBM: Retorna todos los detalles de un odontograma (Relación uno a
     // mucho con oxodon), incluidos los detalles de marca y diente.  [Debería ser
     // implementada por OxodonDao]

     public function listarMarcasArray(&$conn, $odonid) {
           
        $sql="";  
        $sql.="SELECT oxodond.odondid,oxodond.odonid,oxodond.marid,oxmar.codmarca,";
        $sql.="oxodond.pidid,oxpid.codpieza,oxodond.caras,oxodond.carasmarca,oxodond.sectormarca,";
        $sql.="oxodond.estado,oxodond.omedid,oxodond.descripcion,oxodond.fregistro, ";
        $sql.="oxmar.textomarca ";
        $sql.=" FROM oxodond ";
        $sql.="left join oxmar on oxodond.marid=oxmar.marid ";
        $sql.="left join oxpid on oxodond.pidid=oxpid.pidid ";
        $sql.="where odonid=".$odonid;
            //$sql="select * from oxodond where odonid=".$odonid;
        $query  = $conn->prepare($sql);
        $query->execute();
            // $result = $query->fetchAll();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
            // PDO::FETCH_ASSOC
        return $result;
     }   
}

?>