<?php 
	require_once("../connection/Conexion.php");
	require_once("../entity/Oxproc.php");
	require_once("../model/OxprocDao.php");

	class ControllerOxproc {

		private $conexion;

		public function __construct() {
			$conexion=new Conexion();
			$this->conexion=$conexion->getConexion();
		}

		public function fn_getTratamiento($procid) {
			try {
					$arrayTrat     = array();
					$procedimiento = new Oxproc();
					$contProcedi   = new OxprocDao(); 

					$procedimiento->setProcid($procid);

					if($contProcedi->load($this->conexion, $procedimiento)) {
						
						$arrayTrat = $contProcedi->getTratamiento($this->conexion,$procedimiento);
						return $arrayTrat;

					} else {
						return null;
					}	

			} catch(Exception $ex) {
					
					return null;		

			}
		}	

		public function fn_getOdontograma($procid) {
			try {
					$arrayOdon     = array();
					$procedimiento = new Oxproc();
					$contProcedi   = new OxprocDao(); 

					$procedimiento->setProcid($procid);

					if($contProcedi->load($this->conexion, $procedimiento)) {
						
						$arrayOdon = $contProcedi->getOdontograma($this->conexion,$procedimiento);
						return $arrayOdon;

					} else {
						return null;
					}	

			} catch(Exception $ex) {
					
					return null;		

			}
		}

		// Generar listado de Procedimientos de un Odontograma en estados P o F
		public function fn_listaProcediminetosOdx($idodontograma) {
			$sql="";
			$sql="SELECT x.procid,x.codproced,x.tratid,x.odonid,x.serid,x.descripcion,x.pidid,x.caraTXT,x.procarapieza,x.procsector,x.marid,x.estado,x.fechaproc,x.fecharealizada,";
			$sql.="x.cantidad,x.procvalor,x.procvalortotal,x.vlrcopagopres,x.procautorizar,x.observaciones,x.medidprog,x.medidrealiz,x.cobid,x.conscob,x.finalidad,x.ambito,x.costumerid";
			$sql.=" FROM oxproc x ";  // INNER JOIN oxmar y ";
			// $sql.="on x.marid=y.marid ";
			// $sql.="INNER JOIN oxpid d ";
			// $sql.="on x.pidid=d.pidid ";
			$sql.="where x.odonid=" . $idodontograma;
			
			try {
				$contProcedi=new OxprocDao();
				$res=$contProcedi->consultasTabla($this->conexion,$sql);		
				return $res;
			}catch(Exception $ex) {
				echo $ex;
				return null;
			}

		}

		public function cargarProc(&$valueObject) {
			try {
				$contProcedi=new OxprocDao();
				$contProcedi->load($this->conexion,$valueObject);		
			}catch(Exception $ex) {
				echo $ex;
				return null;
			}

		}

		public function fn_getPocedObj(&$valueObject) {
			try {
				$contProcedi=new OxprocDao();
		        $searchResults = $contProcedi->getOdontograma($this->conexion,$valueObject);
		        return $searchResults;
	        }catch(Exception $ex) {
				echo $ex;
				return null;
			}
    	}

	}

?>