<?php
require_once("../connection/Conexion.php"); 
require_once("../model/Us_cgsDao.php");
require_once("../entity/Us_cgs.php");

class ControllerUs_cgs {
	private $conexion2;

	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion(); 
	}

	/* Función retornar un valor consecutivo */
	public function fn_genConsecutivo($prefijo) {
		try	{
			$modeloCop = new Us_cgsDao();
			$consecutivo=$modeloCop->noConsecutivo($this->conexion,"@ODX");
			return $consecutivo;

		} catch(Exception $ex) {
			return "0";
		}
	}

}

?>