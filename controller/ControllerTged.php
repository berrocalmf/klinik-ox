<?php

	require_once("../connection/Conexion.php");
	require_once("../entity/Tged.php");
	require_once("../model/TgedDao.php");

	class ControllerTged {
		private $conexion;

		function __construct() {
			$conexion=new Conexion();
			$this->conexion=$conexion->getConexion();
		}

		function fn_getTGEDValue($tablaIn,$campoIn,$codigoIn) {
			$descripcion="";
			
			try {
				$tgedC=new TgedDao();
				$descripcion = $tgedC->getTGEDValue($this->conexion,$tablaIn,$campoIn,$codigoIn);
				return $descripcion;
			} catch(Exception $ex) {
				echo 'Error: ' . '<br>' . $ex;
				$descripcion=""
				return $descripcion;
			}

		}

	}	

?>