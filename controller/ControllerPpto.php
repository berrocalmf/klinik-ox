<?php
	require_once("../connection/Conexion.php");
	require_once("../entity/Ppto.php");
	require_once("../model/PptoDao.php");

	class ControllerPpto {
	    
	    private $conexion;

		/* Class constructor */
		public function __construct() {
			$conexion=new Conexion();
			$this->conexion=$conexion->getConexion();
		}

		function fn_ListarPagosPpto($idpresupuesto) {
			try {
				$cPpto = new PptoDao();		// Modelo
				$ppto  = new Ppto();		// Entidad	
				$ppto->setPptoid($idpresupuesto);
	
				if($cPpto->load($this->conexion, $ppto)==true) {
					$res=$cPpto->loadPagosPpto($this->conexion,$ppto);
				} else {
					$res=null;
				}
				return $res;
			} catch (Exception $ex) {
				echo "<br>Error: Listar Marcas Odontograma No. " . 	$idodontograma;
				echo "<br>Exception: <br><br>" . $ex;
				return null;
			}
		}

	}
?>