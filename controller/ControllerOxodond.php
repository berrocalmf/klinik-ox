<?php 
require_once("../connection/Conexion.php");
// require("../connection/Conexion.php");			// Objeto para estblecer conexiones
require_once("../entity/Oxodond.php");
require_once("../model/OxodondDao.php");

class ControllerOxodond {
	private $conexion;

	/*
		Constructor de la clase
	*/
	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion();
	}

	/* Copia todas las marcas de un Odontograma a otro */
	public function fn_copiarMarcas($idodonori,$idodondes) {

		try {
			$modeloDetalleOdon=new OxodondDao();
			$modeloDetalleOdon->copyMarcas($this->conexion,$idodonori,$idodondes);
			return true;
		}catch(Exception $ex) {
			return false;
		}
	}

	public function fn_listarEstados($idodontograma) {
		$sql="";
		$sql="SELECT x.odondid,x.odonid,x.marid,y.descripcion,x.pidid,d.codpieza,x.fregistro, x.caras ";
		$sql.="FROM oxodond x INNER JOIN oxmar y ";
		$sql.="on x.marid=y.marid ";
		$sql.="INNER JOIN oxpid d ";
		$sql.="on x.pidid=d.pidid ";
		$sql.="where x.odonid=" . $idodontograma;

		try {
			$modeloDetalleOdon=new OxodondDao();
			$res=$modeloDetalleOdon->consultasTabla($this->conexion,$sql);		
			return $res;
		}catch(Exception $ex) {
			return null;
		}
	}

	public function fn_listarEstadosOdx($idodontograma) {
		try {
			$modeloDetalleOdon=new OxodondDao();
			$arrayEstados=$modeloDetalleOdon->loadAllDetallesOdx($this->conexion,$idodontograma);		
			return $arrayEstados;
		}catch(Exception $ex) {
			return null;
		}
	}

	public function fn_listarMarcasOdx($idodontograma) {
		try {
			$modeloDetalleOdon=new OxodondDao();
			$arrayEstados=$modeloDetalleOdon->listarMarcasArray($this->conexion,$idodontograma);		
			return $arrayEstados;
		}catch(Exception $ex) {
			return null;
		}
	}

}

?>