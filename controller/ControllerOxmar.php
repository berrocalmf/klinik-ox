<?php

require_once("../connection/Conexion.php");
require_once("../entity/Oxmar.php");
require_once("../model/OxmarDao.php");

class ControllerOxmar {

	private $conexion;

	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion();
	}

	/* Retorna objeto con la información de la marca (búsqueda por ID) */

	public function fn_getMarcaxCod($marcaId) {
		$marca = new Oxmar();
		$marca->setMarid($marcaId);

		$modeloMarca = new OxmarDao();
		if($modeloMarca->load($this->conexion, $marca)) {
			return $marca;
		}

		return null;   // Probar esta línea si se carga el objeto con null
	}

	/* Retorna la información de la marca buscada por el código de marca */

	public function fn_getMarcaxId($marcaCod) {
		try {
			$marca = new Oxmar();			// Marca
			$modeloMarca = new OxmarDao();	// Controlador de Marcas

			$sql = "select * from oxmar where codmarca='" . $marcaCod . "'";

			if($modeloMarca->singleQuery($this->conexion, $sql, $marca)) {
				return $marca;
			}
		} catch(Exception $ex) {
			return null;		
		}

		return null;
	}

	/* Retorna el Códgio de un Id de Marca enviado */

	public function fn_getCogigoMarca($marcaId) {
		$codigo="0";
		
		try {
			
			$modeloMarca = new OxmarDao();	// Controlador de Marcas
			$codigo = $modeloMarca->getCodMarca($this->conexion, $marcaId);
			return $codigo;

		} catch(Exception $ex) {
			return $codigo;		
		}	

	}

	/* (13.SEP.2018 - FMBM) Retorna el Id de un código de marca enviado */
	
	public function fn_getIdMarca($marcacod) {
		$id=0;
		
		try {
			
			$modeloMarca = new OxmarDao();	// Controlador de Marcas
			$id = $modeloMarca->getIdMarca($this->conexion, $marcacod);
			return $id;

		} catch(Exception $ex) {
			return $id;		
		}
	}

	/*
		Listado de marcas configuradas en la tabla marcas
		Mod. 2 15.Oct.2018
		Ing. FMBM

	*/
	public function fn_listaMarcasTodas() {
		$arrMarcas = array();
		
		try {
			
			$modeloMarca = new OxmarDao();	// Controlador de Marcas
			$arrMarcas = $modeloMarca->loadAll($this->conexion);
			return $arrMarcas;

		} catch(Exception $ex) {
			return $arrMarcas;		
		}
	}

	/*
		Registro de marcas Odontológicas utilizando un SP de MariaDb
		Mod. 3 17.Oct.2018
		INg. FMBM
	*/
	public function fn_registraMarcas() {

		$odonId      = trim($_POST["idOdontograma"]);
		$arrEstados  = trim($_POST["estados"]);
		$descripcion = trim($_POST["descripcion"]);
		$codDiente   = trim($_POST["codDiente"]);
		$arrCaras    = trim($_POST["caras"]);

		$modeloMarca = new OxmarDao();
		$realizado	 = false;

		try {

			$realizado = $modeloMarca->registraMarcas($this->conexion,$odonId,$arrEstados,$arrCaras,$descripcion,$codDiente);
			
		} catch(Exception $ex) {
			echo $ex;
		}
		
		return $realizado;

	}

}

?>