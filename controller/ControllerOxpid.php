<?php
require_once("../connection/Conexion.php");
require_once("../entity/Oxpid.php");
require_once("../model/OxpidDao.php");

class ControllerOxpid {

	private $conexion;

	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion();
	}

	/* Método para crear Pieza Dentales */
	public function fn_crearOxpid() {
		try {
			$modeloOxpid = new OxpidDao();
			$oxpid = $modeloOxpid->createValueObject();

			$oxpid->setCodpieza($_POST['codpieza']);
			$oxpid->setDescripcion($_POST['descripcion']);
			$oxpid->setTipoPid($_POST['tipoPid']);
			$oxpid->setPidcaracentro($_POST['pidcaracentro']);
			$oxpid->setPidcarasuperior($_POST['pidcarasuperior']);
			$oxpid->setPidcarainferior($_POST['pidcarainferior']);
			$oxpid->setPidcaraizquierda($_POST['pidcaraizquierda']);
			$oxpid->setPidcaraderecha($_POST['pidcaraderecha']);

			if($modeloOxpid->crear($this->conexion, $oxpid)) {
				return true;	
			} else {
				return false;	
			}

		} catch(Exception $ex) {
			return false;
		}	

	}

}	


?>