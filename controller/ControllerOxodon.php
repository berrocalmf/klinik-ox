<?php

require_once("../connection/Conexion.php");
require_once("../entity/Oxodon.php");
require_once("../model/OxodonDao.php");
require_once("ControllerOxodond.php");
require_once("ControllerUs_cgs.php");

class ControllerOxodon {

	private $conexion;

	/* Class constructor */
	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion();
	}

	// Crear un nuevo Odontograma
	// Ing. FBERROCALM

	public function fn_registrarOdontograma() {
		
		$noFicha=0;
		$noOdontograma;			
		$arrayOdontograma;
		$ultimoIdOdx=0;	
		$modeloodontograma =new OxodonDao();
		$Odontograma       =new Oxodon();
		$contDetalleOdon   =new ControllerOxodond();
		$contConsec        =new ControllerUs_cgs();

		$noFicha=$modeloodontograma->noFichas($this->conexion,$_POST["afiid"]);
		
		if($noFicha!=0) {
			$arrayOdontograma=$modeloodontograma->loadUltimoOdonAfi($this->conexion, $_POST["afiid"]);
			$ultimoIdOdx=count($arrayOdontograma)==0?0:$arrayOdontograma[0]->getOdonid();			
		} else {
			$ultimoIdOdx=0;
		}

		$noFicha=($noFicha + 1);
		$noOdontograma = "01" . str_pad($contConsec->fn_genConsecutivo("@ODX"),8,"0",STR_PAD_LEFT);

		try {
			$Odontograma->setNoodontogram($noOdontograma);
			$Odontograma->setNroficha($noFicha);
			$Odontograma->setFecha(null);
			$Odontograma->setMedid($_POST["medid"]);			
			$Odontograma->setAteid($_POST["ateid"]);		
			$Odontograma->setAfiid($_POST["afiid"]);
			$Odontograma->setEstado("P");
			$Odontograma->setDenttemporal($_POST["denttemporal"]);
			$Odontograma->setIndiceCPOD($_POST["indiceCPOD"]);
			$Odontograma->setIndiceCEOD($_POST["indiceCEOD"]);
			$Odontograma->setCustomerid($_POST["customerid"]);
			$Odontograma->setCitid($_POST["citid"]);
			$Odontograma->setOdxinicial($_POST["odxinicial"]);
			$Odontograma->setMotivoanula($_POST["motivoAnula"]);

			if($modeloodontograma->create($this->conexion,$Odontograma)==true) {
				// Arrastre de marcas Odontológicas:
				if($ultimoIdOdx != 0) {
					$contDetalleOdon->fn_copiarMarcas($ultimoIdOdx,$Odontograma->getOdonid());
				} 	
			}			
			return true;
		} catch(Exception $ex) {
			return false;		
		}
	}

	// Proceso para <listar> todos los odontogramas con una Instrucción SQL
	// Ing. FBERROCALM

	public function fn_listarOdontogramas($sql) {
		try {
			$modeloodontograma=new OxodonDao();										
			// $sql = "select * from oxodon where afiid=" . $afiId;	
			$arrayTOdontograma=$modeloodontograma->listQuery($this->conexion, $sql);
			return $arrayTOdontograma;
		} catch(Exception $e) {
			return null;
		}
	}	

	// Proceso para <listar> todos los odontogramas de un afiliado
	// Ing. FBERROCALM

	public function fn_listarOdonPaciente($afiId) {
		try {
			$modeloodontograma = new OxodonDao();										
			$sql = "select * from oxodon where pacid=" . $afiId;	
			
			$arrayTOdontograma = $modeloodontograma->listQuery($this->conexion, $sql);
			
			return $arrayTOdontograma;
		} catch(Exception $e) {
			return null;
		}
	}

	// Generar listado de Procedimientos de un Odontograma en estados 'P' o 'F'
	// Consulta con joins para aumentar el detalle de la información solicitada
	// Relación uno a muchos entre Oxodon y Oxproc.
	// Ing. FBERROCALM - DESCARTADO
	
	public function fn_listaOmedOdx($idodontograma) {
		$sql="";
		$sql="SELECT x.omedid,x.omeid,o.fechaome,x.noitem,x.serid,y.nomservicio,x.cantidad,x.impreso,x.comentarios,x.fechares,x.resultados,x.pidid,d.codpieza,x.proccarapieza,x.procsector,x.marid,m.codmarca,x.estado,x.fecharealizado,x.procautorizado,x.idmedicorealiza,z.nombre,x.odonid";
		$sql.=" FROM omed x";  // INNER JOIN oxmar y ";
		$sql.=" INNER JOIN ser y on x.serid=y.serid";
		$sql.=" INNER JOIN ome o on x.omeid=o.omeid";
		$sql.=" LEFT JOIN oxpid d on x.pidid=d.pidid";
		$sql.=" LEFT JOIN oxmar m on x.marid=m.marid";
		$sql.=" LEFT JOIN med z on x.idmedicorealiza=z.medid ";
		$sql.="where x.odonid=" . $idodontograma;
		
		try {
			$cOxodon=new OxodonDao();
			$res=$cOxodon->consultasTabla($this->conexion,$sql);		
			return $res;
		}catch(Exception $ex) {
			echo $ex;
			return null;
		}
	}

	// Generar listado de Procedimientos asociados a un Presupuesto en cualquier estado
	// Ing. FBERROCALM.

	public function fn_listaProcediminetosOdx($idppto,$estado) {
		$sql="";
		$sql="SELECT x.procid,x.codproced,x.ateid,x.odonid,x.serid,x.descripcion,x.pidid,d.codpieza,x.caraTXT,x.procarapieza,x.procsector,x.marid,m.codmarca,x.estado,x.fechaproc,x.fecharealizada,";
		$sql.="x.cantidad,x.procvalor,x.procvalortotal,x.vlrcopagopres,x.procautorizar,x.observaciones,x.medidprog,x.medidrealiz,z.nombre,x.cobid,x.conscob,x.finalidad,x.ambito,x.customerid,x.omedid,x.pptoid,x.motivoAnula,x.personal_at";
		$sql.=" FROM oxproc x "; 
		$sql.=" LEFT JOIN oxpid d on x.pidid=d.pidid";
		$sql.=" LEFT JOIN oxmar m on x.marid=m.marid";
		$sql.=" LEFT JOIN med z on x.medidrealiz=z.medid ";
		$sql.="where x.pptoid={$idppto} and x.estado='{$estado}'";
		
		try {
			$contProcedi=new OxodonDao();
			$res=$contProcedi->consultasTabla($this->conexion,$sql);		
			return $res;
		}catch(Exception $ex) {
			echo $ex;
			return null;
		}
	}

	// Generar listado de Procedimientos asociados a un Presupuesto
	// Ing. FBERROCALM.

	public function fn_listaProcediminetosPpto($idppto) {
		$sql="";
		$sql="SELECT x.procid,x.codproced,x.ateid,x.odonid,x.serid,x.descripcion,x.pidid,d.codpieza,x.caraTXT,x.procarapieza,x.procsector,x.marid,m.codmarca,x.estado,x.fechaproc,x.fecharealizada,";
		$sql.="x.cantidad,x.procvalor,x.procvalortotal,x.vlrcopagopres,x.procautorizar,x.observaciones,x.medidprog,x.medidrealiz,z.nombre,x.cobid,x.conscob,x.finalidad,x.ambito,x.customerid,x.omedid,x.pptoid,x.motivoAnula,x.personal_at";
		$sql.=" FROM oxproc x "; 
		$sql.=" LEFT JOIN oxpid d on x.pidid=d.pidid";
		$sql.=" LEFT JOIN oxmar m on x.marid=m.marid";
		$sql.=" LEFT JOIN med z on x.medidrealiz=z.medid ";
		$sql.="where x.pptoid={$idppto}";    // and x.estado='{$estado}'";
		
		try {
			$contProcedi=new OxodonDao();
			$res=$contProcedi->consultasTabla($this->conexion,$sql);		
			return $res;
		}catch(Exception $ex) {
			echo $ex;
			return null;
		}
	}

	// Generar listado de Procedimientos asociados a un Odontograma en cualquier estado
	// Ing. FBERROCALM.

	public function fn_listaProcedOdxAct($idOdontograma) {
		$sql="";
		$sql="SELECT x.procid,x.codproced,x.ateid,x.odonid,x.serid,x.descripcion,x.pidid,d.codpieza,x.caraTXT,x.procarapieza,x.procsector,x.marid,m.codmarca,x.estado,x.fechaproc,x.fecharealizada,";
		$sql.="x.cantidad,x.procvalor,x.procvalortotal,x.vlrcopagopres,x.procautorizar,x.observaciones,x.medidprog,x.medidrealiz,z.nombre,x.cobid,x.conscob,x.finalidad,x.ambito,x.customerid,x.omedid,x.pptoid,x.motivoAnula,x.personal_at";
		$sql.=" FROM oxproc x "; 
		$sql.=" LEFT JOIN oxpid d on x.pidid=d.pidid";
		$sql.=" LEFT JOIN oxmar m on x.marid=m.marid";
		$sql.=" LEFT JOIN med z on x.medidrealiz=z.medid ";
		$sql.="where x.odonid={$idOdontograma}";    // and x.estado='{$estado}'";
		
		try {
			$contProcedi=new OxodonDao();
			$res=$contProcedi->consultasTabla($this->conexion,$sql);		
			return $res;
		}catch(Exception $ex) {
			echo $ex;
			return null;
		}
	}

	// Generar listado de Procedimientos de un Odontograma en estados P o F
	// Ing. FBERROCALM.

	function fn_listProcedimientos($idodontograma) {
		try {
			$cOxodon=new OxodonDao();
			$res=$cOxodon->listadoProcediminetos($this->conexion,$idodontograma);	
			return $res;
		}catch(Exception $ex) {
			echo $ex;
			return null;
		}
	}

	// Generar listado de Marcas dentales de un Odontograma
	// Ing. FBERROCALM. 2018/11/29

	function fn_ListarEstadosOdx($idodontograma) {
		try {
			$cOxodon=new OxodonDao();		// Modelo
			$odx=new Oxodon();				// Entidad	
			$odx->setOdonid($idodontograma);

			if($cOxodon->load($this->conexion, $odx)==true) {
				$res=$cOxodon->listDetallesOdx($this->conexion,$odx);
			} else {
				$res=null;
			}
			return $res;
		} catch (Exception $ex) {
			echo "<br>Error: Listar Marcas Odontograma No. " . 	$idodontograma;
			echo "<br>Exception: <br><br>" . $ex;
			return null;
		}
	}

}

?>