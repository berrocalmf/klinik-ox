<?php
require_once("../connection/Conexion.php");
require_once("../entity/Ate.php");
require_once("../model/AteDao.php");

class ControllerAte {
    
    private $conexion;

	/* Class constructor */
	public function __construct() {
		$conexion=new Conexion();
		$this->conexion=$conexion->getConexion();
	}

    // Retorna información del presupuesto de una Atención Odontológica
    // Parámetros
    // ateid: int
    // Ing. FMBM 2019-01-21
    public function fn_getPresupuesto($ateid) {
        try {

            $arrPresupuesto = array();
            $ateC = new AteDao();
            $ate  = new Ate();
            $ate->setAteid($ateid);
            
            if($ateC->load($this->conexion, $ate)==true) {
				$arrPresupuesto=$ateC->loadPresupuesto($this->conexion,$ate);
			} else {
				$res=null;
			}
			return $arrPresupuesto;     // <-- Arreglo de objetos de tipo Ppto

        } catch (Exception $ex) {
            echo "<br>Exception: <br><br>" . $ex;
			return null;
        }
        
    }

}
?>