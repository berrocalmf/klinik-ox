<!doctype html>
<html lang="sp">
<head>
  	<meta charset="UTF-8">
  	<link rel="icon" type="image/x-icon" href="./public/img/Tooth.ico" />
  	<title>..: HISTORIA CLINICA ODONTOLOGICA :..</title>

  	<!-- Hojas de Estilo: -->
  	<link rel="stylesheet" href="../public/css/cssDiente.css">
  	<link rel="stylesheet" href="../public/css/cssDienteGeneral.css">
  	<link rel="stylesheet" href="../public/css/cssFormulario.css">
  	<link rel="stylesheet" href="../public/css/cssComponentes.css">
  	<link rel="stylesheet" href="../public/css/cssComponentesPersonalizados.css">
  	<link rel="stylesheet" href="../public/css/cssContenido.css">
    <link rel="stylesheet" href="../public/css/cssEstiloPestannas.css">

    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
   
    <style type="text/css">
            #div1 {width:350px;height:70px;padding:10px;border:1px solid #aaaaaa;}
    </style>

    <style>
          /* Set height of the grid so .sidenav can be 100% (adjust if needed) */
          .row.content {height: 1500px}
          
          /* Set gray background color and 100% height */
          .sidenav {
            background-color: #f1f1f1;
            height: 100%;
          }
          
          /* Set black background color, white text and some padding */
          footer {
            background-color: #555;
            color: white;
            padding: 15px;
          }

          header {
            background-color: #555;
            color: white;
            padding: 15px;
          }
          
          /* On small screens, set height to 'auto' for sidenav and grid */
          @media screen and (max-width: 767px) {
            .sidenav {
              height: auto;
              padding: 15px;
            }
            .row.content {height: auto;} 
          }
    </style>

    <!-- Librerías bootstrap y jquery -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Librerias Javascript y JQuery (Es muy importante el orden en que se linkean los archivos de Scripts Jquery debe estar antes de todos los otros)-->
  	
    <script src="../public/js/jquery-2.0.3.min.js"></script>	
    <script src="../public/js/jsAcciones.js"></script>
    <script type="text/javascript" src="../public/js/cambiarPestanna.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

</head>
<body>  <!-- El encabezado contiene el Tag de apertura del bloque Body -->
