<?php

 /**
  * Us_cgs Value Object.
  * This class is value object representing database table us_cgs
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 02.NOV.2018
    Reviewed: Ok. Ing. FMBM 2018/12/20 10:37
  */

class Us_cgs {

    private $us_cgsid;
    private $saas_ciaid;
    private $saas_ciasid;
    private $prefijo;
    private $consecutivo;

    public function __construct(){}

    function getUs_cgsid() {
          return $this->us_cgsid;
    }
    function setUs_cgsid($us_cgsidIn) {
          $this->us_cgsid = $us_cgsidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getSaas_ciasid() {
          return $this->saas_ciasid;
    }
    function setSaas_ciasid($saas_ciasidIn) {
          $this->saas_ciasid = $saas_ciasidIn;
    }

    function getPrefijo() {
          return $this->prefijo;
    }
    function setPrefijo($prefijoIn) {
          $this->prefijo = $prefijoIn;
    }

    function getConsecutivo() {
          return $this->consecutivo;
    }
    function setConsecutivo($consecutivoIn) {
          $this->consecutivo = $consecutivoIn;
    }

    function setAll($us_cgsidIn,
          $saas_ciaidIn,
          $saas_ciasidIn,
          $prefijoIn,
          $consecutivoIn) {
          $this->us_cgsid = $us_cgsidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->saas_ciasid = $saas_ciasidIn;
          $this->prefijo = $prefijoIn;
          $this->consecutivo = $consecutivoIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getUs_cgsid() != $this->us_cgsid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciasid() != $this->saas_ciasid) {
                    return(false);
          }
          if ($valueObject->getPrefijo() != $this->prefijo) {
                    return(false);
          }
          if ($valueObject->getConsecutivo() != $this->consecutivo) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Us_cgs, mapping to table us_cgs\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."Id = ".$this->us_cgsid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."saas_ciasid = ".$this->saas_ciasid."\n"; 
        $out = $out."prefijo = ".$this->prefijo."\n"; 
        $out = $out."consecutivo = ".$this->consecutivo."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Us_cgs();

        $cloned->setUs_cgsid($this->us_cgsid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setSaas_ciasid($this->saas_ciasid); 
        $cloned->setPrefijo($this->prefijo); 
        $cloned->setConsecutivo($this->consecutivo); 

        return $cloned;
    }

}

?>