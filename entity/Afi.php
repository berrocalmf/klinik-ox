<?php
require_once("../model/AteDao.php");
 /**
  * Afi Value Object.
  * This class is value object representing database table afi
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 02.NOV.2018
  * Revisado: Ok. 2018/12/20 09:19
  */

class Afi {

    private $afiid;
    private $saas_ciaid;
    private $papellido;
    private $sapellido;
    private $pnombre;
    private $snombre;
    private $tipodoc;
    private $docidafiliado;
    private $email;
    private $celular;
    private $telefonores;
    private $telefonoofi;
    private $idalterna;
    private $fechanacimiento;
    private $gruposanguineo;
    private $estadocivil;
    private $grupoetnico;
    private $sexo;
    private $localidad;
    private $tipozona;
    private $direccion;
    private $cpostal;
    private $ciuid;
    private $estado;
    private $ocuid;
    private $tercid;
    private $nivel;
    private $fechaultimavis;
    private $incapacidadlaboral;
    private $tipodiscapacidad;
    private $gradodiscapacidad;
    private $tipoafiliado;
    private $nocaso;
    private $remitidopor;
    private $obs;

    public function __construct(){}

    function getAfiid() {
          return $this->afiid;
    }
    function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getPapellido() {
          return $this->papellido;
    }
    function setPapellido($papellidoIn) {
          $this->papellido = $papellidoIn;
    }

    function getSapellido() {
          return $this->sapellido;
    }
    function setSapellido($sapellidoIn) {
          $this->sapellido = $sapellidoIn;
    }

    function getPnombre() {
          return $this->pnombre;
    }
    function setPnombre($pnombreIn) {
          $this->pnombre = $pnombreIn;
    }

    function getSnombre() {
          return $this->snombre;
    }
    function setSnombre($snombreIn) {
          $this->snombre = $snombreIn;
    }

    function getTipodoc() {
          return $this->tipodoc;
    }
    function setTipodoc($tipodocIn) {
          $this->tipodoc = $tipodocIn;
    }

    function getDocidafiliado() {
          return $this->docidafiliado;
    }
    function setDocidafiliado($docidafiliadoIn) {
          $this->docidafiliado = $docidafiliadoIn;
    }

    function getEmail() {
          return $this->email;
    }
    function setEmail($emailIn) {
          $this->email = $emailIn;
    }

    function getCelular() {
          return $this->celular;
    }
    function setCelular($celularIn) {
          $this->celular = $celularIn;
    }

    function getTelefonores() {
          return $this->telefonores;
    }
    function setTelefonores($telefonoresIn) {
          $this->telefonores = $telefonoresIn;
    }

    function getTelefonoofi() {
          return $this->telefonoofi;
    }
    function setTelefonoofi($telefonoofiIn) {
          $this->telefonoofi = $telefonoofiIn;
    }

    function getIdalterna() {
          return $this->idalterna;
    }
    function setIdalterna($idalternaIn) {
          $this->idalterna = $idalternaIn;
    }

    function getFechanacimiento() {
          return $this->fechanacimiento;
    }
    function setFechanacimiento($fechanacimientoIn) {
          $this->fechanacimiento = $fechanacimientoIn;
    }

    function getGruposanguineo() {
          return $this->gruposanguineo;
    }
    function setGruposanguineo($gruposanguineoIn) {
          $this->gruposanguineo = $gruposanguineoIn;
    }

    function getEstadocivil() {
          return $this->estadocivil;
    }
    function setEstadocivil($estadocivilIn) {
          $this->estadocivil = $estadocivilIn;
    }

    function getGrupoetnico() {
          return $this->grupoetnico;
    }
    function setGrupoetnico($grupoetnicoIn) {
          $this->grupoetnico = $grupoetnicoIn;
    }

    function getSexo() {
          return $this->sexo;
    }
    function setSexo($sexoIn) {
          $this->sexo = $sexoIn;
    }

    function getLocalidad() {
          return $this->localidad;
    }
    function setLocalidad($localidadIn) {
          $this->localidad = $localidadIn;
    }

    function getTipozona() {
          return $this->tipozona;
    }
    function setTipozona($tipozonaIn) {
          $this->tipozona = $tipozonaIn;
    }

    function getDireccion() {
          return $this->direccion;
    }
    function setDireccion($direccionIn) {
          $this->direccion = $direccionIn;
    }

    function getCpostal() {
          return $this->cpostal;
    }
    function setCpostal($cpostalIn) {
          $this->cpostal = $cpostalIn;
    }

    function getCiuid() {
          return $this->ciuid;
    }
    function setCiuid($ciuidIn) {
          $this->ciuid = $ciuidIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function getOcuid() {
          return $this->ocuid;
    }
    function setOcuid($ocuidIn) {
          $this->ocuid = $ocuidIn;
    }

    function getTercid() {
          return $this->tercid;
    }
    function setTercid($tercidIn) {
          $this->tercid = $tercidIn;
    }

    function getNivel() {
          return $this->nivel;
    }
    function setNivel($nivelIn) {
          $this->nivel = $nivelIn;
    }

    function getFechaultimavis() {
          return $this->fechaultimavis;
    }
    function setFechaultimavis($fechaultimavisIn) {
          $this->fechaultimavis = $fechaultimavisIn;
    }

    function getIncapacidadlaboral() {
          return $this->incapacidadlaboral;
    }
    function setIncapacidadlaboral($incapacidadlaboralIn) {
          $this->incapacidadlaboral = $incapacidadlaboralIn;
    }

    function getTipodiscapacidad() {
          return $this->tipodiscapacidad;
    }
    function setTipodiscapacidad($tipodiscapacidadIn) {
          $this->tipodiscapacidad = $tipodiscapacidadIn;
    }

    function getGradodiscapacidad() {
          return $this->gradodiscapacidad;
    }
    function setGradodiscapacidad($gradodiscapacidadIn) {
          $this->gradodiscapacidad = $gradodiscapacidadIn;
    }

    function getTipoafiliado() {
          return $this->tipoafiliado;
    }
    function setTipoafiliado($tipoafiliadoIn) {
          $this->tipoafiliado = $tipoafiliadoIn;
    }

    function getNocaso() {
          return $this->nocaso;
    }
    function setNocaso($nocasoIn) {
          $this->nocaso = $nocasoIn;
    }

    function getRemitidopor() {
          return $this->remitidopor;
    }
    function setRemitidopor($remitidoporIn) {
          $this->remitidopor = $remitidoporIn;
    }

    function getObs() {
          return $this->obs;
    }
    function setObs($obsIn) {
          $this->obs = $obsIn;
    }

    function setAll($afiidIn,
          $saas_ciaidIn,
          $papellidoIn,
          $sapellidoIn,
          $pnombreIn,
          $snombreIn,
          $tipodocIn,
          $docidafiliadoIn,
          $emailIn,
          $celularIn,
          $telefonoresIn,
          $telefonoofiIn,
          $idalternaIn,
          $fechanacimientoIn,
          $gruposanguineoIn,
          $estadocivilIn,
          $grupoetnicoIn,
          $sexoIn,
          $localidadIn,
          $tipozonaIn,
          $direccionIn,
          $cpostalIn,
          $ciuidIn,
          $estadoIn,
          $ocuidIn,
          $tercidIn,
          $nivelIn,
          $fechaultimavisIn,
          $incapacidadlaboralIn,
          $tipodiscapacidadIn,
          $gradodiscapacidadIn,
          $tipoafiliadoIn,
          $nocasoIn,
          $remitidoporIn,
          $obsIn) {
          $this->afiid = $afiidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->papellido = $papellidoIn;
          $this->sapellido = $sapellidoIn;
          $this->pnombre = $pnombreIn;
          $this->snombre = $snombreIn;
          $this->tipodoc = $tipodocIn;
          $this->docidafiliado = $docidafiliadoIn;
          $this->email = $emailIn;
          $this->celular = $celularIn;
          $this->telefonores = $telefonoresIn;
          $this->telefonoofi = $telefonoofiIn;
          $this->idalterna = $idalternaIn;
          $this->fechanacimiento = $fechanacimientoIn;
          $this->gruposanguineo = $gruposanguineoIn;
          $this->estadocivil = $estadocivilIn;
          $this->grupoetnico = $grupoetnicoIn;
          $this->sexo = $sexoIn;
          $this->localidad = $localidadIn;
          $this->tipozona = $tipozonaIn;
          $this->direccion = $direccionIn;
          $this->cpostal = $cpostalIn;
          $this->ciuid = $ciuidIn;
          $this->estado = $estadoIn;
          $this->ocuid = $ocuidIn;
          $this->tercid = $tercidIn;
          $this->nivel = $nivelIn;
          $this->fechaultimavis = $fechaultimavisIn;
          $this->incapacidadlaboral = $incapacidadlaboralIn;
          $this->tipodiscapacidad = $tipodiscapacidadIn;
          $this->gradodiscapacidad = $gradodiscapacidadIn;
          $this->tipoafiliado = $tipoafiliadoIn;
          $this->nocaso = $nocasoIn;
          $this->remitidopor = $remitidoporIn;
          $this->obs = $obsIn;
    }
    
    function hasEqualMapping($valueObject) {

          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getPapellido() != $this->papellido) {
                    return(false);
          }
          if ($valueObject->getSapellido() != $this->sapellido) {
                    return(false);
          }
          if ($valueObject->getPnombre() != $this->pnombre) {
                    return(false);
          }
          if ($valueObject->getSnombre() != $this->snombre) {
                    return(false);
          }
          if ($valueObject->getTipodoc() != $this->tipodoc) {
                    return(false);
          }
          if ($valueObject->getDocidafiliado() != $this->docidafiliado) {
                    return(false);
          }
          if ($valueObject->getEmail() != $this->email) {
                    return(false);
          }
          if ($valueObject->getCelular() != $this->celular) {
                    return(false);
          }
          if ($valueObject->getTelefonores() != $this->telefonores) {
                    return(false);
          }
          if ($valueObject->getTelefonoofi() != $this->telefonoofi) {
                    return(false);
          }
          if ($valueObject->getIdalterna() != $this->idalterna) {
                    return(false);
          }
          if ($valueObject->getFechanacimiento() != $this->fechanacimiento) {
                    return(false);
          }
          if ($valueObject->getGruposanguineo() != $this->gruposanguineo) {
                    return(false);
          }
          if ($valueObject->getEstadocivil() != $this->estadocivil) {
                    return(false);
          }
          if ($valueObject->getGrupoetnico() != $this->grupoetnico) {
                    return(false);
          }
          if ($valueObject->getSexo() != $this->sexo) {
                    return(false);
          }
          if ($valueObject->getLocalidad() != $this->localidad) {
                    return(false);
          }
          if ($valueObject->getTipozona() != $this->tipozona) {
                    return(false);
          }
          if ($valueObject->getDireccion() != $this->direccion) {
                    return(false);
          }
          if ($valueObject->getCpostal() != $this->cpostal) {
                    return(false);
          }
          if ($valueObject->getCiuid() != $this->ciuid) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getOcuid() != $this->ocuid) {
                    return(false);
          }
          if ($valueObject->getTercid() != $this->tercid) {
                    return(false);
          }
          if ($valueObject->getNivel() != $this->nivel) {
                    return(false);
          }
          if ($valueObject->getFechaultimavis() != $this->fechaultimavis) {
                    return(false);
          }
          if ($valueObject->getIncapacidadlaboral() != $this->incapacidadlaboral) {
                    return(false);
          }
          if ($valueObject->getTipodiscapacidad() != $this->tipodiscapacidad) {
                    return(false);
          }
          if ($valueObject->getGradodiscapacidad() != $this->gradodiscapacidad) {
                    return(false);
          }
          if ($valueObject->getTipoafiliado() != $this->tipoafiliado) {
                    return(false);
          }
          if ($valueObject->getNocaso() != $this->nocaso) {
                    return(false);
          }
          if ($valueObject->getRemitidopor() != $this->remitidopor) {
                    return(false);
          }
          if ($valueObject->getObs() != $this->obs) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Afi, mapping to table afi\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."papellido = ".$this->papellido."\n"; 
        $out = $out."sapellido = ".$this->sapellido."\n"; 
        $out = $out."pnombre = ".$this->pnombre."\n"; 
        $out = $out."snombre = ".$this->snombre."\n"; 
        $out = $out."tipodoc = ".$this->tipodoc."\n"; 
        $out = $out."docidafiliado = ".$this->docidafiliado."\n"; 
        $out = $out."email = ".$this->email."\n"; 
        $out = $out."celular = ".$this->celular."\n"; 
        $out = $out."telefonores = ".$this->telefonores."\n"; 
        $out = $out."telefonoofi = ".$this->telefonoofi."\n"; 
        $out = $out."idalterna = ".$this->idalterna."\n"; 
        $out = $out."fechanacimiento = ".$this->fechanacimiento."\n"; 
        $out = $out."gruposanguineo = ".$this->gruposanguineo."\n"; 
        $out = $out."estadocivil = ".$this->estadocivil."\n"; 
        $out = $out."grupoetnico = ".$this->grupoetnico."\n"; 
        $out = $out."sexo = ".$this->sexo."\n"; 
        $out = $out."localidad = ".$this->localidad."\n"; 
        $out = $out."tipozona = ".$this->tipozona."\n"; 
        $out = $out."direccion = ".$this->direccion."\n"; 
        $out = $out."cpostal = ".$this->cpostal."\n"; 
        $out = $out."ciuid = ".$this->ciuid."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."ocuid = ".$this->ocuid."\n"; 
        $out = $out."tercid = ".$this->tercid."\n"; 
        $out = $out."nivel = ".$this->nivel."\n"; 
        $out = $out."fechaultimavis = ".$this->fechaultimavis."\n"; 
        $out = $out."incapacidadlaboral = ".$this->incapacidadlaboral."\n"; 
        $out = $out."tipodiscapacidad = ".$this->tipodiscapacidad."\n"; 
        $out = $out."gradodiscapacidad = ".$this->gradodiscapacidad."\n"; 
        $out = $out."tipoafiliado = ".$this->tipoafiliado."\n"; 
        $out = $out."nocaso = ".$this->nocaso."\n"; 
        $out = $out."remitidopor = ".$this->remitidopor."\n"; 
        $out = $out."obs = ".$this->obs."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Afi();

        $cloned->setAfiid($this->afiid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setPapellido($this->papellido); 
        $cloned->setSapellido($this->sapellido); 
        $cloned->setPnombre($this->pnombre); 
        $cloned->setSnombre($this->snombre); 
        $cloned->setTipodoc($this->tipodoc); 
        $cloned->setDocidafiliado($this->docidafiliado); 
        $cloned->setEmail($this->email); 
        $cloned->setCelular($this->celular); 
        $cloned->setTelefonores($this->telefonores); 
        $cloned->setTelefonoofi($this->telefonoofi); 
        $cloned->setIdalterna($this->idalterna); 
        $cloned->setFechanacimiento($this->fechanacimiento); 
        $cloned->setGruposanguineo($this->gruposanguineo); 
        $cloned->setEstadocivil($this->estadocivil); 
        $cloned->setGrupoetnico($this->grupoetnico); 
        $cloned->setSexo($this->sexo); 
        $cloned->setLocalidad($this->localidad); 
        $cloned->setTipozona($this->tipozona); 
        $cloned->setDireccion($this->direccion); 
        $cloned->setCpostal($this->cpostal); 
        $cloned->setCiuid($this->ciuid); 
        $cloned->setEstado($this->estado); 
        $cloned->setOcuid($this->ocuid); 
        $cloned->setTercid($this->tercid); 
        $cloned->setNivel($this->nivel); 
        $cloned->setFechaultimavis($this->fechaultimavis); 
        $cloned->setIncapacidadlaboral($this->incapacidadlaboral); 
        $cloned->setTipodiscapacidad($this->tipodiscapacidad); 
        $cloned->setGradodiscapacidad($this->gradodiscapacidad); 
        $cloned->setTipoafiliado($this->tipoafiliado); 
        $cloned->setNocaso($this->nocaso); 
        $cloned->setRemitidopor($this->remitidopor); 
        $cloned->setObs($this->obs); 

        return $cloned;
    }
    
    // Retorna un Array con objetos de tipo ATE (Relación de asociación muchos) 
    // Ok. Ing. FMBM 02.NOV.2018
    function getObjecstAte(&$conn) {
          $arrayObjetos=[];
          $cAte = new AteDao(); 
          $sql = "SELECT * FROM ate WHERE (afiid = ".$this->afiid.") "; 
        
          $arrayObjetos =  $cAte->listQuery($conn,$sql);
          return $arrayObjetos;    
    }

}

?>