<?php

 /**
  * Ser Value Object.
  * This class is value object representing database table ser
  * This class is intented to be used together with associated Dao object.
  * Créé par Ing. FMBM 2018.11.05
  * Revu et validé par Ing. FMBM 
    Reviewed: Ok. Ing. FMBM 2018/12/20 10:37
  */

class Ser {

    private $serid;
    private $saas_ciaid;
    private $preid;         // <-- FK
    private $codservicio;
    private $idalterna;
    private $nomservicio;
    private $nomgenerico;
    private $prescripcion;
    private $cantidad;
    private $sexo;
    private $edadinicial;
    private $edadfinal;
    private $cobrable;
    private $pyp;
    private $procedimiento;
    private $manualt;
    private $tratamiento;
    private $tipomedicamento;
    private $formafarma;
    private $concentracion;
    private $umedida;
    private $obs;
    private $rencrid;       // <-- FK
    private $estado;

    public function __construct(){}

    function getSerid() {
          return $this->serid;
    }
    function setSerid($seridIn) {
          $this->serid = $seridIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getPreid() {
          return $this->preid;
    }
    function setPreid($preidIn) {
          $this->preid = $preidIn;
    }

    function getCodservicio() {
          return $this->codservicio;
    }
    function setCodservicio($codservicioIn) {
          $this->codservicio = $codservicioIn;
    }

    function getIdalterna() {
          return $this->idalterna;
    }
    function setIdalterna($idalternaIn) {
          $this->idalterna = $idalternaIn;
    }

    function getNomservicio() {
          return $this->nomservicio;
    }
    function setNomservicio($nomservicioIn) {
          $this->nomservicio = $nomservicioIn;
    }

    function getNomgenerico() {
          return $this->nomgenerico;
    }
    function setNomgenerico($nomgenericoIn) {
          $this->nomgenerico = $nomgenericoIn;
    }

    function getPrescripcion() {
          return $this->prescripcion;
    }
    function setPrescripcion($prescripcionIn) {
          $this->prescripcion = $prescripcionIn;
    }

    function getCantidad() {
          return $this->cantidad;
    }
    function setCantidad($cantidadIn) {
          $this->cantidad = $cantidadIn;
    }

    function getSexo() {
          return $this->sexo;
    }
    function setSexo($sexoIn) {
          $this->sexo = $sexoIn;
    }

    function getEdadinicial() {
          return $this->edadinicial;
    }
    function setEdadinicial($edadinicialIn) {
          $this->edadinicial = $edadinicialIn;
    }

    function getEdadfinal() {
          return $this->edadfinal;
    }
    function setEdadfinal($edadfinalIn) {
          $this->edadfinal = $edadfinalIn;
    }

    function getCobrable() {
          return $this->cobrable;
    }
    function setCobrable($cobrableIn) {
          $this->cobrable = $cobrableIn;
    }

    function getPyp() {
          return $this->pyp;
    }
    function setPyp($pypIn) {
          $this->pyp = $pypIn;
    }

    function getProcedimiento() {
          return $this->procedimiento;
    }
    function setProcedimiento($procedimientoIn) {
          $this->procedimiento = $procedimientoIn;
    }

    function getManualt() {
          return $this->manualt;
    }
    function setManualt($manualtIn) {
          $this->manualt = $manualtIn;
    }

    function getTratamiento() {
          return $this->tratamiento;
    }
    function setTratamiento($tratamientoIn) {
          $this->tratamiento = $tratamientoIn;
    }

    function getTipomedicamento() {
          return $this->tipomedicamento;
    }
    function setTipomedicamento($tipomedicamentoIn) {
          $this->tipomedicamento = $tipomedicamentoIn;
    }

    function getFormafarma() {
          return $this->formafarma;
    }
    function setFormafarma($formafarmaIn) {
          $this->formafarma = $formafarmaIn;
    }

    function getConcentracion() {
          return $this->concentracion;
    }
    function setConcentracion($concentracionIn) {
          $this->concentracion = $concentracionIn;
    }

    function getUmedida() {
          return $this->umedida;
    }
    function setUmedida($umedidaIn) {
          $this->umedida = $umedidaIn;
    }

    function getObs() {
          return $this->obs;
    }
    function setObs($obsIn) {
          $this->obs = $obsIn;
    }

    function getRencrid() {
          return $this->rencrid;
    }
    function setRencrid($rencridIn) {
          $this->rencrid = $rencridIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function setAll($seridIn,
          $saas_ciaidIn,
          $preidIn,
          $codservicioIn,
          $idalternaIn,
          $nomservicioIn,
          $nomgenericoIn,
          $prescripcionIn,
          $cantidadIn,
          $sexoIn,
          $edadinicialIn,
          $edadfinalIn,
          $cobrableIn,
          $pypIn,
          $procedimientoIn,
          $manualtIn,
          $tratamientoIn,
          $tipomedicamentoIn,
          $formafarmaIn,
          $concentracionIn,
          $umedidaIn,
          $obsIn,
          $rencridIn,
          $estadoIn) {
          $this->serid = $seridIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->preid = $preidIn;
          $this->codservicio = $codservicioIn;
          $this->idalterna = $idalternaIn;
          $this->nomservicio = $nomservicioIn;
          $this->nomgenerico = $nomgenericoIn;
          $this->prescripcion = $prescripcionIn;
          $this->cantidad = $cantidadIn;
          $this->sexo = $sexoIn;
          $this->edadinicial = $edadinicialIn;
          $this->edadfinal = $edadfinalIn;
          $this->cobrable = $cobrableIn;
          $this->pyp = $pypIn;
          $this->procedimiento = $procedimientoIn;
          $this->manualt = $manualtIn;
          $this->tratamiento = $tratamientoIn;
          $this->tipomedicamento = $tipomedicamentoIn;
          $this->formafarma = $formafarmaIn;
          $this->concentracion = $concentracionIn;
          $this->umedida = $umedidaIn;
          $this->obs = $obsIn;
          $this->rencrid = $rencridIn;
          $this->estado = $estadoIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getSerid() != $this->serid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getPreid() != $this->preid) {
                    return(false);
          }
          if ($valueObject->getCodservicio() != $this->codservicio) {
                    return(false);
          }
          if ($valueObject->getIdalterna() != $this->idalterna) {
                    return(false);
          }
          if ($valueObject->getNomservicio() != $this->nomservicio) {
                    return(false);
          }
          if ($valueObject->getNomgenerico() != $this->nomgenerico) {
                    return(false);
          }
          if ($valueObject->getPrescripcion() != $this->prescripcion) {
                    return(false);
          }
          if ($valueObject->getCantidad() != $this->cantidad) {
                    return(false);
          }
          if ($valueObject->getSexo() != $this->sexo) {
                    return(false);
          }
          if ($valueObject->getEdadinicial() != $this->edadinicial) {
                    return(false);
          }
          if ($valueObject->getEdadfinal() != $this->edadfinal) {
                    return(false);
          }
          if ($valueObject->getCobrable() != $this->cobrable) {
                    return(false);
          }
          if ($valueObject->getPyp() != $this->pyp) {
                    return(false);
          }
          if ($valueObject->getProcedimiento() != $this->procedimiento) {
                    return(false);
          }
          if ($valueObject->getManualt() != $this->manualt) {
                    return(false);
          }
          if ($valueObject->getTratamiento() != $this->tratamiento) {
                    return(false);
          }
          if ($valueObject->getTipomedicamento() != $this->tipomedicamento) {
                    return(false);
          }
          if ($valueObject->getFormafarma() != $this->formafarma) {
                    return(false);
          }
          if ($valueObject->getConcentracion() != $this->concentracion) {
                    return(false);
          }
          if ($valueObject->getUmedida() != $this->umedida) {
                    return(false);
          }
          if ($valueObject->getObs() != $this->obs) {
                    return(false);
          }
          if ($valueObject->getRencrid() != $this->rencrid) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Ser, mapping to table ser\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."serid = ".$this->serid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."preid = ".$this->preid."\n"; 
        $out = $out."codservicio = ".$this->codservicio."\n"; 
        $out = $out."idalterna = ".$this->idalterna."\n"; 
        $out = $out."nomservicio = ".$this->nomservicio."\n"; 
        $out = $out."nomgenerico = ".$this->nomgenerico."\n"; 
        $out = $out."prescripcion = ".$this->prescripcion."\n"; 
        $out = $out."cantidad = ".$this->cantidad."\n"; 
        $out = $out."sexo = ".$this->sexo."\n"; 
        $out = $out."edadinicial = ".$this->edadinicial."\n"; 
        $out = $out."edadfinal = ".$this->edadfinal."\n"; 
        $out = $out."cobrable = ".$this->cobrable."\n"; 
        $out = $out."pyp = ".$this->pyp."\n"; 
        $out = $out."procedimiento = ".$this->procedimiento."\n"; 
        $out = $out."manualt = ".$this->manualt."\n"; 
        $out = $out."tratamiento = ".$this->tratamiento."\n"; 
        $out = $out."tipomedicamento = ".$this->tipomedicamento."\n"; 
        $out = $out."formafarma = ".$this->formafarma."\n"; 
        $out = $out."concentracion = ".$this->concentracion."\n"; 
        $out = $out."umedida = ".$this->umedida."\n"; 
        $out = $out."obs = ".$this->obs."\n"; 
        $out = $out."rencrid = ".$this->rencrid."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Ser();

        $cloned->setSerid($this->serid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setPreid($this->preid); 
        $cloned->setCodservicio($this->codservicio); 
        $cloned->setIdalterna($this->idalterna); 
        $cloned->setNomservicio($this->nomservicio); 
        $cloned->setNomgenerico($this->nomgenerico); 
        $cloned->setPrescripcion($this->prescripcion); 
        $cloned->setCantidad($this->cantidad); 
        $cloned->setSexo($this->sexo); 
        $cloned->setEdadinicial($this->edadinicial); 
        $cloned->setEdadfinal($this->edadfinal); 
        $cloned->setCobrable($this->cobrable); 
        $cloned->setPyp($this->pyp); 
        $cloned->setProcedimiento($this->procedimiento); 
        $cloned->setManualt($this->manualt); 
        $cloned->setTratamiento($this->tratamiento); 
        $cloned->setTipomedicamento($this->tipomedicamento); 
        $cloned->setFormafarma($this->formafarma); 
        $cloned->setConcentracion($this->concentracion); 
        $cloned->setUmedida($this->umedida); 
        $cloned->setObs($this->obs); 
        $cloned->setRencrid($this->rencrid); 
        $cloned->setEstado($this->estado); 

        return $cloned;
    }

}

?>