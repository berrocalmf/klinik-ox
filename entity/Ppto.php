<?php
 /**
  * Ppto Value Object.
  * This class is value object representing database table ppto
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 2018/11/18 (domingo)
    Revisado: Ok. 2018/12/20 09:19
  */
require_once("../entity/Ate.php");
require_once("../model/AteDao.php");
require_once("../entity/Cit.php");
require_once("../model/CitDao.php");
require_once("../entity/Afi.php");
require_once("../model/AfiDao.php");
require_once("../entity/Pptop.php");
require_once("../model/PptopDao.php");

class Ppto {
    private $pptoid;
    private $cnspresupuesto;
    private $fechappto;
    private $afiid;           // <-- FK
    private $ateid;           // <-- FK
    private $obs;
    private $vlrtotalppto;
    private $vlrpptocopago;
    private $vlrrealppto;
    private $vlrrealcopago;
    private $vlrsaldoppto;
    private $vlrsaldocopago;
    private $citid;           // <-- FK
    private $customerid;
    private $usrid;           // <-- FK
    private $estado;
    private $motivoAnula;

    public function __construct() {}

    function getPptoid() {
          return $this->pptoid;
    }
    function setPptoid($pptoidIn) {
          $this->pptoid = $pptoidIn;
    }

    function getCnspresupuesto() {
          return $this->cnspresupuesto;
    }
    function setCnspresupuesto($cnspresupuestoIn) {
          $this->cnspresupuesto = $cnspresupuestoIn;
    }

    function getFechappto() {
          return $this->fechappto;
    }
    function setFechappto($fechapptoIn) {
          $this->fechappto = $fechapptoIn;
    }

    function getAfiid() {
          return $this->afiid;
    }
    function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    function getAteid() {
          return $this->ateid;
    }
    function setAteid($ateidIn) {
          $this->ateid = $ateidIn;
    }

    function getObs() {
          return $this->obs;
    }
    function setObs($obsIn) {
          $this->obs = $obsIn;
    }

    function getVlrtotalppto() {
          return $this->vlrtotalppto;
    }
    function setVlrtotalppto($vlrtotalpptoIn) {
          $this->vlrtotalppto = $vlrtotalpptoIn;
    }

    function getVlrpptocopago() {
          return $this->vlrpptocopago;
    }
    function setVlrpptocopago($vlrpptocopagoIn) {
          $this->vlrpptocopago = $vlrpptocopagoIn;
    }

    function getVlrrealppto() {
          return $this->vlrrealppto;
    }
    function setVlrrealppto($vlrrealpptoIn) {
          $this->vlrrealppto = $vlrrealpptoIn;
    }

    function getVlrrealcopago() {
          return $this->vlrrealcopago;
    }
    function setVlrrealcopago($vlrrealcopagoIn) {
          $this->vlrrealcopago = $vlrrealcopagoIn;
    }

    function getVlrsaldoppto() {
          return $this->vlrsaldoppto;
    }
    function setVlrsaldoppto($vlrsaldopptoIn) {
          $this->vlrsaldoppto = $vlrsaldopptoIn;
    }

    function getVlrsaldocopago() {
          return $this->vlrsaldocopago;
    }
    function setVlrsaldocopago($vlrsaldocopagoIn) {
          $this->vlrsaldocopago = $vlrsaldocopagoIn;
    }

    function getCitid() {
          return $this->citid;
    }
    function setCitid($citidIn) {
          $this->citid = $citidIn;
    }

    function getCustomerid() {
          return $this->customerid;
    }
    function setCustomerid($customeridIn) {
          $this->customerid = $customeridIn;
    }

    function getUsrid() {
          return $this->usrid;
    }
    function setUsrid($usridIn) {
          $this->usrid = $usridIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function getMotivoAnula() {
          return $this->motivoAnula;
    }
    function setMotivoAnula($motivoAnulaIn) {
          $this->motivoAnula = $motivoAnulaIn;
    }

    function setAll($pptoidIn,
          $cnspresupuestoIn,
          $fechapptoIn,
          $afiidIn,
          $ateidIn,
          $obsIn,
          $vlrtotalpptoIn,
          $vlrpptocopagoIn,
          $vlrrealpptoIn,
          $vlrrealcopagoIn,
          $vlrsaldopptoIn,
          $vlrsaldocopagoIn,
          $citidIn,
          $customeridIn,
          $usridIn,
          $estadoIn,
          $motivoAnulaIn) {
          $this->pptoid = $pptoidIn;
          $this->cnspresupuesto = $cnspresupuestoIn;
          $this->fechappto = $fechapptoIn;
          $this->afiid = $afiidIn;
          $this->ateid = $ateidIn;
          $this->obs = $obsIn;
          $this->vlrtotalppto = $vlrtotalpptoIn;
          $this->vlrpptocopago = $vlrpptocopagoIn;
          $this->vlrrealppto = $vlrrealpptoIn;
          $this->vlrrealcopago = $vlrrealcopagoIn;
          $this->vlrsaldoppto = $vlrsaldopptoIn;
          $this->vlrsaldocopago = $vlrsaldocopagoIn;
          $this->citid = $citidIn;
          $this->customerid = $customeridIn;
          $this->usrid = $usridIn;
          $this->estado = $estadoIn;
          $this->motivoAnula = $motivoAnulaIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getPptoid() != $this->pptoid) {
                    return(false);
          }
          if ($valueObject->getCnspresupuesto() != $this->cnspresupuesto) {
                    return(false);
          }
          if ($valueObject->getFechappto() != $this->fechappto) {
                    return(false);
          }
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getAteid() != $this->ateid) {
                    return(false);
          }
          if ($valueObject->getObs() != $this->obs) {
                    return(false);
          }
          if ($valueObject->getVlrtotalppto() != $this->vlrtotalppto) {
                    return(false);
          }
          if ($valueObject->getVlrpptocopago() != $this->vlrpptocopago) {
                    return(false);
          }
          if ($valueObject->getVlrrealppto() != $this->vlrrealppto) {
                    return(false);
          }
          if ($valueObject->getVlrrealcopago() != $this->vlrrealcopago) {
                    return(false);
          }
          if ($valueObject->getVlrsaldoppto() != $this->vlrsaldoppto) {
                    return(false);
          }
          if ($valueObject->getVlrsaldocopago() != $this->vlrsaldocopago) {
                    return(false);
          }
          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          if ($valueObject->getCustomerid() != $this->customerid) {
                    return(false);
          }
          if ($valueObject->getUsrid() != $this->usrid) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getMotivoAnula() != $this->motivoAnula) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Ppto, mapping to table ppto\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."pptoid = ".$this->pptoid."\n"; 
        $out = $out."cnspresupuesto = ".$this->cnspresupuesto."\n"; 
        $out = $out."fechappto = ".$this->fechappto."\n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."ateid = ".$this->ateid."\n"; 
        $out = $out."obs = ".$this->obs."\n"; 
        $out = $out."vlrtotalppto = ".$this->vlrtotalppto."\n"; 
        $out = $out."vlrpptocopago = ".$this->vlrpptocopago."\n"; 
        $out = $out."vlrrealppto = ".$this->vlrrealppto."\n"; 
        $out = $out."vlrrealcopago = ".$this->vlrrealcopago."\n"; 
        $out = $out."vlrsaldoppto = ".$this->vlrsaldoppto."\n"; 
        $out = $out."vlrsaldocopago = ".$this->vlrsaldocopago."\n"; 
        $out = $out."citid = ".$this->citid."\n"; 
        $out = $out."customerid = ".$this->customerid."\n"; 
        $out = $out."usrid = ".$this->usrid."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."motivoAnula = ".$this->motivoAnula."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Ppto();

        $cloned->setPptoid($this->pptoid); 
        $cloned->setCnspresupuesto($this->cnspresupuesto); 
        $cloned->setFechappto($this->fechappto); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setAteid($this->ateid); 
        $cloned->setObs($this->obs); 
        $cloned->setVlrtotalppto($this->vlrtotalppto); 
        $cloned->setVlrpptocopago($this->vlrpptocopago); 
        $cloned->setVlrrealppto($this->vlrrealppto); 
        $cloned->setVlrrealcopago($this->vlrrealcopago); 
        $cloned->setVlrsaldoppto($this->vlrsaldoppto); 
        $cloned->setVlrsaldocopago($this->vlrsaldocopago); 
        $cloned->setCitid($this->citid); 
        $cloned->setCustomerid($this->customerid); 
        $cloned->setUsrid($this->usrid); 
        $cloned->setEstado($this->estado); 
        $cloned->setMotivoAnula($this->motivoAnula); 

        return $cloned;
    }

    // Relaciones de asociación uno a muchos

    


    // Retorna un objeto de tipo AFI (Relación de asociación) 
    // Ok. Ing. FMBM 02.NOV.2018
    public function getObjectAfi(&$conn) {
          $arrayObjetos =[];
          $afi = new Afi();
          $cAfi = new AfiDao(); 

          $afi->setAfiid($this->afiid);

          if($cAfi->load($conn, $afi)) {
              $arrayObjetos[]=$afi;
              return $arrayObjetos;
          } else {
            return null;
          }
    }
    
     // Retorna un objeto de tipo ATE (Relación de asociación) 
    // Ok. Ing. FMBM 02.NOV.2018
    public function getObjectAte(&$conn) {
          $arrayObjetos =[];
          $ate = new Ate();
          $cAte = new AteDao(); 

          $ate->setAteid($this->ateid);

          if($cAte->load($conn, $ate)) {
              $arrayObjetos[]=$ate;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo Cit (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    public function getObjectCit(&$conn) {
          $arrayObjetos =[];
          $cit = new Cit();
          $cCit = new CitDao(); 

          $cit->setCitid($this->citid);

          if($cCit->load($conn, $cit)) {
              $arrayObjetos[]=$cit;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Relaciones uno a muchos:

    // Retorna un Array con objetos de tipo pptop (Relación de asociación uno a muchos) 
    // Ok. Ing. FMBM 02.NOV.2018
    function getPagosPpto(&$conn) {
          $arrayObjetos=[];
          $cPptop = new PptopDao(); 
          $sql = "SELECT * FROM pptop WHERE (pptoid = ".$this->pptoid.") "; 
          $arrayObjetos =  $cPptop->listQuery($conn,$sql);
          return $arrayObjetos;    
    }

}

?>