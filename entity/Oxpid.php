<?php

class Oxpid {

    /* 
      Oxpid File (Configuracion de piezas dentales) 
      Ing. FMBM / 13.SEP.2018
      Revisado: Ok. 2018/12/20 09:19
    */

    private $pidid;
    private $codpieza;
    private $descripcion;
    private $tipoPid;
    private $pidcaracentro;
    private $pidcarasuperior;
    private $pidcarainferior;
    private $pidcaraizquierda;
    private $pidcaraderecha;

    public function __construct() {}

    // function Oxpid () {}
    // function Oxpid ($pididIn) {$this->pidid = $pididIn;}

    public function getPidid() {
          return $this->pidid;
    }
    public function setPidid($pididIn) {
          $this->pidid = $pididIn;
    }

    public function getCodpieza() {
          return $this->codpieza;
    }
    public function setCodpieza($codpiezaIn) {
          $this->codpieza = $codpiezaIn;
    }

    public function getDescripcion() {
          return $this->descripcion;
    }
    public function setDescripcion($descripcionIn) {
          $this->descripcion = $descripcionIn;
    }

    public function getTipoPid() {
          return $this->tipoPid;
    }
    public function setTipoPid($tipoPidIn) {
          $this->tipoPid = $tipoPidIn;
    }

    public function getPidcaracentro() {
          return $this->pidcaracentro;
    }
    public function setPidcaracentro($pidcaracentroIn) {
          $this->pidcaracentro = $pidcaracentroIn;
    }

    public function getPidcarasuperior() {
          return $this->pidcarasuperior;
    }
    public function setPidcarasuperior($pidcarasuperiorIn) {
          $this->pidcarasuperior = $pidcarasuperiorIn;
    }

    public function getPidcarainferior() {
          return $this->pidcarainferior;
    }
    public function setPidcarainferior($pidcarainferiorIn) {
          $this->pidcarainferior = $pidcarainferiorIn;
    }

    public function getPidcaraizquierda() {
          return $this->pidcaraizquierda;
    }
    public function setPidcaraizquierda($pidcaraizquierdaIn) {
          $this->pidcaraizquierda = $pidcaraizquierdaIn;
    }

    public function getPidcaraderecha() {
          return $this->pidcaraderecha;
    }
    public function setPidcaraderecha($pidcaraderechaIn) {
          $this->pidcaraderecha = $pidcaraderechaIn;
    }

    public function setAll($pididIn, $codpiezaIn, $descripcionIn, $tipoPidIn, $pidcaracentroIn, $pidcarasuperiorIn, $pidcarainferiorIn,
          $pidcaraizquierdaIn, $pidcaraderechaIn) {
          $this->pidid            = $pididIn;
          $this->codpieza         = $codpiezaIn;
          $this->descripcion      = $descripcionIn;
          $this->tipoPid          = $tipoPidIn;
          $this->pidcaracentro    = $pidcaracentroIn;
          $this->pidcarasuperior  = $pidcarasuperiorIn;
          $this->pidcarainferior  = $pidcarainferiorIn;
          $this->pidcaraizquierda = $pidcaraizquierdaIn;
          $this->pidcaraderecha   = $pidcaraderechaIn;
    }

    public function hasEqualMapping($valueObject) {

          if ($valueObject->getPidid() != $this->pidid) {
                    return(false);
          }
          if ($valueObject->getCodpieza() != $this->codpieza) {
                    return(false);
          }
          if ($valueObject->getDescripcion() != $this->descripcion) {
                    return(false);
          }
          if ($valueObject->getTipoPid() != $this->tipoPid) {
                    return(false);
          }
          if ($valueObject->getPidcaracentro() != $this->pidcaracentro) {
                    return(false);
          }
          if ($valueObject->getPidcarasuperior() != $this->pidcarasuperior) {
                    return(false);
          }
          if ($valueObject->getPidcarainferior() != $this->pidcarainferior) {
                    return(false);
          }
          if ($valueObject->getPidcaraizquierda() != $this->pidcaraizquierda) {
                    return(false);
          }
          if ($valueObject->getPidcaraderecha() != $this->pidcaraderecha) {
                    return(false);
          }

          return true;
    }

    public function toString() {
        $out = "";
        $out = $out."\nclass Oxpid, mapping to table oxpid\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."pidid = ".$this->pidid."\n"; 
        $out = $out."codpieza = ".$this->codpieza."\n"; 
        $out = $out."descripcion = ".$this->descripcion."\n"; 
        $out = $out."tipoPid = ".$this->tipoPid."\n"; 
        $out = $out."pidcaracentro = ".$this->pidcaracentro."\n"; 
        $out = $out."pidcarasuperior = ".$this->pidcarasuperior."\n"; 
        $out = $out."pidcarainferior = ".$this->pidcarainferior."\n"; 
        $out = $out."pidcaraizquierda = ".$this->pidcaraizquierda."\n"; 
        $out = $out."pidcaraderecha = ".$this->pidcaraderecha."\n"; 
        return $out;
    }

   public function clone() {
        $cloned = new Oxpid();

        $cloned->setPidid($this->pidid); 
        $cloned->setCodpieza($this->codpieza); 
        $cloned->setDescripcion($this->descripcion); 
        $cloned->setTipoPid($this->tipoPid); 
        $cloned->setPidcaracentro($this->pidcaracentro); 
        $cloned->setPidcarasuperior($this->pidcarasuperior); 
        $cloned->setPidcarainferior($this->pidcarainferior); 
        $cloned->setPidcaraizquierda($this->pidcaraizquierda); 
        $cloned->setPidcaraderecha($this->pidcaraderecha); 

        return $cloned;
    }
}

?>