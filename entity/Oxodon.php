<?php

/**
  * Oxodon Value Object.
  * This class is value object representing database table oxodon
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 05.NOV.2018 
  * Revisado: Ok. 2018/12/20 09:19
  */

require_once("../entity/Afi.php");
require_once("../model/AfiDao.php");
require_once("../entity/Ate.php");
require_once("../model/AteDao.php");
require_once("../entity/Cit.php");
require_once("../model/CitDao.php");
require_once("../entity/Med.php");
require_once("../model/MedDao.php");
require_once("../model/OxodondDao.php");

class Oxodon {

    // Ing. FMBM 02.NOV.2018 [se agrega citid, odxinicial]
    private $odonid;
    private $noodontogram;
    private $nroficha;
    private $fecha;
    private $medid;         // <-- FK
    private $ateid;         // <-- FK
    private $afiid;         // <-- FK 
    private $estado;
    private $denttemporal;
    private $indiceCPOD;
    private $indiceCEOD;
    private $citid;         // <-- FK
    private $customerid;
    private $odxinicial;
    private $motivoAnula;   // Motivo que genera la anulación del Registro de Odontograma

    // Agergar atributo motivoAnula

    // Relaciones foráneas: med, ate, afi, cit.

    public function __construct(){}

    public function getOdonid() {
          return $this->odonid;
    }

    public function setOdonid($odonidIn) {
          $this->odonid = $odonidIn;
    }

    public function getNoodontogram() {
          return $this->noodontogram;
    }

    public function setNoodontogram($noodontogramIn) {
          $this->noodontogram = $noodontogramIn;
    }

    public function getNroficha() {
          return $this->nroficha;
    }

    public function setNroficha($nrofichaIn) {
          $this->nroficha = $nrofichaIn;
    }

    public function getFecha() {
          return $this->fecha;
    }

    public function setFecha($fechaIn) {
          $this->fecha = $fechaIn;
    }

    public function getMedid() {
          return $this->medid;
    }

    public function setMedid($medidIn) {
          $this->medid = $medidIn;
    }

    public function getAteid() {
          return $this->ateid;
    }

    public function setAteid($ateidIn) {
          $this->ateid = $ateidIn;
    }

    public function getAfiid() {
          return $this->afiid;
    }

    public function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    public function getEstado() {
          return $this->estado;
    }

    public function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    public function getDenttemporal() {
          return $this->denttemporal;
    }

    public function setDenttemporal($denttemporalIn) {
          $this->denttemporal = $denttemporalIn;
    }

    public function getIndiceCPOD() {
          return $this->indiceCPOD;
    }

    public function setIndiceCPOD($indiceCPODIn) {
          $this->indiceCPOD = $indiceCPODIn;
    }

    public function getIndiceCEOD() {
          return $this->indiceCEOD;
    }

    public function setIndiceCEOD($indiceCEODIn) {
          $this->indiceCEOD = $indiceCEODIn;
    }

    public function getCustomerid() {
          return $this->customerid;
    }

    public function setCustomerid($customeridIn) {
          $this->customerid = $customeridIn;
    }
    
    public function getCitid() {
          return $this->citid;
    }

    public function setCitid($citidIn) {
          $this->citid = $citidIn;
    }
    
    public function getOdxinicial() {
          return $this->odxinicial;
    }

    public function setOdxinicial($odxinicialIn) {
          $this->odxinicial = $odxinicialIn;
    }

     public function getMotivoanula() {
          return $this->motivoAnula;
    }

    public function setMotivoanula($motivoAnulaIn) {
          $this->motivoAnula = $motivoAnulaIn;
    }

    public function setAll($odonidIn,$noodontogramIn,$nrofichaIn,$fechaIn,$medidIn,$ateidIn,$afiidIn,$estadoIn,$denttemporalIn,$indiceCPODIn,$indiceCEODIn,$citidIn,$customeridIn,$odxinicialIn,$motivoAnulaIn) {
          $this->odonid       = $odonidIn;
          $this->noodontogram = $noodontogramIn;
          $this->nroficha     = $nrofichaIn;
          $this->fecha        = $fechaIn;
          $this->medid        = $medidIn;
          $this->ateid        = $ateidIn;
          $this->afiid        = $afiidIn;
          $this->estado       = $estadoIn;
          $this->denttemporal = $denttemporalIn;
          $this->indiceCPOD   = $indiceCPODIn;
          $this->indiceCEOD   = $indiceCEODIn;
          $this->citid        = $citidIn;   
          $this->customerid   = $customeridIn;
          $this->odxinicial   = $odxinicialIn;
          $this->motivoAnula  = $motivoAnulaIn;
    }

    public function hasEqualMapping($valueObject) {

          if ($valueObject->getOdonid() != $this->odonid) {
                    return(false);
          }
          
          if ($valueObject->getNoodontogram() != $this->noodontogram) {
                    return(false);
          }
          
          if ($valueObject->getNroficha() != $this->nroficha) {
                    return(false);
          }
          
          if ($valueObject->getFecha() != $this->fecha) {
                    return(false);
          }
          
          if ($valueObject->getMedid() != $this->medid) {
                    return(false);
          }
          
          if ($valueObject->getAteid() != $this->ateid) {
                    return(false);
          }
          
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
                    
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          
          if ($valueObject->getDenttemporal() != $this->denttemporal) {
                    return(false);
          }
          
          if ($valueObject->getIndiceCPOD() != $this->indiceCPOD) {
                    return(false);
          }
          
          if ($valueObject->getIndiceCEOD() != $this->indiceCEOD) {
                    return(false);
          }
       
          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          
          if ($valueObject->getCustomerid() != $this->customerid) {
                    return(false);
          }
       
          if ($valueObject->getOdxinicial() != $this->odxinicial) {
                    return(false);
          }

          if ($valueObject->getMotivoanula() != $this->motivoAnula) {
                    return(false);
          }

          return true;
    }

    public function toString() {
        $out = $out."\nclass Oxodon, mapping to table oxodon<br>";
        $out = $out."Persistent attributes: <br>"; 
        $out = $out."Id = ".$this->odonid."<br>"; 
        $out = $out."No. Odontograma = ".$this->noodontogram."<br>"; 
        $out = $out."Nro. Ficha = ".$this->nroficha."<br>"; 
        $out = $out."Fecha = ".$this->fecha."<br>"; 
        $out = $out."Id. M&eaacute;dico = ".$this->medid."<br>"; 
        $out = $out."Id. Atenci&oacute;n = ".$this->ateid."<br>"; 
        $out = $out."Id. Afiliado = ".$this->afiid."<br>"; 
        $out = $out."Estado = ".$this->estado."<br>"; 
        $out = $out."Dent. temporal = ".$this->denttemporal."<br>"; 
        $out = $out."Indice CPOD = ".$this->indiceCPOD."<br>"; 
        $out = $out."Indice CEOD = ".$this->indiceCEOD."<br>"; 
        $out = $out."Id. Cita = ".$this->citid."<br>"; 
        $out = $out."customerid = ".$this->customerid."<br>"; 
        $out = $out."Es inicial = ".$this->odxinicial."<br>";
        $out = $out."motivoAnula = ".$this->motivoAnula."<br>";
        return $out;
    }

    public function clone() {
        $cloned = new Oxodon();

        $cloned->setOdonid($this->odonid); 
        $cloned->setNoodontogram($this->noodontogram); 
        $cloned->setNroficha($this->nroficha); 
        $cloned->setFecha($this->fecha); 
        $cloned->setMedid($this->medid); 
        $cloned->setAteid($this->ateid); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setEstado($this->estado); 
        $cloned->setDenttemporal($this->denttemporal); 
        $cloned->setIndiceCPOD($this->indiceCPOD); 
        $cloned->setIndiceCEOD($this->indiceCEOD); 
        $cloned->setCitid($this->citid); 
        $cloned->setCustomerid($this->customerid); 
        $cloned->setOdxinicial($this->odxinicial); 
        $cloned->setMotivoanula($this->motivoAnula); 

        return $cloned;
    }
    
    // Retorna un objeto de tipo ATE (Relación de asociación) 
    // Ok. Ing. FMBM 02.NOV.2018
    public function getObjectAte(&$conn) {
          $arrayObjetos =[];
          $ate = new Ate();
          $cAte = new AteDao(); 

          $ate->setAteid($this->ateid);

          if($cAte->load($conn, $ate)) {
              $arrayObjetos[]=$ate;
              return $arrayObjetos;
          } else {
            return null;
          }
    }
    
    // Retorna un objeto de tipo AFI (Relación de asociación) 
    // Ok. Ing. FMBM 02.NOV.2018
    public function getObjectAfi(&$conn) {
          $arrayObjetos =[];
          $afi = new Afi();
          $cAfi = new AfiDao(); 

          $afi->setAfiid($this->afiid);

          if($cAfi->load($conn, $afi)) {
              $arrayObjetos[]=$afi;
              return $arrayObjetos;
          } else {
            return null;
          }
    }
    
    // Retorna un objeto de tipo Cit (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    public function getObjectCit(&$conn) {
          $arrayObjetos =[];
          $cit = new Cit();
          $cCit = new CitDao(); 

          $cit->setCitid($this->citid);

          if($cCit->load($conn, $cit)) {
              $arrayObjetos[]=$cit;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo Med (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    public function getObjectMed(&$conn) {
          $arrayObjetos =[];
          $med = new Med();
          $cMed = new MedDao(); 

          $med->setMedid($this->medid);

          if($cMed->load($conn, $med)) {
              $arrayObjetos[]=$med;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un Array con objetos de tipo oxodond (Relación de asociación uno a muchos) 
    // Ok. Ing. FMBM 02.NOV.2018
    function getDetallesOdon(&$conn) {
          $arrayObjetos=[];
          $cOxodond = new OxodondDao(); 
          $sql = "SELECT * FROM oxodond WHERE (odonid = ".$this->odonid.") "; 
        
          $arrayObjetos =  $cOxodond->listQuery($conn,$sql);
          return $arrayObjetos;    
    }

    // Retourne un tableau avec des objets de type omed (relation d'association un à plusieurs)
    // Ok. Ing. FMBM 02.NOV.2018
    // function getListaOmed(&$conn) {
    //       $arrayObjetos=[];
    //       $cOmed = new OmedDao(); 
    //       $sql = "SELECT * FROM omed WHERE (odonid = ".$this->odonid.") "; 
        
    //       $arrayObjetos =  $cOmed->listQuery($conn,$sql);
    //       return $arrayObjetos;    
    // }

    // Retourne un tableau avec des objets de type oxproc (relation d'association un à plusieurs)
    // Ok. Ing. FMBM 18.NOV.2018
    function getListaProc(&$conn) {
          $arrayObjetos=[];
          $cOxproc = new OxprocDao(); 
          $sql = "SELECT * FROM oxproc WHERE (odonid = ".$this->odonid.") "; 
        
          $arrayObjetos =  $cOxproc->listQuery($conn,$sql);
          return $arrayObjetos;    
    }

    // Retourne les enregistrements détaillés des marques dentaires de l'odontogramme
    // Ok. Ing. FMBM 2018/11/05
    public function getMarcasArray(&$conn) {
           
        $sql="";  
        $sql.="SELECT oxodond.odondid,oxodond.odonid,oxodond.marid,oxmar.codmarca,";
        $sql.="oxodond.pidid,oxpid.codpieza,oxodond.caras,oxodond.carasmarca,";
        $sql.="oxodond.estado,oxodond.oxpraid,oxodond.descripcion,oxodond.fregistro, ";
        $sql.="oxmar.textomarca ";
        $sql.=" FROM oxodond ";
        $sql.="left join oxmar on oxodond.marid=oxmar.marid ";
        $sql.="left join oxpid on oxodond.pidid=oxpid.pidid ";
        $sql.="where odonid=".$this->odonid;

        //$sql="select * from oxodond where odonid=".$odonid;
        $query  = $conn->prepare($sql);
        $query->execute();
        // $result = $query->fetchAll();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        // PDO::FETCH_ASSOC
        return $result;
     }

}

?>