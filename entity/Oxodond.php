<?php
/**
  * Oxodond Value Object.
  * This class is value object representing database table oxodond
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 05.NOV.2018 
  * Revisado: Ok. 2018/12/20 09:19
  */

require_once("../entity/Oxodon.php");
require_once("../model/OxodonDao.php");
require_once("../entity/Oxmar.php");
require_once("../model/OxmarDao.php");
require_once("../entity/Oxpid.php");
require_once("../model/OxpidDao.php");
require_once("../entity/Omed.php");
require_once("../model/OmedDao.php");

class Oxodond {
    private $odondid;
    private $odonid;    // <-- FK
    private $marid;     // <-- FK
    private $pidid;     // <-- FK
    private $caras;
    private $carasmarca;
    private $sectormarca;
    private $estado;
    private $omedid;    // <-- FK 
    private $descripcion;
    private $fregistro;

    // Agregar variable de clase sectormarca
    
    //public function Oxodond () {}
    
    public function __construct(){}

    public function getOdondid() {
          return $this->odondid;
    }
    public function setOdondid($odondidIn) {
          $this->odondid = $odondidIn;
    }

    public function getOdonid() {
          return $this->odonid;
    }
    public function setOdonid($odonidIn) {
          $this->odonid = $odonidIn;
    }

    public function getMarid() {
          return $this->marid;
    }
    public function setMarid($maridIn) {
          $this->marid = $maridIn;
    }

    public function getPidid() {
          return $this->pidid;
    }
    public function setPidid($pididIn) {
          $this->pidid = $pididIn;
    }

    public function getCaras() {
          return $this->caras;
    }
    public function setCaras($carasIn) {
          $this->caras = $carasIn;
    }

    public function getCarasmarca() {
          return $this->carasmarca;
    }
    public function setCarasmarca($carasmarcaIn) {
          $this->carasmarca = $carasmarcaIn;
    }

    public function getSectormarca() {
          return $this->sectormarca;
    }
    public function setSectormarca($sectormarcaIn) {
          $this->sectormarca = $sectormarcaIn;
    }

    public function getEstado() {
          return $this->estado;
    }
    public function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    public function getOmedid() {
          return $this->omedid;
    }
    public function setOmedid($omedidIn) {
          $this->omedid = $omedidIn;
    }

    public function getFregistro() {
          return $this->fregistro;
    }
    public function setFregistro($fregistroIn) {
          $this->fregistro = $fregistroIn;
    }

    public function getDescripcion() {
          return $this->descripcion;
    }
    public function setDescripcion($descripcionIn) {
          $this->descripcion = $descripcionIn;
    }

    public function setAll($odondidIn,$odonidIn,$maridIn,$pididIn,$carasIn,$carasmarcaIn,$sectormarcaIn,$estadoIn,$omedidIn,$fregistroIn, $descripcionIn) {
          $this->odondid     = $odondidIn;
          $this->odonid      = $odonidIn;
          $this->marid       = $maridIn;
          $this->pidid       = $pididIn;
          $this->caras       = $carasIn;
          $this->carasmarca  = $carasmarcaIn;
          $this->sectormarca = $sectormarcaIn; 
          $this->estado      = $estadoIn;
          $this->omedid     = $omedidIn;
          $this->fregistro   = $fregistroIn;
          $this->descripcion = $descripcionIn;
    }

    public function hasEqualMapping($valueObject) {

          if ($valueObject->getOdondid() != $this->odondid) {
                    return(false);
          }
          if ($valueObject->getOdonid() != $this->odonid) {
                    return(false);
          }
          if ($valueObject->getMarid() != $this->marid) {
                    return(false);
          }
          if ($valueObject->getPidid() != $this->pidid) {
                    return(false);
          }
          if ($valueObject->getCaras() != $this->caras) {
                    return(false);
          }
          if ($valueObject->getCarasmarca() != $this->carasmarca) {
                    return(false);
          }
          if ($valueObject->getSectormarca() != $this->sectormarca) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getOmedid() != $this->omedid) {
                    return(false);
          }
          if ($valueObject->getFregistro() != $this->fregistro) {
                    return(false);
          }
          if ($valueObject->getDescripcion() != $this->descripcion) {
                    return(false);
          }

          return true;
    }

    public function toString() {
        $out = "";
        $out = $out."class Oxodond, mapping to table oxodond\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."Id. = ".$this->odondid."\n"; 
        $out = $out."Id. Odontograma = ".$this->odonid."\n"; 
        $out = $out."Id. Marca = ".$this->marid."\n"; 
        $out = $out."Id. Diente = ".$this->pidid."\n"; 
        $out = $out."Caras = ".$this->caras."\n"; 
        $out = $out."Carasmarca = ".$this->carasmarca."\n"; 
        $out = $out."sectormarca = ".$this->sectormarca."\n"; 
        $out = $out."Estado = ".$this->estado."\n"; 
        $out = $out."Id. Omed = ".$this->omedid."\n"; 
        $out = $out."F. Registro = ".$this->fregistro."\n"; 
        $out = $out."Descripci&oacute;n = ".$this->descripcion."\n"; 
        return $out;
    }

    public function clone() {
        $cloned = new Oxodond();

        $cloned->setOdondid($this->odondid); 
        $cloned->setOdonid($this->odonid); 
        $cloned->setMarid($this->marid); 
        $cloned->setPidid($this->pidid); 
        $cloned->setCaras($this->caras); 
        $cloned->setCarasmarca($this->carasmarca);
        $cloned->setSectormarca($this->sectormarca); 
        $cloned->setEstado($this->estado); 
        $cloned->setOmedid($this->omedid); 
        $cloned->setFregistro($this->fregistro); 
        $cloned->setDescripcion($this->descripcion); 

        return $cloned;
    }

    // Relaciones de aosicación

    // Retorna un objeto de tipo Oxodon (Relación de asociación)
    // Ok. Ing. FMBM 03.NOV.2018 
    public function getObjectOdon(&$conn) {
          $arrayObjetos =[];
          $odontograma  = new Oxodon();
          $cOdontograma = new OxodonDao(); 

          $odontograma->setOdonid($this->odonid);

          if($cOdontograma->load($conn, $odontograma)) {
              $arrayObjetos[]=$odontograma;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo oxmar (Relación de asociación)
    // Ok. Ing. FMBM 03.NOV.2018 
    public function getObjectMarca(&$conn) {
          $arrayObjetos =[];
          $oxmar  = new Oxmar();
          $cOxmar = new OxmarDao(); 

          $oxmar->setMarid($this->marid);

          if($cOxmar->load($conn, $oxmar)) {
              $arrayObjetos[]=$oxmar;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo oxpid (Relación de asociación)
    // Ok. Ing. FMBM 03.NOV.2018 
    public function getObjectPieza(&$conn) {
          $arrayObjetos =[];
          $oxpid  = new Oxpid();
          $cOxpid = new OxpidDao(); 

          $oxpid->setPidid($this->pidid);

          if($cOxpid->load($conn, $oxpid)) {
              $arrayObjetos[]=$oxpid;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type omed (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    public function getObjectOmed(&$conn) {
          $arrayObjetos =[];
          $omed  = new Omed();
          $cOmed = new OmedDao(); 

          $omed->setOmedid($this->omedid);

          if($cOmed->load($conn, $omed)) {
              $arrayObjetos[]=$omed;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

}

?>