<?php
 /**
  * Omed Value Object.
  * This class is value object representing database table omed
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 05.NOV.2018 
  * Revisado: Ok. 2018/12/20 09:19
  */

// require_once("../entity/Oxodon.php");
// require_once("../model/OxodonDao.php");
// require_once("../entity/Med.php");
// require_once("../model/MedDao.php");
// require_once("../entity/Oxmar.php");
// require_once("../model/OxmarDao.php");
// require_once("../entity/Oxpid.php");
// require_once("../model/OxpidDao.php");
require_once("../entity/Ser.php");
require_once("../model/SerDao.php");
require_once("../entity/Ome.php");
require_once("../model/OmeDao.php");

class Omed {

    private $omedid;
    private $omeid;                 // <-- FK
    private $noitem;
    private $serid;                 // <-- FK
    private $cantidad;
    private $impreso;
    private $comentarios;
    private $fechares;
    private $resultados;    
    private $procid;                // <-- FK
    // private $pidid;                // <-- FK
    // private $proccarapieza;
    // private $procsector;
    // private $marid;               // <-- FK
    // private $estado;
    // private $fecharealizado;
    // private $procautorizado;
    // private $idmedicorealiza;     // <-- FK
    // private $odonid;              // <-- FK

    public function __construct(){}

    function getOmedid() {
          return $this->omedid;
    }
    function setOmedid($omedidIn) {
          $this->omedid = $omedidIn;
    }

    function getOmeid() {
          return $this->omeid;
    }
    function setOmeid($omeidIn) {
          $this->omeid = $omeidIn;
    }

    function getNoitem() {
          return $this->noitem;
    }
    function setNoitem($noitemIn) {
          $this->noitem = $noitemIn;
    }

    function getSerid() {
          return $this->serid;
    }
    function setSerid($seridIn) {
          $this->serid = $seridIn;
    }

    function getCantidad() {
          return $this->cantidad;
    }
    function setCantidad($cantidadIn) {
          $this->cantidad = $cantidadIn;
    }

    function getImpreso() {
          return $this->impreso;
    }
    function setImpreso($impresoIn) {
          $this->impreso = $impresoIn;
    }

    function getComentarios() {
          return $this->comentarios;
    }
    function setComentarios($comentariosIn) {
          $this->comentarios = $comentariosIn;
    }

    function getFechares() {
          return $this->fechares;
    }
    function setFechares($fecharesIn) {
          $this->fechares = $fecharesIn;
    }

    function getResultados() {
          return $this->resultados;
    }
    function setResultados($resultadosIn) {
          $this->resultados = $resultadosIn;
    }

    function getProcid() {
          return $this->procid;
    }
    function setProcid($procidIn) {
          $this->procid = $procidIn;
    }

    // function getProccarapieza() {
    //       return $this->proccarapieza;
    // }
    // function setProccarapieza($proccarapiezaIn) {
    //       $this->proccarapieza = $proccarapiezaIn;
    // }

    // function getProcsector() {
    //       return $this->procsector;
    // }
    // function setProcsector($procsectorIn) {
    //       $this->procsector = $procsectorIn;
    // }

    // function getMarid() {
    //       return $this->marid;
    // }
    // function setMarid($maridIn) {
    //       $this->marid = $maridIn;
    // }

    // function getEstado() {
    //       return $this->estado;
    // }
    // function setEstado($estadoIn) {
    //       $this->estado = $estadoIn;
    // }

    // function getFecharealizado() {
    //       return $this->fecharealizado;
    // }
    // function setFecharealizado($fecharealizadoIn) {
    //       $this->fecharealizado = $fecharealizadoIn;
    // }

    // function getProcautorizado() {
    //       return $this->procautorizado;
    // }
    // function setProcautorizado($procautorizadoIn) {
    //       $this->procautorizado = $procautorizadoIn;
    // }

    // function getIdmedicorealiza() {
    //       return $this->idmedicorealiza;
    // }
    // function setIdmedicorealiza($idmedicorealizaIn) {
    //       $this->idmedicorealiza = $idmedicorealizaIn;
    // }

    // function getOdonid() {
    //       return $this->odonid;
    // }
    // function setOdonid($odonidIn) {
    //       $this->odonid = $odonidIn;
    // }

    function setAll($omedidIn,
          $omeidIn,
          $noitemIn,
          $seridIn,
          $cantidadIn,
          $impresoIn,
          $comentariosIn,
          $fecharesIn,
          $resultadosIn,
          $procidIn) {
          $this->omedid = $omedidIn;
          $this->omeid = $omeidIn;
          $this->noitem = $noitemIn;
          $this->serid = $seridIn;
          $this->cantidad = $cantidadIn;
          $this->impreso = $impresoIn;
          $this->comentarios = $comentariosIn;
          $this->fechares = $fecharesIn;
          $this->resultados = $resultadosIn;
          $this->procid = $procidIn;
          // $this->pidid = $pididIn;
          // $this->proccarapieza = $proccarapiezaIn;
          // $this->procsector = $procsectorIn;
          // $this->marid = $maridIn;
          // $this->estado = $estadoIn;
          // $this->fecharealizado = $fecharealizadoIn;
          // $this->procautorizado = $procautorizadoIn;
          // $this->idmedicorealiza = $idmedicorealizaIn;
          // $this->odonid = $odonidIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getOmedid() != $this->omedid) {
                    return(false);
          }
          if ($valueObject->getOmeid() != $this->omeid) {
                    return(false);
          }
          if ($valueObject->getNoitem() != $this->noitem) {
                    return(false);
          }
          if ($valueObject->getSerid() != $this->serid) {
                    return(false);
          }
          if ($valueObject->getCantidad() != $this->cantidad) {
                    return(false);
          }
          if ($valueObject->getImpreso() != $this->impreso) {
                    return(false);
          }
          if ($valueObject->getComentarios() != $this->comentarios) {
                    return(false);
          }
          if ($valueObject->getFechares() != $this->fechares) {
                    return(false);
          }
          if ($valueObject->getResultados() != $this->resultados) {
                    return(false);
          }
          if ($valueObject->getProcid() != $this->procid) {
                    return(false);
          }
          // if ($valueObject->getPidid() != $this->pidid) {
          //           return(false);
          // }
          // if ($valueObject->getProccarapieza() != $this->proccarapieza) {
          //           return(false);
          // }
          // if ($valueObject->getProcsector() != $this->procsector) {
          //           return(false);
          // }
          // if ($valueObject->getMarid() != $this->marid) {
          //           return(false);
          // }
          // if ($valueObject->getEstado() != $this->estado) {
          //           return(false);
          // }
          // if ($valueObject->getFecharealizado() != $this->fecharealizado) {
          //           return(false);
          // }
          // if ($valueObject->getProcautorizado() != $this->procautorizado) {
          //           return(false);
          // }
          // if ($valueObject->getIdmedicorealiza() != $this->idmedicorealiza) {
          //           return(false);
          // }
          // if ($valueObject->getOdonid() != $this->odonid) {
          //           return(false);
          // }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Omed, mapping to table omed\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."Id. Reg = ".$this->omedid."\n"; 
        $out = $out."Id. Orden = ".$this->omeid."\n"; 
        $out = $out."No item = ".$this->noitem."\n"; 
        $out = $out."Id. Servicio = ".$this->serid."\n"; 
        $out = $out."Cantidad = ".$this->cantidad."\n"; 
        $out = $out."Impreso = ".$this->impreso."\n"; 
        $out = $out."Comentarios = ".$this->comentarios."\n"; 
        $out = $out."Fe. Result = ".$this->fechares."\n"; 
        $out = $out."Resultados = ".$this->resultados."\n"; 
        $out = $out."Procid = ".$this->procid."\n"; 
        // $out = $out."Id. P. Dental = ".$this->pidid."\n"; 
        // $out = $out."Cara = ".$this->proccarapieza."\n"; 
        // $out = $out."procsector = ".$this->procsector."\n"; 
        // $out = $out."marid = ".$this->marid."\n"; 
        // $out = $out."estado = ".$this->estado."\n"; 
        // $out = $out."fecharealizado = ".$this->fecharealizado."\n"; 
        // $out = $out."procautorizado = ".$this->procautorizado."\n"; 
        // $out = $out."idmedicorealiza = ".$this->idmedicorealiza."\n"; 
        // $out = $out."odonid = ".$this->odonid."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Omed();

        $cloned->setOmedid($this->omedid); 
        $cloned->setOmeid($this->omeid); 
        $cloned->setNoitem($this->noitem); 
        $cloned->setSerid($this->serid); 
        $cloned->setCantidad($this->cantidad); 
        $cloned->setImpreso($this->impreso); 
        $cloned->setComentarios($this->comentarios); 
        $cloned->setFechares($this->fechares); 
        $cloned->setResultados($this->resultados); 
        $cloned->setProcid($this->procid); 
        // $cloned->setPidid($this->pidid); 
        // $cloned->setProccarapieza($this->proccarapieza); 
        // $cloned->setProcsector($this->procsector); 
        // $cloned->setMarid($this->marid); 
        // $cloned->setEstado($this->estado); 
        // $cloned->setFecharealizado($this->fecharealizado); 
        // $cloned->setProcautorizado($this->procautorizado); 
        // $cloned->setIdmedicorealiza($this->idmedicorealiza); 
        // $cloned->setOdonid($this->odonid); 

        return $cloned;
    }

    // Retourne un objet de type ome (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    public function getObjectOme(&$conn) {
          $arrayObjetos =[];
          $ome  = new Ome();
          $cOme = new OmeDao(); 

          $ome->setOmeid($this->omeid);

          if($cOme->load($conn, $ome)) {
              $arrayObjetos[]=$ome;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type ser (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    public function getObjectSer(&$conn) {
          $arrayObjetos =[];
          $ser  = new Ser();
          $cSer = new SerDao(); 

          $ser->setSerid($this->serid);

          if($cSer->load($conn, $ser)) {
              $arrayObjetos[]=$ser;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type oxpid (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    // public function getObjectPieza(&$conn) {
    //       $arrayObjetos =[];
    //       $oxpid  = new Oxpid();
    //       $cOxpid = new OxpidDao(); 

    //       $oxpid->setPidid($this->pidid);

    //       if($cOxpid->load($conn, $oxpid)) {
    //           $arrayObjetos[]=$oxpid;
    //           return $arrayObjetos;
    //       } else {
    //         return null;
    //       }
    // }

    // Retourne un objet de type oxmar (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    // public function getObjectMarca(&$conn) {
    //       $arrayObjetos =[];
    //       $oxmar  = new Oxmar();
    //       $cOxmar = new OxmarDao(); 

    //       $oxmar->setMarid($this->marid);

    //       if($cOxmar->load($conn, $oxmar)) {
    //           $arrayObjetos[]=$oxmar;
    //           return $arrayObjetos;
    //       } else {
    //         return null;
    //       }
    // } 

    // Retourne un objet de type med (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05 
    // public function getObjectMed(&$conn) {
    //       $arrayObjetos =[];
    //       $med = new Med();
    //       $cMed = new MedDao(); 

    //       $med->setMedid($this->idmedicorealiza);

    //       if($cMed->load($conn, $med)) {
    //           $arrayObjetos[]=$med;
    //           return $arrayObjetos;
    //       } else {
    //         return null;
    //       }
    // }

    // // Retourne un objet de type oxodon (Relation d'association)
    // // Ok. Ing. FMBM 2018.11.05
    // public function getObjectOdon(&$conn) {
    //       $arrayObjetos =[];
    //       $odontograma  = new Oxodon();
    //       $cOdontograma = new OxodonDao(); 

    //       $odontograma->setOdonid($this->odonid);

    //       if($cOdontograma->load($conn, $odontograma)) {
    //           $arrayObjetos[]=$odontograma;
    //           return $arrayObjetos;
    //       } else {
    //         return null;
    //       }
    // }  


}

?>