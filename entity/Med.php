<?php
 /**
  * Med Value Object.
  * This class is value object representing database table med
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 02.NOV.2018
  * Revisado: Ok. 2018/12/20 09:19
  */

class Med {

    private $medid;
    private $saas_ciaid;
    private $idmedico;
    private $tipo;
    private $nombre;
    private $mesid;
    private $registromedico;
    private $direccion;
    private $celular;
    private $telefonos;
    private $beeper;
    private $ciuid;
    private $sigla;
    private $consultorio;
    private $estado;

    public function __construct(){}

    function getMedid() {
          return $this->medid;
    }
    function setMedid($medidIn) {
          $this->medid = $medidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getIdmedico() {
          return $this->idmedico;
    }
    function setIdmedico($idmedicoIn) {
          $this->idmedico = $idmedicoIn;
    }

    function getTipo() {
          return $this->tipo;
    }
    function setTipo($tipoIn) {
          $this->tipo = $tipoIn;
    }

    function getNombre() {
          return $this->nombre;
    }
    function setNombre($nombreIn) {
          $this->nombre = $nombreIn;
    }

    function getMesid() {
          return $this->mesid;
    }
    function setMesid($mesidIn) {
          $this->mesid = $mesidIn;
    }

    function getRegistromedico() {
          return $this->registromedico;
    }
    function setRegistromedico($registromedicoIn) {
          $this->registromedico = $registromedicoIn;
    }

    function getDireccion() {
          return $this->direccion;
    }
    function setDireccion($direccionIn) {
          $this->direccion = $direccionIn;
    }

    function getCelular() {
          return $this->celular;
    }
    function setCelular($celularIn) {
          $this->celular = $celularIn;
    }

    function getTelefonos() {
          return $this->telefonos;
    }
    function setTelefonos($telefonosIn) {
          $this->telefonos = $telefonosIn;
    }

    function getBeeper() {
          return $this->beeper;
    }
    function setBeeper($beeperIn) {
          $this->beeper = $beeperIn;
    }

    function getCiuid() {
          return $this->ciuid;
    }
    function setCiuid($ciuidIn) {
          $this->ciuid = $ciuidIn;
    }

    function getSigla() {
          return $this->sigla;
    }
    function setSigla($siglaIn) {
          $this->sigla = $siglaIn;
    }

    function getConsultorio() {
          return $this->consultorio;
    }
    function setConsultorio($consultorioIn) {
          $this->consultorio = $consultorioIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function setAll($medidIn,
          $saas_ciaidIn,
          $idmedicoIn,
          $tipoIn,
          $nombreIn,
          $mesidIn,
          $registromedicoIn,
          $direccionIn,
          $celularIn,
          $telefonosIn,
          $beeperIn,
          $ciuidIn,
          $siglaIn,
          $consultorioIn,
          $estadoIn) {
          $this->medid = $medidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->idmedico = $idmedicoIn;
          $this->tipo = $tipoIn;
          $this->nombre = $nombreIn;
          $this->mesid = $mesidIn;
          $this->registromedico = $registromedicoIn;
          $this->direccion = $direccionIn;
          $this->celular = $celularIn;
          $this->telefonos = $telefonosIn;
          $this->beeper = $beeperIn;
          $this->ciuid = $ciuidIn;
          $this->sigla = $siglaIn;
          $this->consultorio = $consultorioIn;
          $this->estado = $estadoIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getMedid() != $this->medid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getIdmedico() != $this->idmedico) {
                    return(false);
          }
          if ($valueObject->getTipo() != $this->tipo) {
                    return(false);
          }
          if ($valueObject->getNombre() != $this->nombre) {
                    return(false);
          }
          if ($valueObject->getMesid() != $this->mesid) {
                    return(false);
          }
          if ($valueObject->getRegistromedico() != $this->registromedico) {
                    return(false);
          }
          if ($valueObject->getDireccion() != $this->direccion) {
                    return(false);
          }
          if ($valueObject->getCelular() != $this->celular) {
                    return(false);
          }
          if ($valueObject->getTelefonos() != $this->telefonos) {
                    return(false);
          }
          if ($valueObject->getBeeper() != $this->beeper) {
                    return(false);
          }
          if ($valueObject->getCiuid() != $this->ciuid) {
                    return(false);
          }
          if ($valueObject->getSigla() != $this->sigla) {
                    return(false);
          }
          if ($valueObject->getConsultorio() != $this->consultorio) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Med, mapping to table med\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."medid = ".$this->medid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."idmedico = ".$this->idmedico."\n"; 
        $out = $out."tipo = ".$this->tipo."\n"; 
        $out = $out."nombre = ".$this->nombre."\n"; 
        $out = $out."mesid = ".$this->mesid."\n"; 
        $out = $out."registromedico = ".$this->registromedico."\n"; 
        $out = $out."direccion = ".$this->direccion."\n"; 
        $out = $out."celular = ".$this->celular."\n"; 
        $out = $out."telefonos = ".$this->telefonos."\n"; 
        $out = $out."beeper = ".$this->beeper."\n"; 
        $out = $out."ciuid = ".$this->ciuid."\n"; 
        $out = $out."sigla = ".$this->sigla."\n"; 
        $out = $out."consultorio = ".$this->consultorio."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Med();

        $cloned->setMedid($this->medid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setIdmedico($this->idmedico); 
        $cloned->setTipo($this->tipo); 
        $cloned->setNombre($this->nombre); 
        $cloned->setMesid($this->mesid); 
        $cloned->setRegistromedico($this->registromedico); 
        $cloned->setDireccion($this->direccion); 
        $cloned->setCelular($this->celular); 
        $cloned->setTelefonos($this->telefonos); 
        $cloned->setBeeper($this->beeper); 
        $cloned->setCiuid($this->ciuid); 
        $cloned->setSigla($this->sigla); 
        $cloned->setConsultorio($this->consultorio); 
        $cloned->setEstado($this->estado); 

        return $cloned;
    }

}

?>