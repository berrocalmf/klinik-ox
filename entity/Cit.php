<?php

 /**
  * Cit Value Object.
  * This class is value object representing database table cit
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 02.NOV.2018
  * Revisado: Ok. 2018/12/20 09:19
  */

class Cit {

    private $citid;
    private $saas_ciaid;
    private $medid;
    private $fecha_sol;
    private $horallegada;
    private $fechacita;
    private $sitio;
    private $horaatencion;
    private $afiid;
    private $mtcid;
    private $tiposolicitud;
    private $tipoorden;
    private $serid;
    private $pvez;
    private $urgencia;
    private $valortotal;
    private $valorcopa;
    private $valorexedente;
    private $impreso;
    private $cumplida;
    private $facturado;
    private $enviadocob;
    private $telaviso;
    private $obs;
    private $estado;
    private $fechafin;
    private $tercid;
    private $afivid;
    private $asignada;
    private $fechaultimavis;

    public function __construct(){}

    function getCitid() {
          return $this->citid;
    }
    function setCitid($citidIn) {
          $this->citid = $citidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getMedid() {
          return $this->medid;
    }
    function setMedid($medidIn) {
          $this->medid = $medidIn;
    }

    function getFecha_sol() {
          return $this->fecha_sol;
    }
    function setFecha_sol($fecha_solIn) {
          $this->fecha_sol = $fecha_solIn;
    }

    function getHorallegada() {
          return $this->horallegada;
    }
    function setHorallegada($horallegadaIn) {
          $this->horallegada = $horallegadaIn;
    }

    function getFechacita() {
          return $this->fechacita;
    }
    function setFechacita($fechacitaIn) {
          $this->fechacita = $fechacitaIn;
    }

    function getSitio() {
          return $this->sitio;
    }
    function setSitio($sitioIn) {
          $this->sitio = $sitioIn;
    }

    function getHoraatencion() {
          return $this->horaatencion;
    }
    function setHoraatencion($horaatencionIn) {
          $this->horaatencion = $horaatencionIn;
    }

    function getAfiid() {
          return $this->afiid;
    }
    function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    function getMtcid() {
          return $this->mtcid;
    }
    function setMtcid($mtcidIn) {
          $this->mtcid = $mtcidIn;
    }

    function getTiposolicitud() {
          return $this->tiposolicitud;
    }
    function setTiposolicitud($tiposolicitudIn) {
          $this->tiposolicitud = $tiposolicitudIn;
    }

    function getTipoorden() {
          return $this->tipoorden;
    }
    function setTipoorden($tipoordenIn) {
          $this->tipoorden = $tipoordenIn;
    }

    function getSerid() {
          return $this->serid;
    }
    function setSerid($seridIn) {
          $this->serid = $seridIn;
    }

    function getPvez() {
          return $this->pvez;
    }
    function setPvez($pvezIn) {
          $this->pvez = $pvezIn;
    }

    function getUrgencia() {
          return $this->urgencia;
    }
    function setUrgencia($urgenciaIn) {
          $this->urgencia = $urgenciaIn;
    }

    function getValortotal() {
          return $this->valortotal;
    }
    function setValortotal($valortotalIn) {
          $this->valortotal = $valortotalIn;
    }

    function getValorcopa() {
          return $this->valorcopa;
    }
    function setValorcopa($valorcopaIn) {
          $this->valorcopa = $valorcopaIn;
    }

    function getValorexedente() {
          return $this->valorexedente;
    }
    function setValorexedente($valorexedenteIn) {
          $this->valorexedente = $valorexedenteIn;
    }

    function getImpreso() {
          return $this->impreso;
    }
    function setImpreso($impresoIn) {
          $this->impreso = $impresoIn;
    }

    function getCumplida() {
          return $this->cumplida;
    }
    function setCumplida($cumplidaIn) {
          $this->cumplida = $cumplidaIn;
    }

    function getFacturado() {
          return $this->facturado;
    }
    function setFacturado($facturadoIn) {
          $this->facturado = $facturadoIn;
    }

    function getEnviadocob() {
          return $this->enviadocob;
    }
    function setEnviadocob($enviadocobIn) {
          $this->enviadocob = $enviadocobIn;
    }

    function getTelaviso() {
          return $this->telaviso;
    }
    function setTelaviso($telavisoIn) {
          $this->telaviso = $telavisoIn;
    }

    function getObs() {
          return $this->obs;
    }
    function setObs($obsIn) {
          $this->obs = $obsIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function getFechafin() {
          return $this->fechafin;
    }
    function setFechafin($fechafinIn) {
          $this->fechafin = $fechafinIn;
    }

    function getTercid() {
          return $this->tercid;
    }
    function setTercid($tercidIn) {
          $this->tercid = $tercidIn;
    }

    function getAfivid() {
          return $this->afivid;
    }
    function setAfivid($afividIn) {
          $this->afivid = $afividIn;
    }

    function getAsignada() {
          return $this->asignada;
    }
    function setAsignada($asignadaIn) {
          $this->asignada = $asignadaIn;
    }

    function getFechaultimavis() {
          return $this->fechaultimavis;
    }
    function setFechaultimavis($fechaultimavisIn) {
          $this->fechaultimavis = $fechaultimavisIn;
    }

    function setAll($citidIn,
          $saas_ciaidIn,
          $medidIn,
          $fecha_solIn,
          $horallegadaIn,
          $fechacitaIn,
          $sitioIn,
          $horaatencionIn,
          $afiidIn,
          $mtcidIn,
          $tiposolicitudIn,
          $tipoordenIn,
          $seridIn,
          $pvezIn,
          $urgenciaIn,
          $valortotalIn,
          $valorcopaIn,
          $valorexedenteIn,
          $impresoIn,
          $cumplidaIn,
          $facturadoIn,
          $enviadocobIn,
          $telavisoIn,
          $obsIn,
          $estadoIn,
          $fechafinIn,
          $tercidIn,
          $afividIn,
          $asignadaIn,
          $fechaultimavisIn) {
          $this->citid = $citidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->medid = $medidIn;
          $this->fecha_sol = $fecha_solIn;
          $this->horallegada = $horallegadaIn;
          $this->fechacita = $fechacitaIn;
          $this->sitio = $sitioIn;
          $this->horaatencion = $horaatencionIn;
          $this->afiid = $afiidIn;
          $this->mtcid = $mtcidIn;
          $this->tiposolicitud = $tiposolicitudIn;
          $this->tipoorden = $tipoordenIn;
          $this->serid = $seridIn;
          $this->pvez = $pvezIn;
          $this->urgencia = $urgenciaIn;
          $this->valortotal = $valortotalIn;
          $this->valorcopa = $valorcopaIn;
          $this->valorexedente = $valorexedenteIn;
          $this->impreso = $impresoIn;
          $this->cumplida = $cumplidaIn;
          $this->facturado = $facturadoIn;
          $this->enviadocob = $enviadocobIn;
          $this->telaviso = $telavisoIn;
          $this->obs = $obsIn;
          $this->estado = $estadoIn;
          $this->fechafin = $fechafinIn;
          $this->tercid = $tercidIn;
          $this->afivid = $afividIn;
          $this->asignada = $asignadaIn;
          $this->fechaultimavis = $fechaultimavisIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getMedid() != $this->medid) {
                    return(false);
          }
          if ($valueObject->getFecha_sol() != $this->fecha_sol) {
                    return(false);
          }
          if ($valueObject->getHorallegada() != $this->horallegada) {
                    return(false);
          }
          if ($valueObject->getFechacita() != $this->fechacita) {
                    return(false);
          }
          if ($valueObject->getSitio() != $this->sitio) {
                    return(false);
          }
          if ($valueObject->getHoraatencion() != $this->horaatencion) {
                    return(false);
          }
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getMtcid() != $this->mtcid) {
                    return(false);
          }
          if ($valueObject->getTiposolicitud() != $this->tiposolicitud) {
                    return(false);
          }
          if ($valueObject->getTipoorden() != $this->tipoorden) {
                    return(false);
          }
          if ($valueObject->getSerid() != $this->serid) {
                    return(false);
          }
          if ($valueObject->getPvez() != $this->pvez) {
                    return(false);
          }
          if ($valueObject->getUrgencia() != $this->urgencia) {
                    return(false);
          }
          if ($valueObject->getValortotal() != $this->valortotal) {
                    return(false);
          }
          if ($valueObject->getValorcopa() != $this->valorcopa) {
                    return(false);
          }
          if ($valueObject->getValorexedente() != $this->valorexedente) {
                    return(false);
          }
          if ($valueObject->getImpreso() != $this->impreso) {
                    return(false);
          }
          if ($valueObject->getCumplida() != $this->cumplida) {
                    return(false);
          }
          if ($valueObject->getFacturado() != $this->facturado) {
                    return(false);
          }
          if ($valueObject->getEnviadocob() != $this->enviadocob) {
                    return(false);
          }
          if ($valueObject->getTelaviso() != $this->telaviso) {
                    return(false);
          }
          if ($valueObject->getObs() != $this->obs) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getFechafin() != $this->fechafin) {
                    return(false);
          }
          if ($valueObject->getTercid() != $this->tercid) {
                    return(false);
          }
          if ($valueObject->getAfivid() != $this->afivid) {
                    return(false);
          }
          if ($valueObject->getAsignada() != $this->asignada) {
                    return(false);
          }
          if ($valueObject->getFechaultimavis() != $this->fechaultimavis) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Cit, mapping to table cit\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."citid = ".$this->citid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."medid = ".$this->medid."\n"; 
        $out = $out."fecha_sol = ".$this->fecha_sol."\n"; 
        $out = $out."horallegada = ".$this->horallegada."\n"; 
        $out = $out."fechacita = ".$this->fechacita."\n"; 
        $out = $out."sitio = ".$this->sitio."\n"; 
        $out = $out."horaatencion = ".$this->horaatencion."\n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."mtcid = ".$this->mtcid."\n"; 
        $out = $out."tiposolicitud = ".$this->tiposolicitud."\n"; 
        $out = $out."tipoorden = ".$this->tipoorden."\n"; 
        $out = $out."serid = ".$this->serid."\n"; 
        $out = $out."pvez = ".$this->pvez."\n"; 
        $out = $out."urgencia = ".$this->urgencia."\n"; 
        $out = $out."valortotal = ".$this->valortotal."\n"; 
        $out = $out."valorcopa = ".$this->valorcopa."\n"; 
        $out = $out."valorexedente = ".$this->valorexedente."\n"; 
        $out = $out."impreso = ".$this->impreso."\n"; 
        $out = $out."cumplida = ".$this->cumplida."\n"; 
        $out = $out."facturado = ".$this->facturado."\n"; 
        $out = $out."enviadocob = ".$this->enviadocob."\n"; 
        $out = $out."telaviso = ".$this->telaviso."\n"; 
        $out = $out."obs = ".$this->obs."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."fechafin = ".$this->fechafin."\n"; 
        $out = $out."tercid = ".$this->tercid."\n"; 
        $out = $out."afivid = ".$this->afivid."\n"; 
        $out = $out."asignada = ".$this->asignada."\n"; 
        $out = $out."fechaultimavis = ".$this->fechaultimavis."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Cit();

        $cloned->setCitid($this->citid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setMedid($this->medid); 
        $cloned->setFecha_sol($this->fecha_sol); 
        $cloned->setHorallegada($this->horallegada); 
        $cloned->setFechacita($this->fechacita); 
        $cloned->setSitio($this->sitio); 
        $cloned->setHoraatencion($this->horaatencion); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setMtcid($this->mtcid); 
        $cloned->setTiposolicitud($this->tiposolicitud); 
        $cloned->setTipoorden($this->tipoorden); 
        $cloned->setSerid($this->serid); 
        $cloned->setPvez($this->pvez); 
        $cloned->setUrgencia($this->urgencia); 
        $cloned->setValortotal($this->valortotal); 
        $cloned->setValorcopa($this->valorcopa); 
        $cloned->setValorexedente($this->valorexedente); 
        $cloned->setImpreso($this->impreso); 
        $cloned->setCumplida($this->cumplida); 
        $cloned->setFacturado($this->facturado); 
        $cloned->setEnviadocob($this->enviadocob); 
        $cloned->setTelaviso($this->telaviso); 
        $cloned->setObs($this->obs); 
        $cloned->setEstado($this->estado); 
        $cloned->setFechafin($this->fechafin); 
        $cloned->setTercid($this->tercid); 
        $cloned->setAfivid($this->afivid); 
        $cloned->setAsignada($this->asignada); 
        $cloned->setFechaultimavis($this->fechaultimavis); 

        return $cloned;
    }

}

?>