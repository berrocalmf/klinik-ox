<?php

 /**
  * Ome Value Object.
  * This class is value object representing database table ome
  * This class is intented to be used together with associated Dao object.
  * Créé par Ing. FMBM 2018.11.05
  * Revu et validé par Ing. FMBM 
  * Revisado: Ok. 2018/12/20 09:19
  */

class Ome {

    private $omeid;
    private $saas_ciaid;
    private $consecutivo;
    private $fechaome;
    private $edad;
    private $preid;
    private $urgencia;
    private $proid;
    private $fecharesultados;
    private $causaexterna;
    private $ambito;
    private $finalidad;
    private $imprimedx;
    private $impreso;
    private $afiid;
    private $ateid;
    private $usrid;
    private $citid; // private $afivid;

    public function __construct(){}

    function getOmeid() {
          return $this->omeid;
    }
    function setOmeid($omeidIn) {
          $this->omeid = $omeidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getConsecutivo() {
          return $this->consecutivo;
    }
    function setConsecutivo($consecutivoIn) {
          $this->consecutivo = $consecutivoIn;
    }

    function getFechaome() {
          return $this->fechaome;
    }
    function setFechaome($fechaomeIn) {
          $this->fechaome = $fechaomeIn;
    }

    function getEdad() {
          return $this->edad;
    }
    function setEdad($edadIn) {
          $this->edad = $edadIn;
    }

    function getPreid() {
          return $this->preid;
    }
    function setPreid($preidIn) {
          $this->preid = $preidIn;
    }

    function getUrgencia() {
          return $this->urgencia;
    }
    function setUrgencia($urgenciaIn) {
          $this->urgencia = $urgenciaIn;
    }

    function getProid() {
          return $this->proid;
    }
    function setProid($proidIn) {
          $this->proid = $proidIn;
    }

    function getFecharesultados() {
          return $this->fecharesultados;
    }
    function setFecharesultados($fecharesultadosIn) {
          $this->fecharesultados = $fecharesultadosIn;
    }

    function getCausaexterna() {
          return $this->causaexterna;
    }
    function setCausaexterna($causaexternaIn) {
          $this->causaexterna = $causaexternaIn;
    }

    function getAmbito() {
          return $this->ambito;
    }
    function setAmbito($ambitoIn) {
          $this->ambito = $ambitoIn;
    }

    function getFinalidad() {
          return $this->finalidad;
    }
    function setFinalidad($finalidadIn) {
          $this->finalidad = $finalidadIn;
    }

    function getImprimedx() {
          return $this->imprimedx;
    }
    function setImprimedx($imprimedxIn) {
          $this->imprimedx = $imprimedxIn;
    }

    function getImpreso() {
          return $this->impreso;
    }
    function setImpreso($impresoIn) {
          $this->impreso = $impresoIn;
    }

    function getAfiid() {
          return $this->afiid;
    }
    function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    function getCitid() {
          return $this->citid;
    }
    function setCitid($citiddIn) {
          $this->citid = $citiddIn;
    }

    function getAteid() {
          return $this->ateid;
    }
    function setAteid($ateidIn) {
          $this->ateid = $ateidIn;
    }

    function getUsrid() {
          return $this->usrid;
    }
    function setUsrid($usridIn) {
          $this->usrid = $usridIn;
    }

    function setAll($omeidIn,
          $saas_ciaidIn,
          $consecutivoIn,
          $fechaomeIn,
          $edadIn,
          $preidIn,
          $urgenciaIn,
          $proidIn,
          $fecharesultadosIn,
          $causaexternaIn,
          $ambitoIn,
          $finalidadIn,
          $imprimedxIn,
          $impresoIn,
          $afiidIn,
          $citidIn,
          $ateidIn,
          $usridIn) {
          $this->omeid = $omeidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->consecutivo = $consecutivoIn;
          $this->fechaome = $fechaomeIn;
          $this->edad = $edadIn;
          $this->preid = $preidIn;
          $this->urgencia = $urgenciaIn;
          $this->proid = $proidIn;
          $this->fecharesultados = $fecharesultadosIn;
          $this->causaexterna = $causaexternaIn;
          $this->ambito = $ambitoIn;
          $this->finalidad = $finalidadIn;
          $this->imprimedx = $imprimedxIn;
          $this->impreso = $impresoIn;
          $this->afiid = $afiidIn;
          $this->citid = $citidIn;
          $this->ateid = $ateidIn;
          $this->usrid = $usridIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getOmeid() != $this->omeid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getConsecutivo() != $this->consecutivo) {
                    return(false);
          }
          if ($valueObject->getFechaome() != $this->fechaome) {
                    return(false);
          }
          if ($valueObject->getEdad() != $this->edad) {
                    return(false);
          }
          if ($valueObject->getPreid() != $this->preid) {
                    return(false);
          }
          if ($valueObject->getUrgencia() != $this->urgencia) {
                    return(false);
          }
          if ($valueObject->getProid() != $this->proid) {
                    return(false);
          }
          if ($valueObject->getFecharesultados() != $this->fecharesultados) {
                    return(false);
          }
          if ($valueObject->getCausaexterna() != $this->causaexterna) {
                    return(false);
          }
          if ($valueObject->getAmbito() != $this->ambito) {
                    return(false);
          }
          if ($valueObject->getFinalidad() != $this->finalidad) {
                    return(false);
          }
          if ($valueObject->getImprimedx() != $this->imprimedx) {
                    return(false);
          }
          if ($valueObject->getImpreso() != $this->impreso) {
                    return(false);
          }
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          if ($valueObject->getAteid() != $this->ateid) {
                    return(false);
          }
          if ($valueObject->getUsrid() != $this->usrid) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Ome, mapping to table ome\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."omeid = ".$this->omeid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."consecutivo = ".$this->consecutivo."\n"; 
        $out = $out."fechaome = ".$this->fechaome."\n"; 
        $out = $out."edad = ".$this->edad."\n"; 
        $out = $out."preid = ".$this->preid."\n"; 
        $out = $out."urgencia = ".$this->urgencia."\n"; 
        $out = $out."proid = ".$this->proid."\n"; 
        $out = $out."fecharesultados = ".$this->fecharesultados."\n"; 
        $out = $out."causaexterna = ".$this->causaexterna."\n"; 
        $out = $out."ambito = ".$this->ambito."\n"; 
        $out = $out."finalidad = ".$this->finalidad."\n"; 
        $out = $out."imprimedx = ".$this->imprimedx."\n"; 
        $out = $out."impreso = ".$this->impreso."\n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."citid = ".$this->citid."\n"; 
        $out = $out."ateid = ".$this->ateid."\n"; 
        $out = $out."usrid = ".$this->usrid."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Ome();

        $cloned->setOmeid($this->omeid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setConsecutivo($this->consecutivo); 
        $cloned->setFechaome($this->fechaome); 
        $cloned->setEdad($this->edad); 
        $cloned->setPreid($this->preid); 
        $cloned->setUrgencia($this->urgencia); 
        $cloned->setProid($this->proid); 
        $cloned->setFecharesultados($this->fecharesultados); 
        $cloned->setCausaexterna($this->causaexterna); 
        $cloned->setAmbito($this->ambito); 
        $cloned->setFinalidad($this->finalidad); 
        $cloned->setImprimedx($this->imprimedx); 
        $cloned->setImpreso($this->impreso); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setCitid($this->citid); 
        $cloned->setAteid($this->ateid); 
        $cloned->setUsrid($this->usrid); 

        return $cloned;
    }

}

?>