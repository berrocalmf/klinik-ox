<?php

/**
  * Oxmar Value Object.
  * This class is value object representing database table oxmar
  * This class is intented to be used together with associated Dao object.
  * Ing. FMBM 05.NOV.2018 
  * (Falta actualizar y ajustar a la tabla de la base de datos.) */

class Oxmar {

    private $marid;
    private $codmarca;
    private $descripcion;
    private $textomarca;
    private $tipografica;
    private $colorrealizada;
    private $coloreporrealizar;
    private $colorlineades;
    private $mcaras;

    private $mtexto;
    private $mgrafica;

    private $sumaindiceplaca;
    private $sumaindicopceo;
    private $indcaries;
    private $estado;

    public function __construct(){}
    // function Oxmar () {}

    /* function Oxmar ($maridIn) {
          $this->marid = $maridIn;
    } */

    public function getMarid() {
          return $this->marid;
    }
    public function setMarid($maridIn) {
          $this->marid = $maridIn;
    }

    public function getCodmarca() {
          return $this->codmarca;
    }
    public function setCodmarca($codmarcaIn) {
          $this->codmarca = $codmarcaIn;
    }

    public function getDescripcion() {
          return $this->descripcion;
    }
    public function setDescripcion($descripcionIn) {
          $this->descripcion = $descripcionIn;
    }

    public function getTextomarca() {
          return $this->textomarca;
    }
    public function setTextomarca($textomarcaIn) {
          $this->textomarca = $textomarcaIn;
    }

    public function getTipografica() {
          return $this->tipografica;
    }
    public function setTipografica($tipograficaIn) {
          $this->tipografica = $tipograficaIn;
    }

    public function getColorrealizada() {
          return $this->colorrealizada;
    }
    public function setColorrealizada($colorrealizadaIn) {
          $this->colorrealizada = $colorrealizadaIn;
    }

    public function getColoreporrealizar() {
          return $this->coloreporrealizar;
    }
    public function setColoreporrealizar($coloreporrealizarIn) {
          $this->coloreporrealizar = $coloreporrealizarIn;
    }

    public function getColorlineades() {
          return $this->colorlineades;
    }
    public function setColorlineades($colorlineadesIn) {
          $this->colorlineades = $colorlineadesIn;
    }

    public function getMcaras() {
          return $this->mcaras;
    }
    public function setMcaras($mcarasIn) {
          $this->mcaras = $mcarasIn;
    }

    public function getMtexto() {
          return $this->mtexto;
    }
    public function setMtexto($mtextoIn) {
          $this->mtexto = $mtextoIn;
    }

    public function getMgrafica() {
          return $this->mgrafica;
    }
    public function setMgrafica($mgraficaIn) {
          $this->mgrafica = $mgraficaIn;
    }

    public function getSumaindiceplaca() {
          return $this->sumaindiceplaca;
    }
    public function setSumaindiceplaca($sumaindiceplacaIn) {
          $this->sumaindiceplaca = $sumaindiceplacaIn;
    }

    public function getSumaindicopceo() {
          return $this->sumaindicopceo;
    }
    public function setSumaindicopceo($sumaindicopceoIn) {
          $this->sumaindicopceo = $sumaindicopceoIn;
    }

    public function getIndcaries() {
          return $this->indcaries;
    }
    public function setIndcaries($indcariesIn) {
          $this->indcaries = $indcariesIn;
    }

    public function getEstado() {
          return $this->estado;
    }
    public function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    public function setAll($maridIn, $codmarcaIn, $descripcionIn,
          $textomarcaIn,
          $tipograficaIn,
          $colorrealizadaIn,
          $coloreporrealizarIn,
          $colorlineadesIn,
          $mcarasIn,$mtextoIn,$mgraficaIn,
          $sumaindiceplacaIn,
          $sumaindicopceoIn,
          $indcariesIn,
          $estadoIn) {
          $this->marid             = $maridIn;
          $this->codmarca          = $codmarcaIn;
          $this->descripcion       = $descripcionIn;
          $this->textomarca        = $textomarcaIn;
          $this->tipografica       = $tipograficaIn;
          $this->colorrealizada    = $colorrealizadaIn;
          $this->coloreporrealizar = $coloreporrealizarIn;
          $this->colorlineades     = $colorlineadesIn;
          $this->mcaras            = $mcarasIn;
          $this->setMtexto         = $mtextoIn;
          $this->setMgrafica       = $mgraficaIn;
          $this->sumaindiceplaca   = $sumaindiceplacaIn;
          $this->sumaindicopceo    = $sumaindicopceoIn;
          $this->indcaries         = $indcariesIn;
          $this->estado            = $estadoIn;
    }

    public function hasEqualMapping($valueObject) {

          if ($valueObject->getMarid() != $this->marid) {
                    return(false);
          }
          if ($valueObject->getCodmarca() != $this->codmarca) {
                    return(false);
          }
          if ($valueObject->getDescripcion() != $this->descripcion) {
                    return(false);
          }
          if ($valueObject->getTextomarca() != $this->textomarca) {
                    return(false);
          }
          if ($valueObject->getTipografica() != $this->tipografica) {
                    return(false);
          }
          if ($valueObject->getColorrealizada() != $this->colorrealizada) {
                    return(false);
          }
          if ($valueObject->getColoreporrealizar() != $this->coloreporrealizar) {
                    return(false);
          }
          if ($valueObject->getColorlineades() != $this->colorlineades) {
                    return(false);
          }
          if ($valueObject->getMcaras() != $this->mcaras) {
                    return(false);
          }
          if ($valueObject->getMtexto() != $this->mtexto) {
                    return(false);
          }
          if ($valueObject->getMgrafica() != $this->mgrafica) {
                    return(false);
          }
          if ($valueObject->getSumaindiceplaca() != $this->sumaindiceplaca) {
                    return(false);
          }
          if ($valueObject->getSumaindicopceo() != $this->sumaindicopceo) {
                    return(false);
          }
          if ($valueObject->getIndcaries() != $this->indcaries) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }

          return true;
    }

    public function toString() {
        $out = "";
        $out = $out."\nclass Oxmar, mapping to table oxmar\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."marid = ".$this->marid."\n"; 
        $out = $out."codmarca = ".$this->codmarca."\n"; 
        $out = $out."descripcion = ".$this->descripcion."\n"; 
        $out = $out."textomarca = ".$this->textomarca."\n"; 
        $out = $out."tipografica = ".$this->tipografica."\n"; 
        $out = $out."colorrealizada = ".$this->colorrealizada."\n"; 
        $out = $out."coloreporrealizar = ".$this->coloreporrealizar."\n"; 
        $out = $out."colorlineades = ".$this->colorlineades."\n"; 
        $out = $out."mcaras = ".$this->mcaras."\n"; 
        $out = $out."mtexto = ".$this->mtexto."\n"; 
        $out = $out."mgrafica = ".$this->mgrafica."\n"; 
        $out = $out."sumaindiceplaca = ".$this->sumaindiceplaca."\n"; 
        $out = $out."sumaindicopceo = ".$this->sumaindicopceo."\n"; 
        $out = $out."indcaries = ".$this->indcaries."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        return $out;
    }

    public function clone() {
        $cloned = new Oxmar();

        $cloned->setMarid($this->marid); 
        $cloned->setCodmarca($this->codmarca); 
        $cloned->setDescripcion($this->descripcion); 
        $cloned->setTextomarca($this->textomarca); 
        $cloned->setTipografica($this->tipografica); 
        $cloned->setColorrealizada($this->colorrealizada); 
        $cloned->setColoreporrealizar($this->coloreporrealizar); 
        $cloned->setColorlineades($this->colorlineades); 
        $cloned->setMcaras($this->mcaras); 
        $cloned->setMtexto($this->mtexto); 
        $cloned->setMgrafica($this->mgrafica); 
        $cloned->setSumaindiceplaca($this->sumaindiceplaca); 
        $cloned->setSumaindicopceo($this->sumaindicopceo); 
        $cloned->setIndcaries($this->indcaries); 
        $cloned->setEstado($this->estado); 

        return $cloned;
    }
    
}

?>