<?php

 /**
  * Ate Value Object.
  * This class is value object representing database table ate
  * This class is intented to be used together with associated Dao object.
  * Revisado: Ok. 2018/12/20 09:19
  */

require_once("../entity/Cit.php");
require_once("../model/CitDao.php");
require_once("../entity/Afi.php");
require_once("../model/AfiDao.php");
require_once("../model/OxodonDao.php");
require_once("../model/PptoDao.php");

class Ate {

    /** 
     * Persistent Instance privateiables. This data is directly 
     * mapped to the columns of database table.
     */
    private $ateid;
    private $saas_ciaid;
    private $tipoatencion;
    private $consecutivo;
    private $afiid;             // <-- FK
    private $terid;             // <-- FK
    private $tercntid;          // <-- FK
    private $fecha;
    private $aconombre;
    private $acoparentesco;
    private $acodocid;
    private $acodireccion;
    private $acotelefono;
    private $acociuid;
    private $resnombre;
    private $restelefono;
    private $resparentesco;
    private $resciuid;
    private $citid;             // <-- FK
    private $motivoate;
    private $fechafin;
    private $feproxcontrol;
    private $estado;
    private $docidafiliado;

    public function __construct(){}

    function getAteid() {
          return $this->ateid;
    }
    function setAteid($ateidIn) {
          $this->ateid = $ateidIn;
    }

    function getSaas_ciaid() {
          return $this->saas_ciaid;
    }
    function setSaas_ciaid($saas_ciaidIn) {
          $this->saas_ciaid = $saas_ciaidIn;
    }

    function getTipoatencion() {
          return $this->tipoatencion;
    }
    function setTipoatencion($tipoatencionIn) {
          $this->tipoatencion = $tipoatencionIn;
    }

    function getConsecutivo() {
          return $this->consecutivo;
    }
    function setConsecutivo($consecutivoIn) {
          $this->consecutivo = $consecutivoIn;
    }

    function getAfiid() {
          return $this->afiid;
    }
    function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    function getTerid() {
          return $this->terid;
    }
    function setTerid($teridIn) {
          $this->terid = $teridIn;
    }

    function getTercntid() {
          return $this->tercntid;
    }
    function setTercntid($tercntidIn) {
          $this->tercntid = $tercntidIn;
    }

    function getFecha() {
          return $this->fecha;
    }
    function setFecha($fechaIn) {
          $this->fecha = $fechaIn;
    }

    function getAconombre() {
          return $this->aconombre;
    }
    function setAconombre($aconombreIn) {
          $this->aconombre = $aconombreIn;
    }

    function getAcoparentesco() {
          return $this->acoparentesco;
    }
    function setAcoparentesco($acoparentescoIn) {
          $this->acoparentesco = $acoparentescoIn;
    }

    function getAcodocid() {
          return $this->acodocid;
    }
    function setAcodocid($acodocidIn) {
          $this->acodocid = $acodocidIn;
    }

    function getAcodireccion() {
          return $this->acodireccion;
    }
    function setAcodireccion($acodireccionIn) {
          $this->acodireccion = $acodireccionIn;
    }

    function getAcotelefono() {
          return $this->acotelefono;
    }
    function setAcotelefono($acotelefonoIn) {
          $this->acotelefono = $acotelefonoIn;
    }

    function getAcociuid() {
          return $this->acociuid;
    }
    function setAcociuid($acociuidIn) {
          $this->acociuid = $acociuidIn;
    }

    function getResnombre() {
          return $this->resnombre;
    }
    function setResnombre($resnombreIn) {
          $this->resnombre = $resnombreIn;
    }

    function getRestelefono() {
          return $this->restelefono;
    }
    function setRestelefono($restelefonoIn) {
          $this->restelefono = $restelefonoIn;
    }

    function getResparentesco() {
          return $this->resparentesco;
    }
    function setResparentesco($resparentescoIn) {
          $this->resparentesco = $resparentescoIn;
    }

    function getResciuid() {
          return $this->resciuid;
    }
    function setResciuid($resciuidIn) {
          $this->resciuid = $resciuidIn;
    }

    function getCitid() {
          return $this->citid;
    }
    function setCitid($citidIn) {
          $this->citid = $citidIn;
    }

    function getMotivoate() {
          return $this->motivoate;
    }
    function setMotivoate($motivoateIn) {
          $this->motivoate = $motivoateIn;
    }

    function getFechafin() {
          return $this->fechafin;
    }
    function setFechafin($fechafinIn) {
          $this->fechafin = $fechafinIn;
    }

    function getFeproxcontrol() {
          return $this->feproxcontrol;
    }
    function setFeproxcontrol($feproxcontrolIn) {
          $this->feproxcontrol = $feproxcontrolIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function getDocidafiliado() {
          return $this->docidafiliado;
    }
    function setDocidafiliado($docidafiliadoIn) {
          $this->docidafiliado = $docidafiliadoIn;
    }

    function setAll($ateidIn,
          $saas_ciaidIn,
          $tipoatencionIn,
          $consecutivoIn,
          $afiidIn,
          $teridIn,
          $tercntidIn,
          $fechaIn,
          $aconombreIn,
          $acoparentescoIn,
          $acodocidIn,
          $acodireccionIn,
          $acotelefonoIn,
          $acociuidIn,
          $resnombreIn,
          $restelefonoIn,
          $resparentescoIn,
          $resciuidIn,
          $citidIn,
          $motivoateIn,
          $fechafinIn,
          $feproxcontrolIn,
          $estadoIn,
          $docidafiliadoIn) {
          $this->ateid = $ateidIn;
          $this->saas_ciaid = $saas_ciaidIn;
          $this->tipoatencion = $tipoatencionIn;
          $this->consecutivo = $consecutivoIn;
          $this->afiid = $afiidIn;
          $this->terid = $teridIn;
          $this->tercntid = $tercntidIn;
          $this->fecha = $fechaIn;
          $this->aconombre = $aconombreIn;
          $this->acoparentesco = $acoparentescoIn;
          $this->acodocid = $acodocidIn;
          $this->acodireccion = $acodireccionIn;
          $this->acotelefono = $acotelefonoIn;
          $this->acociuid = $acociuidIn;
          $this->resnombre = $resnombreIn;
          $this->restelefono = $restelefonoIn;
          $this->resparentesco = $resparentescoIn;
          $this->resciuid = $resciuidIn;
          $this->citid = $citidIn;
          $this->motivoate = $motivoateIn;
          $this->fechafin = $fechafinIn;
          $this->feproxcontrol = $feproxcontrolIn;
          $this->estado = $estadoIn;
          $this->docidafiliado = $docidafiliadoIn;
    }


    function hasEqualMapping($valueObject) {

          if ($valueObject->getAteid() != $this->ateid) {
                    return(false);
          }
          if ($valueObject->getSaas_ciaid() != $this->saas_ciaid) {
                    return(false);
          }
          if ($valueObject->getTipoatencion() != $this->tipoatencion) {
                    return(false);
          }
          if ($valueObject->getConsecutivo() != $this->consecutivo) {
                    return(false);
          }
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getTerid() != $this->terid) {
                    return(false);
          }
          if ($valueObject->getTercntid() != $this->tercntid) {
                    return(false);
          }
          if ($valueObject->getFecha() != $this->fecha) {
                    return(false);
          }
          if ($valueObject->getAconombre() != $this->aconombre) {
                    return(false);
          }
          if ($valueObject->getAcoparentesco() != $this->acoparentesco) {
                    return(false);
          }
          if ($valueObject->getAcodocid() != $this->acodocid) {
                    return(false);
          }
          if ($valueObject->getAcodireccion() != $this->acodireccion) {
                    return(false);
          }
          if ($valueObject->getAcotelefono() != $this->acotelefono) {
                    return(false);
          }
          if ($valueObject->getAcociuid() != $this->acociuid) {
                    return(false);
          }
          if ($valueObject->getResnombre() != $this->resnombre) {
                    return(false);
          }
          if ($valueObject->getRestelefono() != $this->restelefono) {
                    return(false);
          }
          if ($valueObject->getResparentesco() != $this->resparentesco) {
                    return(false);
          }
          if ($valueObject->getResciuid() != $this->resciuid) {
                    return(false);
          }
          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          if ($valueObject->getMotivoate() != $this->motivoate) {
                    return(false);
          }
          if ($valueObject->getFechafin() != $this->fechafin) {
                    return(false);
          }
          if ($valueObject->getFeproxcontrol() != $this->feproxcontrol) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getDocidafiliado() != $this->docidafiliado) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Ate, mapping to table ate\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."ateid = ".$this->ateid."\n"; 
        $out = $out."saas_ciaid = ".$this->saas_ciaid."\n"; 
        $out = $out."tipoatencion = ".$this->tipoatencion."\n"; 
        $out = $out."consecutivo = ".$this->consecutivo."\n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."terid = ".$this->terid."\n"; 
        $out = $out."tercntid = ".$this->tercntid."\n"; 
        $out = $out."fecha = ".$this->fecha."\n"; 
        $out = $out."aconombre = ".$this->aconombre."\n"; 
        $out = $out."acoparentesco = ".$this->acoparentesco."\n"; 
        $out = $out."acodocid = ".$this->acodocid."\n"; 
        $out = $out."acodireccion = ".$this->acodireccion."\n"; 
        $out = $out."acotelefono = ".$this->acotelefono."\n"; 
        $out = $out."acociuid = ".$this->acociuid."\n"; 
        $out = $out."resnombre = ".$this->resnombre."\n"; 
        $out = $out."restelefono = ".$this->restelefono."\n"; 
        $out = $out."resparentesco = ".$this->resparentesco."\n"; 
        $out = $out."resciuid = ".$this->resciuid."\n"; 
        $out = $out."citid = ".$this->citid."\n"; 
        $out = $out."motivoate = ".$this->motivoate."\n"; 
        $out = $out."fechafin = ".$this->fechafin."\n"; 
        $out = $out."feproxcontrol = ".$this->feproxcontrol."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."docidafiliado = ".$this->docidafiliado."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Ate();

        $cloned->setAteid($this->ateid); 
        $cloned->setSaas_ciaid($this->saas_ciaid); 
        $cloned->setTipoatencion($this->tipoatencion); 
        $cloned->setConsecutivo($this->consecutivo); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setTerid($this->terid); 
        $cloned->setTercntid($this->tercntid); 
        $cloned->setFecha($this->fecha); 
        $cloned->setAconombre($this->aconombre); 
        $cloned->setAcoparentesco($this->acoparentesco); 
        $cloned->setAcodocid($this->acodocid); 
        $cloned->setAcodireccion($this->acodireccion); 
        $cloned->setAcotelefono($this->acotelefono); 
        $cloned->setAcociuid($this->acociuid); 
        $cloned->setResnombre($this->resnombre); 
        $cloned->setRestelefono($this->restelefono); 
        $cloned->setResparentesco($this->resparentesco); 
        $cloned->setResciuid($this->resciuid); 
        $cloned->setCitid($this->citid); 
        $cloned->setMotivoate($this->motivoate); 
        $cloned->setFechafin($this->fechafin); 
        $cloned->setFeproxcontrol($this->feproxcontrol); 
        $cloned->setEstado($this->estado); 
        $cloned->setDocidafiliado($this->docidafiliado); 

        return $cloned;
    }

    // Relaciones de sociación

    // Retourne un objet de type afi (Relation d'association)
    // Ok. Ing. FMBM 2018/11/05
    public function getObjectAfi(&$conn) {
          $arrayObjetos =[];
          $afi = new Afi();
          $cAfi = new AfiDao(); 

          $afi->setAfiid($this->afiid);

          if($cAfi->load($conn, $afi)) {
              $arrayObjetos[]=$afi;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type cit (Relation d'association)
    // Ok. Ing. FMBM 2018/11/05
    public function getObjectCit(&$conn) {
          $arrayObjetos =[];
          $cit = new Cit();
          $cCit = new CitDao(); 

          $cit->setCitid($this->citid);

          if($cCit->load($conn, $cit)) {
              $arrayObjetos[]=$cit;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Relaciones uno a muchos detalles de Atenciones (ATE)

    // Renvoie un tableau avec des objets oxodon (relation d'association un à plusieurs) 
    // Ok. Ing. FMBM 2018/11/05
    function getOdontogramas(&$conn) {
          $arrayObjetos=[];
          $cOxodon = new OxodonDao(); 
          $sql = "SELECT * FROM oxodon WHERE (ateid = ".$this->ateid.") "; 
        
          $arrayObjetos =  $cOxodon->listQuery($conn,$sql);
          return $arrayObjetos;    
    }

    // Renvoie un tableau avec des objets ppto (relation d'association un à plusieurs)
    // Ing. FMBM 2019/01/21
    function getPresupuesto(&$conn) {
          $arrayObjetos=[];
          $cPpto = new PptoDao();
          $sql = "SELECT * FROM ppto WHERE (ateid = ".$this->ateid.") ";
          $arrayObjetos = $cPpto->listQuery($conn,$sql);
          return $arrayObjetos;
    }

}

?>