<?php
// Ok. Ing. FMBM 27.OCT.2018
require_once("../entity/Ate.php");
require_once("../model/AteDao.php");
require_once("../entity/Oxodon.php");
require_once("../model/OxodonDao.php");
require_once("../entity/Ser.php");
require_once("../model/SerDao.php");
require_once("../entity/Oxpid.php");
require_once("../model/OxpidDao.php");
require_once("../entity/Oxmar.php");
require_once("../model/OxmarDao.php");
require_once("../entity/Med.php");
require_once("../model/MedDao.php");
require_once("../entity/Omed.php");
require_once("../model/OmedDao.php");

class Oxproc {

    /**
     * OXPROC Entity
     * Créé par Ing. FMBM
     * Revu et validé par Ing. FMBM 2018.10.27
      Reviewed: Ok. Ing. FMBM 2018/12/20 10:37
    */

    // Persistent Instance variables. This data is directly mapped to the columns of database table.
    private $procid;
    private $codproced;    
    private $ateid;         // FK  ($ateid reemplaza a $tratid) avec l'entité ate
    private $odonid;        // FK avec l'entité oxodon
    private $serid;         // FK avec l'entité ser
    private $descripcion;
    private $pidid;         // FK avec l'entité Oxpid
    private $caraTXT;
    private $procarapieza;
    private $procsector;
    private $marid;         // FK avec l'entité Oxmar
    private $estado;
    private $fechaproc;
    private $fecharealizada;
    private $cantidad;
    private $procvalor;
    private $procvalortotal;
    private $vlrcopagopres;
    private $procautorizar;
    private $observaciones;
    private $medidprog;     // FK avec l'entité Med
    private $medidrealiz;   // FK avec l'entité Med
    private $cobid;         // FK avec l'entité cob
    private $conscob;
    private $finalidad;
    private $ambito;
    private $costumerid;
    private $omedid;        // FK avec l'entité omed
    private $pptoid;        // FK avec l'entité ppto
    private $motivoAnula;
    private $personal_at;

    public function __construct(){}
    // function Oxproc () {}

    // Get- and Set-methods for persistent variables:
    // Ok. Ing. FMBM 27.OCT.2018

    public function getProcid() {
          return $this->procid;
    }
    public function setProcid($procidIn) {
          $this->procid = $procidIn;
    }

    public function getCodproced() {
          return $this->codproced;
    }
    public function setCodproced($codprocedIn) {
          $this->codproced = $codprocedIn;
    }

    public function getAteid() {
          return $this->ateid;
    }
    public function setAteid($ateidIn) {
          $this->ateid = $ateidIn;
    }

    public function getOdonid() {
          return $this->odonid;
    }
    public function setOdonid($odonidIn) {
          $this->odonid = $odonidIn;
    }

    public function getSerid() {
          return $this->serid;
    }
    public function setSerid($seridIn) {
          $this->serid = $seridIn;
    }

    public function getDescripcion() {
          return $this->descripcion;
    }
    public function setDescripcion($descripcionIn) {
          $this->descripcion = $descripcionIn;
    }

    public function getPidid() {
          return $this->pidid;
    }
    public function setPidid($pididIn) {
          $this->pidid = $pididIn;
    }

    public function getCaraTXT() {
          return $this->caraTXT;
    }
    public function setCaraTXT($caraTXTIn) {
          $this->caraTXT = $caraTXTIn;
    }

    public function getProcarapieza() {
          return $this->procarapieza;
    }
    public function setProcarapieza($procarapiezaIn) {
          $this->procarapieza = $procarapiezaIn;
    }

    public function getProcsector() {
          return $this->procsector;
    }
    public function setProcsector($procsectorIn) {
          $this->procsector = $procsectorIn;
    }

    public function getMarid() {
          return $this->marid;
    }
    public function setMarid($maridIn) {
          $this->marid = $maridIn;
    }

    public function getEstado() {
          return $this->estado;
    }
    public function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    public function getFechaproc() {
          return $this->fechaproc;
    }
    public function setFechaproc($fechaprocIn) {
          $this->fechaproc = $fechaprocIn;
    }

    public function getFecharealizada() {
          return $this->fecharealizada;
    }
    public function setFecharealizada($fecharealizadaIn) {
          $this->fecharealizada = $fecharealizadaIn;
    }

    public function getCantidad() {
          return $this->cantidad;
    }
    public function setCantidad($cantidadIn) {
          $this->cantidad = $cantidadIn;
    }

    public function getProcvalor() {
          return $this->procvalor;
    }
    public function setProcvalor($procvalorIn) {
          $this->procvalor = $procvalorIn;
    }

    public function getProcvalortotal() {
          return $this->procvalortotal;
    }
    public function setProcvalortotal($procvalortotalIn) {
          $this->procvalortotal = $procvalortotalIn;
    }

    public function getVlrcopagopres() {
          return $this->vlrcopagopres;
    }
    public function setVlrcopagopres($vlrcopagopresIn) {
          $this->vlrcopagopres = $vlrcopagopresIn;
    }

    public function getProcautorizar() {
          return $this->procautorizar;
    }
    public function setProcautorizar($procautorizarIn) {
          $this->procautorizar = $procautorizarIn;
    }

    public function getObservaciones() {
          return $this->observaciones;
    }
    public function setObservaciones($observacionesIn) {
          $this->observaciones = $observacionesIn;
    }

    public function getMedidprog() {
          return $this->medidprog;
    }
    public function setMedidprog($medidprogIn) {
          $this->medidprog = $medidprogIn;
    }

    public function getMedidrealiz() {
          return $this->medidrealiz;
    }
    public function setMedidrealiz($medidrealizIn) {
          $this->medidrealiz = $medidrealizIn;
    }

    public function getCobid() {
          return $this->cobid;
    }
    public function setCobid($cobidIn) {
          $this->cobid = $cobidIn;
    }

    public function getConscob() {
          return $this->conscob;
    }
    public function setConscob($conscobIn) {
          $this->conscob = $conscobIn;
    }

    public function getFinalidad() {
          return $this->finalidad;
    }
    public function setFinalidad($finalidadIn) {
          $this->finalidad = $finalidadIn;
    }

    public function getAmbito() {
          return $this->ambito;
    }
    public function setAmbito($ambitoIn) {
          $this->ambito = $ambitoIn;
    }

    public function getCostumerid() {
          return $this->costumerid;
    }
    public function setCostumerid($costumeridIn) {
          $this->costumerid = $costumeridIn;
    }


     public function getOmedid() {
          return $this->omedid;
    }
    public function setOmedid($omedidIn) {
          $this->omedid = $omedidIn;
    }

     public function getPptoid() {
          return $this->pptoid;
    }
    public function setPptoid($pptoidIn) {
          $this->pptoid = $pptoidIn;
    }

     public function getMotivoAnula() {
          return $this->motivoAnula;
    }
    public function setMotivoAnula($motivoanulaIn) {
          $this->motivoAnula = $motivoanulaIn;
    }

     public function getPersonal_at() {
          return $this->personal_at;
    }
    public function setPersonal_at($personal_atIn) {
          $this->personal_at = $personal_atIn;
    }

    // setAll allows to set all persistent variables in one method call.
    public function setAll($procidIn,
          $codprocedIn,
          $ateidIn,
          $odonidIn,
          $seridIn,
          $descripcionIn,
          $pididIn,
          $caraTXTIn,
          $procarapiezaIn,
          $procsectorIn,
          $maridIn,
          $estadoIn,
          $fechaprocIn,
          $fecharealizadaIn,
          $cantidadIn,
          $procvalorIn,
          $procvalortotalIn,
          $vlrcopagopresIn,
          $procautorizarIn,
          $observacionesIn,
          $medidprogIn,
          $medidrealizIn,
          $cobidIn,
          $conscobIn,
          $finalidadIn,
          $ambitoIn,
          $costumeridIn,
          $omedidIn,
          $pptoidIn,
          $motivoAnulaIn,
          $personal_atIn) {
          $this->procid = $procidIn;
          $this->codproced = $codprocedIn;
          $this->ateid = $ateidIn;
          $this->odonid = $odonidIn;
          $this->serid = $seridIn;
          $this->descripcion = $descripcionIn;
          $this->pidid = $pididIn;
          $this->caraTXT = $caraTXTIn;
          $this->procarapieza = $procarapiezaIn;
          $this->procsector = $procsectorIn;
          $this->marid = $maridIn;
          $this->estado = $estadoIn;
          $this->fechaproc = $fechaprocIn;
          $this->fecharealizada = $fecharealizadaIn;
          $this->cantidad = $cantidadIn;
          $this->procvalor = $procvalorIn;
          $this->procvalortotal = $procvalortotalIn;
          $this->vlrcopagopres = $vlrcopagopresIn;
          $this->procautorizar = $procautorizarIn;
          $this->observaciones = $observacionesIn;
          $this->medidprog = $medidprogIn;
          $this->medidrealiz = $medidrealizIn;
          $this->cobid = $cobidIn;
          $this->conscob = $conscobIn;
          $this->finalidad = $finalidadIn;
          $this->ambito = $ambitoIn;
          $this->costumerid = $costumeridIn;
          $this->omedid = $omedidIn;
          $this->pptoid = $pptoidIn;
          $this->motivoAnula = $motivoAnulaIn;
          $this->personal_at = $personal_atIn;
    }

    // Compare tow objects and return true id they are equals
    public function hasEqualMapping($valueObject) {

          if ($valueObject->getProcid() != $this->procid) {
                    return(false);
          }
          if ($valueObject->getCodproced() != $this->codproced) {
                    return(false);
          }
          if ($valueObject->getAteid() != $this->ateid) {
                    return(false);
          }
          if ($valueObject->getOdonid() != $this->odonid) {
                    return(false);
          }
          if ($valueObject->getSerid() != $this->serid) {
                    return(false);
          }
          if ($valueObject->getDescripcion() != $this->descripcion) {
                    return(false);
          }
          if ($valueObject->getPidid() != $this->pidid) {
                    return(false);
          }
          if ($valueObject->getCaraTXT() != $this->caraTXT) {
                    return(false);
          }
          if ($valueObject->getProcarapieza() != $this->procarapieza) {
                    return(false);
          }
          if ($valueObject->getProcsector() != $this->procsector) {
                    return(false);
          }
          if ($valueObject->getMarid() != $this->marid) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getFechaproc() != $this->fechaproc) {
                    return(false);
          }
          if ($valueObject->getFecharealizada() != $this->fecharealizada) {
                    return(false);
          }
          if ($valueObject->getCantidad() != $this->cantidad) {
                    return(false);
          }
          if ($valueObject->getProcvalor() != $this->procvalor) {
                    return(false);
          }
          if ($valueObject->getProcvalortotal() != $this->procvalortotal) {
                    return(false);
          }
          if ($valueObject->getVlrcopagopres() != $this->vlrcopagopres) {
                    return(false);
          }
          if ($valueObject->getProcautorizar() != $this->procautorizar) {
                    return(false);
          }
          if ($valueObject->getObservaciones() != $this->observaciones) {
                    return(false);
          }
          if ($valueObject->getMedidprog() != $this->medidprog) {
                    return(false);
          }
          if ($valueObject->getMedidrealiz() != $this->medidrealiz) {
                    return(false);
          }
          if ($valueObject->getCobid() != $this->cobid) {
                    return(false);
          }
          if ($valueObject->getConscob() != $this->conscob) {
                    return(false);
          }
          if ($valueObject->getFinalidad() != $this->finalidad) {
                    return(false);
          }
          if ($valueObject->getAmbito() != $this->ambito) {
                    return(false);
          }
          if ($valueObject->getCostumerid() != $this->costumerid) {
                    return(false);
          }
          if ($valueObject->getOmedid() != $this->omedid) {
                    return(false);
          }
          if ($valueObject->getPptoid() != $this->pptoid) {
                    return(false);
          }
          if ($valueObject->getMotivoAnula() != $this->motivoAnula) {
                    return(false);
          }
          if ($valueObject->getPersonal_at() != $this->personal_at) {
                    return(false);
          }

          return true;
    }

    // Ok. Ing. FMBM 27.OCT.2018
    public function toString() {
        $out = "";
        $out = $out."class Oxproc, mapping to table oxproc<br>";
        $out = $out."Persistent attributes:"."<br>"; 
        $out = $out."procid = [".$this->procid."]<br>"; 
        $out = $out."codproced = [".$this->codproced."]<br>"; 
        $out = $out."ateid = [".$this->ateid."]<br>"; 
        $out = $out."odonid = [".$this->odonid."]<br>"; 
        $out = $out."serid = [".$this->serid."]<br>"; 
        $out = $out."descripcion = [".$this->descripcion."]<br>"; 
        $out = $out."pidid = [".$this->pidid."]<br>"; 
        $out = $out."caraTXT = [".$this->caraTXT."]<br>"; 
        $out = $out."procarapieza = [".$this->procarapieza."]<br>"; 
        $out = $out."procsector = [".$this->procsector."]<br>"; 
        $out = $out."marid = [".$this->marid."]<br>"; 
        $out = $out."estado = [".$this->estado."]<br>"; 
        $out = $out."fechaproc = [".$this->fechaproc."]<br>"; 
        $out = $out."fecharealizada = [".$this->fecharealizada."]<br>"; 
        $out = $out."cantidad = [".$this->cantidad."]<br>"; 
        $out = $out."procvalor = [".$this->procvalor."]<br>"; 
        $out = $out."procvalortotal = [".$this->procvalortotal."]<br>"; 
        $out = $out."vlrcopagopres = [".$this->vlrcopagopres."]<br>"; 
        $out = $out."procautorizar = [".$this->procautorizar."]<br>"; 
        $out = $out."observaciones = [".$this->observaciones."]<br>"; 
        $out = $out."medidprog = [".$this->medidprog."]<br>"; 
        $out = $out."medidrealiz = [".$this->medidrealiz."]<br>"; 
        $out = $out."cobid = [".$this->cobid."]<br>"; 
        $out = $out."conscob = [".$this->conscob."]<br>"; 
        $out = $out."finalidad = [".$this->finalidad."]<br>"; 
        $out = $out."ambito = [".$this->ambito."]<br>"; 
        $out = $out."costumerid = [".$this->costumerid."]<br>"; 
        $out = $out."omedid = [".$this->omedid."]<br>"; 
        $out = $out."pptoid = [".$this->pptoid."]<br>"; 
        $out = $out."motivoAnula = [".$this->motivoAnula."]<br>"; 
        $out = $out."personal_at = [".$this->personal_at."]<br>"; 
        return $out;
    }

     // Clone will return identical deep copy of this valueObject.
    public function clone() {
        $cloned = new Oxproc();

        $cloned->setProcid($this->procid); 
        $cloned->setCodproced($this->codproced); 
        $cloned->setAteid($this->ateid); 
        $cloned->setOdonid($this->odonid); 
        $cloned->setSerid($this->serid); 
        $cloned->setDescripcion($this->descripcion); 
        $cloned->setPidid($this->pidid); 
        $cloned->setCaraTXT($this->caraTXT); 
        $cloned->setProcarapieza($this->procarapieza); 
        $cloned->setProcsector($this->procsector); 
        $cloned->setMarid($this->marid); 
        $cloned->setEstado($this->estado); 
        $cloned->setFechaproc($this->fechaproc); 
        $cloned->setFecharealizada($this->fecharealizada); 
        $cloned->setCantidad($this->cantidad); 
        $cloned->setProcvalor($this->procvalor); 
        $cloned->setProcvalortotal($this->procvalortotal); 
        $cloned->setVlrcopagopres($this->vlrcopagopres); 
        $cloned->setProcautorizar($this->procautorizar); 
        $cloned->setObservaciones($this->observaciones); 
        $cloned->setMedidprog($this->medidprog); 
        $cloned->setMedidrealiz($this->medidrealiz); 
        $cloned->setCobid($this->cobid); 
        $cloned->setConscob($this->conscob); 
        $cloned->setFinalidad($this->finalidad); 
        $cloned->setAmbito($this->ambito); 
        $cloned->setCostumerid($this->costumerid);
        $cloned->setOmedid($this->omedid);
        $cloned->setPptoid($this->pptoid);
        $cloned->setMotivoAnula($this->motivoAnula);
        $cloned->setPersonal_at($this->personal_at); 

        return $cloned;
    }

    // Retorna un objeto de tipo ATE (Relación de asociación) 
    // Ok. Ing. FMBM 02.NOV.2018
    public function getObjectAte(&$conn) {
          $arrayObjetos =[];
          $ate = new Ate();
          $cAte = new AteDao(); 

          $ate->setAteid($this->ateid);

          if($cAte->load($conn, $ate)) {
              $arrayObjetos[]=$ate;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo Oxodon (Relación de asociación)
    // Ok. Ing. FMBM 27.OCT.2018 
    public function getObjectOdon(&$conn) {
          $arrayObjetos =[];
          $odontograma  = new Oxodon();
          $cOdontograma = new OxodonDao(); 

          $odontograma->setOdonid($this->odonid);

          if($cOdontograma->load($conn, $odontograma)) {
              $arrayObjetos[]=$odontograma;
              return $arrayObjetos;
          } else {
            return null;
          }
    }
    
    // Retourne un objet de type ser (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    public function getObjectSer(&$conn) {
          $arrayObjetos =[];
          $ser  = new Ser();
          $cSer = new SerDao(); 

          $ser->setSerid($this->serid);

          if($cSer->load($conn, $ser)) {
              $arrayObjetos[]=$ser;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo oxpid (Relación de asociación)
    // Ok. Ing. FMBM 03.NOV.2018 
    public function getObjectPieza(&$conn) {
          $arrayObjetos =[];
          $oxpid  = new Oxpid();
          $cOxpid = new OxpidDao(); 

          $oxpid->setPidid($this->pidid);

          if($cOxpid->load($conn, $oxpid)) {
              $arrayObjetos[]=$oxpid;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo oxmar (Relación de asociación)
    // Ok. Ing. FMBM 03.NOV.2018 
    public function getObjectMarca(&$conn) {
          $arrayObjetos =[];
          $oxmar  = new Oxmar();
          $cOxmar = new OxmarDao(); 

          $oxmar->setMarid($this->marid);

          if($cOxmar->load($conn, $oxmar)) {
              $arrayObjetos[]=$oxmar;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo Med (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    public function getObjectMedidprog(&$conn) {
          $arrayObjetos =[];
          $med = new Med();
          $cMed = new MedDao(); 

          $med->setMedid($this->medidprog);

          if($cMed->load($conn, $med)) {
              $arrayObjetos[]=$med;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retorna un objeto de tipo Med (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    public function getObjectMedidrealiz(&$conn) {
          $arrayObjetos =[];
          $med = new Med();
          $cMed = new MedDao(); 

          $med->setMedid($this->medidrealiz);

          if($cMed->load($conn, $med)) {
              $arrayObjetos[]=$med;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type omed (Relation d'association)
    // Ok. Ing. FMBM 2018.11.05
    public function getObjectOmed(&$conn) {
          $arrayObjetos =[];
          $omed  = new Omed();
          $cOmed = new OmedDao(); 

          $omed->setOmedid($this->omedid);

          if($cOmed->load($conn, $omed)) {
              $arrayObjetos[]=$omed;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

    // Retourne un objet de type ppto (Relation d'association)
    // Ok. Ing. FMBM 2018.11.18
    public function getObjectPpto(&$conn) {
          $arrayObjetos =[];
          $ppto  = new Ppto();
          $cPpto = new PptoDao(); 

          $ppto->setPptoid($this->pptoid);

          if($cPpto->load($conn, $ppto)) {
              $arrayObjetos[]=$ppto;
              return $arrayObjetos;
          } else {
            return null;
          }
    }

}

?>