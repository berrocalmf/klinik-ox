<?php

class Oxtrat {

    /** 
     * Persistent Instance variables. This data is directly 
     * mapped to the columns of database table.
     */
    private $tratid;
    private $constrat;
    private $motivotrat;
    private $enembarazo;
    private $mesesemb;
    private $probembaraz;
    private $fechainicio;
    private $fechafin;
    private $fechacierre;
    private $feproxcontrol;
    private $observaciones;
    private $estado;
    private $fechaestado;
    private $afiid;
    private $docidafiliado;
    private $terid;
    private $idtercero;
    private $medid;
    private $tratvalor;
    private $tratvlrsaldo;
    private $indiceplaca;
    private $tratcerrado;
    private $costumerid;

    public function __construct(){}

    public function getTratid() {
          return $this->tratid;
    }
    public function setTratid($tratidIn) {
          $this->tratid = $tratidIn;
    }

    public function getConstrat() {
          return $this->constrat;
    }
    public function setConstrat($constratIn) {
          $this->constrat = $constratIn;
    }

    public function getMotivotrat() {
          return $this->motivotrat;
    }
    public function setMotivotrat($motivotratIn) {
          $this->motivotrat = $motivotratIn;
    }

    public function getEnembarazo() {
          return $this->enembarazo;
    }
    public function setEnembarazo($enembarazoIn) {
          $this->enembarazo = $enembarazoIn;
    }

    public function getMesesemb() {
          return $this->mesesemb;
    }
    public function setMesesemb($mesesembIn) {
          $this->mesesemb = $mesesembIn;
    }

    public function getProbembaraz() {
          return $this->probembaraz;
    }
    public function setProbembaraz($probembarazIn) {
          $this->probembaraz = $probembarazIn;
    }

    public function getFechainicio() {
          return $this->fechainicio;
    }
    public function setFechainicio($fechainicioIn) {
          $this->fechainicio = $fechainicioIn;
    }

    public function getFechafin() {
          return $this->fechafin;
    }
    public function setFechafin($fechafinIn) {
          $this->fechafin = $fechafinIn;
    }

    public function getFechacierre() {
          return $this->fechacierre;
    }
    public function setFechacierre($fechacierreIn) {
          $this->fechacierre = $fechacierreIn;
    }

    public function getFeproxcontrol() {
          return $this->feproxcontrol;
    }
    public function setFeproxcontrol($feproxcontrolIn) {
          $this->feproxcontrol = $feproxcontrolIn;
    }

    public function getObservaciones() {
          return $this->observaciones;
    }
    public function setObservaciones($observacionesIn) {
          $this->observaciones = $observacionesIn;
    }

    public function getEstado() {
          return $this->estado;
    }
    public function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    public function getFechaestado() {
          return $this->fechaestado;
    }
    public function setFechaestado($fechaestadoIn) {
          $this->fechaestado = $fechaestadoIn;
    }

    public function getAfiid() {
          return $this->afiid;
    }
    public function setAfiid($afiidIn) {
          $this->afiid = $afiidIn;
    }

    public function getDocidafiliado() {
          return $this->docidafiliado;
    }
    public function setDocidafiliado($docidafiliadoIn) {
          $this->docidafiliado = $docidafiliadoIn;
    }

    public function getTerid() {
          return $this->terid;
    }
    public function setTerid($teridIn) {
          $this->terid = $teridIn;
    }

    public function getIdtercero() {
          return $this->idtercero;
    }
    public function setIdtercero($idterceroIn) {
          $this->idtercero = $idterceroIn;
    }

    public function getMedid() {
          return $this->medid;
    }
    public function setMedid($medidIn) {
          $this->medid = $medidIn;
    }

    public function getTratvalor() {
          return $this->tratvalor;
    }
    public function setTratvalor($tratvalorIn) {
          $this->tratvalor = $tratvalorIn;
    }

    public function getTratvlrsaldo() {
          return $this->tratvlrsaldo;
    }
    public function setTratvlrsaldo($tratvlrsaldoIn) {
          $this->tratvlrsaldo = $tratvlrsaldoIn;
    }

    public function getIndiceplaca() {
          return $this->indiceplaca;
    }
    public function setIndiceplaca($indiceplacaIn) {
          $this->indiceplaca = $indiceplacaIn;
    }

    public function getTratcerrado() {
          return $this->tratcerrado;
    }
    public function setTratcerrado($tratcerradoIn) {
          $this->tratcerrado = $tratcerradoIn;
    }

    public function getCostumerid() {
          return $this->costumerid;
    }
    public function setCostumerid($costumeridIn) {
          $this->costumerid = $costumeridIn;
    }

    public function setAll($tratidIn,
          $constratIn,
          $motivotratIn,
          $enembarazoIn,
          $mesesembIn,
          $probembarazIn,
          $fechainicioIn,
          $fechafinIn,
          $fechacierreIn,
          $feproxcontrolIn,
          $observacionesIn,
          $estadoIn,
          $fechaestadoIn,
          $afiidIn,
          $docidafiliadoIn,
          $teridIn,
          $idterceroIn,
          $medidIn,
          $tratvalorIn,
          $tratvlrsaldoIn,
          $indiceplacaIn,
          $tratcerradoIn,
          $costumeridIn) {
          $this->tratid = $tratidIn;
          $this->constrat = $constratIn;
          $this->motivotrat = $motivotratIn;
          $this->enembarazo = $enembarazoIn;
          $this->mesesemb = $mesesembIn;
          $this->probembaraz = $probembarazIn;
          $this->fechainicio = $fechainicioIn;
          $this->fechafin = $fechafinIn;
          $this->fechacierre = $fechacierreIn;
          $this->feproxcontrol = $feproxcontrolIn;
          $this->observaciones = $observacionesIn;
          $this->estado = $estadoIn;
          $this->fechaestado = $fechaestadoIn;
          $this->afiid = $afiidIn;
          $this->docidafiliado = $docidafiliadoIn;
          $this->terid = $teridIn;
          $this->idtercero = $idterceroIn;
          $this->medid = $medidIn;
          $this->tratvalor = $tratvalorIn;
          $this->tratvlrsaldo = $tratvlrsaldoIn;
          $this->indiceplaca = $indiceplacaIn;
          $this->tratcerrado = $tratcerradoIn;
          $this->costumerid = $costumeridIn;
    }

    public function hasEqualMapping($valueObject) {

          if ($valueObject->getTratid() != $this->tratid) {
                    return(false);
          }
          if ($valueObject->getConstrat() != $this->constrat) {
                    return(false);
          }
          if ($valueObject->getMotivotrat() != $this->motivotrat) {
                    return(false);
          }
          if ($valueObject->getEnembarazo() != $this->enembarazo) {
                    return(false);
          }
          if ($valueObject->getMesesemb() != $this->mesesemb) {
                    return(false);
          }
          if ($valueObject->getProbembaraz() != $this->probembaraz) {
                    return(false);
          }
          if ($valueObject->getFechainicio() != $this->fechainicio) {
                    return(false);
          }
          if ($valueObject->getFechafin() != $this->fechafin) {
                    return(false);
          }
          if ($valueObject->getFechacierre() != $this->fechacierre) {
                    return(false);
          }
          if ($valueObject->getFeproxcontrol() != $this->feproxcontrol) {
                    return(false);
          }
          if ($valueObject->getObservaciones() != $this->observaciones) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getFechaestado() != $this->fechaestado) {
                    return(false);
          }
          if ($valueObject->getAfiid() != $this->afiid) {
                    return(false);
          }
          if ($valueObject->getDocidafiliado() != $this->docidafiliado) {
                    return(false);
          }
          if ($valueObject->getTerid() != $this->terid) {
                    return(false);
          }
          if ($valueObject->getIdtercero() != $this->idtercero) {
                    return(false);
          }
          if ($valueObject->getMedid() != $this->medid) {
                    return(false);
          }
          if ($valueObject->getTratvalor() != $this->tratvalor) {
                    return(false);
          }
          if ($valueObject->getTratvlrsaldo() != $this->tratvlrsaldo) {
                    return(false);
          }
          if ($valueObject->getIndiceplaca() != $this->indiceplaca) {
                    return(false);
          }
          if ($valueObject->getTratcerrado() != $this->tratcerrado) {
                    return(false);
          }
          if ($valueObject->getCostumerid() != $this->costumerid) {
                    return(false);
          }

          return true;
    }

    public function toString() {
        $out = "";
        $out = $out."\nclass Oxtrat, mapping to table oxtrat\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."tratid = ".$this->tratid."\n"; 
        $out = $out."constrat = ".$this->constrat."\n"; 
        $out = $out."motivotrat = ".$this->motivotrat."\n"; 
        $out = $out."enembarazo = ".$this->enembarazo."\n"; 
        $out = $out."mesesemb = ".$this->mesesemb."\n"; 
        $out = $out."probembaraz = ".$this->probembaraz."\n"; 
        $out = $out."fechainicio = ".$this->fechainicio."\n"; 
        $out = $out."fechafin = ".$this->fechafin."\n"; 
        $out = $out."fechacierre = ".$this->fechacierre."\n"; 
        $out = $out."feproxcontrol = ".$this->feproxcontrol."\n"; 
        $out = $out."observaciones = ".$this->observaciones."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."fechaestado = ".$this->fechaestado."\n"; 
        $out = $out."afiid = ".$this->afiid."\n"; 
        $out = $out."docidafiliado = ".$this->docidafiliado."\n"; 
        $out = $out."terid = ".$this->terid."\n"; 
        $out = $out."idtercero = ".$this->idtercero."\n"; 
        $out = $out."medid = ".$this->medid."\n"; 
        $out = $out."tratvalor = ".$this->tratvalor."\n"; 
        $out = $out."tratvlrsaldo = ".$this->tratvlrsaldo."\n"; 
        $out = $out."indiceplaca = ".$this->indiceplaca."\n"; 
        $out = $out."tratcerrado = ".$this->tratcerrado."\n"; 
        $out = $out."costumerid = ".$this->costumerid."\n"; 
        return $out;
    }

    public function clone() {
        $cloned = new Oxtrat();

        $cloned->setTratid($this->tratid); 
        $cloned->setConstrat($this->constrat); 
        $cloned->setMotivotrat($this->motivotrat); 
        $cloned->setEnembarazo($this->enembarazo); 
        $cloned->setMesesemb($this->mesesemb); 
        $cloned->setProbembaraz($this->probembaraz); 
        $cloned->setFechainicio($this->fechainicio); 
        $cloned->setFechafin($this->fechafin); 
        $cloned->setFechacierre($this->fechacierre); 
        $cloned->setFeproxcontrol($this->feproxcontrol); 
        $cloned->setObservaciones($this->observaciones); 
        $cloned->setEstado($this->estado); 
        $cloned->setFechaestado($this->fechaestado); 
        $cloned->setAfiid($this->afiid); 
        $cloned->setDocidafiliado($this->docidafiliado); 
        $cloned->setTerid($this->terid); 
        $cloned->setIdtercero($this->idtercero); 
        $cloned->setMedid($this->medid); 
        $cloned->setTratvalor($this->tratvalor); 
        $cloned->setTratvlrsaldo($this->tratvlrsaldo); 
        $cloned->setIndiceplaca($this->indiceplaca); 
        $cloned->setTratcerrado($this->tratcerrado); 
        $cloned->setCostumerid($this->costumerid); 

        return $cloned;
    }

}

?>