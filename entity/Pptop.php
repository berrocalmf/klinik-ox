<?php
 /**
  * Pptop Value Object.
  * Ing. FBERROCALM 2018/11/29
  */

class Pptop {

    const TABLATGE = 1;      // No. de la Tabla para las configuraciones de TGEN

    private $pptopid;
    private $pptoid;        // <-- FK (ppto)
    private $fecha;
    private $idconcepto;    // <-- Código del concepto de pago ( Cargar Descripción )
    private $detallemov;
    private $tipomov;       // <-- Tipo del movimiento (Cargar Descripción)
    private $vlrmovimiento;
    private $obs;
    private $citid;         // <-- FK (cit)
    private $customerid;    // <-- Id. Del cliente de la Aplicación (Cargar Razon social)
    private $usrid;         // <-- FK (usr)
    private $estado;        // <-- Estado del registro (Cargar Descripción)
    private $motivoAnula;

    // Array con las características adicionales del Objeto

    private $default_args = array(
      'idconcepto'     => null, 
      'tipomov'   => null, 
      'customerid' => null, 
      'estado'   => null, 
    );  

    public function __construct(){}

    public function _setProperties($conceptoIn, $descMovIn, $NomClineteIn, $descEstadoIn) {
          if ( ! is_null( $conceptoIn ) ) {
              $default_args['idconcepto'] = $conceptoIn;
          }
          if ( ! is_null( $descMovIn ) ) {
              $default_args['tipomov'] = $descMovIn; 
          }
          if ( ! is_null( $NomClineteIn ) ) {
              $default_args['customerid'] = $NomClineteIn; 
          }
          if ( ! is_null( $descEstadoIn ) ) {
              $default_args['estado'] = $descEstadoIn; 
          }  
    }

    public function _getProperty($property) {
          return $this->default_args['$property'];
    }

    public function _setProperty($property, $valueIn) {
          if ( ! is_null( $valueIn ) ) {
              return $this->default_args['$property']=$valueIn;
          }
    }    

    function getTableTge() {
        return self::TABLATGE;
    }

    function getPptopid() {
          return $this->pptopid;
    }
    function setPptopid($pptopidIn) {
          $this->pptopid = $pptopidIn;
    }

    function getPptoid() {
          return $this->pptoid;
    }
    function setPptoid($pptoidIn) {
          $this->pptoid = $pptoidIn;
    }

    function getFecha() {
          return $this->fecha;
    }
    function setFecha($fechaIn) {
          $this->fecha = $fechaIn;
    }

    function getIdconcepto() {
          return $this->idconcepto;
    }
    function setIdconcepto($idconceptoIn) {
          $this->idconcepto = $idconceptoIn;
    }

    function getDetallemov() {
          return $this->detallemov;
    }
    function setDetallemov($detallemovIn) {
          $this->detallemov = $detallemovIn;
    }

    function getTipomov() {
          return $this->tipomov;
    }
    function setTipomov($tipomovIn) {
          $this->tipomov = $tipomovIn;
    }

    function getVlrmovimiento() {
          return $this->vlrmovimiento;
    }
    function setVlrmovimiento($vlrmovimientoIn) {
          $this->vlrmovimiento = $vlrmovimientoIn;
    }

    function getObs() {
          return $this->obs;
    }
    function setObs($obsIn) {
          $this->obs = $obsIn;
    }

    function getCitid() {
          return $this->citid;
    }
    function setCitid($citidIn) {
          $this->citid = $citidIn;
    }

    function getCustomerid() {
          return $this->customerid;
    }
    function setCustomerid($customeridIn) {
          $this->customerid = $customeridIn;
    }

    function getUsrid() {
          return $this->usrid;
    }
    function setUsrid($usridIn) {
          $this->usrid = $usridIn;
    }

    function getEstado() {
          return $this->estado;
    }
    function setEstado($estadoIn) {
          $this->estado = $estadoIn;
    }

    function getMotivoAnula() {
          return $this->motivoAnula;
    }
    function setMotivoAnula($motivoAnulaIn) {
          $this->motivoAnula = $motivoAnulaIn;
    }

    function setAll($pptopidIn,
          $pptoidIn,
          $fechaIn,
          $idconceptoIn,
          $detallemovIn,
          $tipomovIn,
          $vlrmovimientoIn,
          $obsIn,
          $citidIn,
          $customeridIn,
          $usridIn,
          $estadoIn,
          $motivoAnulaIn) {
          $this->pptopid = $pptopidIn;
          $this->pptoid = $pptoidIn;
          $this->fecha = $fechaIn;
          $this->idconcepto = $idconceptoIn;
          $this->detallemov = $detallemovIn;
          $this->tipomov = $tipomovIn;
          $this->vlrmovimiento = $vlrmovimientoIn;
          $this->obs = $obsIn;
          $this->citid = $citidIn;
          $this->customerid = $customeridIn;
          $this->usrid = $usridIn;
          $this->estado = $estadoIn;
          $this->motivoAnula = $motivoAnulaIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getPptopid() != $this->pptopid) {
                    return(false);
          }
          if ($valueObject->getPptoid() != $this->pptoid) {
                    return(false);
          }
          if ($valueObject->getFecha() != $this->fecha) {
                    return(false);
          }
          if ($valueObject->getIdconcepto() != $this->idconcepto) {
                    return(false);
          }
          if ($valueObject->getDetallemov() != $this->detallemov) {
                    return(false);
          }
          if ($valueObject->getTipomov() != $this->tipomov) {
                    return(false);
          }
          if ($valueObject->getVlrmovimiento() != $this->vlrmovimiento) {
                    return(false);
          }
          if ($valueObject->getObs() != $this->obs) {
                    return(false);
          }
          if ($valueObject->getCitid() != $this->citid) {
                    return(false);
          }
          if ($valueObject->getCustomerid() != $this->customerid) {
                    return(false);
          }
          if ($valueObject->getUsrid() != $this->usrid) {
                    return(false);
          }
          if ($valueObject->getEstado() != $this->estado) {
                    return(false);
          }
          if ($valueObject->getMotivoAnula() != $this->motivoAnula) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Pptop, mapping to table pptop\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."pptopid = ".$this->pptopid."\n"; 
        $out = $out."pptoid = ".$this->pptoid."\n"; 
        $out = $out."fecha = ".$this->fecha."\n"; 
        $out = $out."idconcepto = ".$this->idconcepto."\n"; 
        $out = $out."detallemov = ".$this->detallemov."\n"; 
        $out = $out."tipomov = ".$this->tipomov."\n"; 
        $out = $out."vlrmovimiento = ".$this->vlrmovimiento."\n"; 
        $out = $out."obs = ".$this->obs."\n"; 
        $out = $out."citid = ".$this->citid."\n"; 
        $out = $out."customerid = ".$this->customerid."\n"; 
        $out = $out."usrid = ".$this->usrid."\n"; 
        $out = $out."estado = ".$this->estado."\n"; 
        $out = $out."motivoAnula = ".$this->motivoAnula."\n"; 
        return $out;
    }

   function clone() {
        $cloned = new Pptop();
        $cloned->setPptopid($this->pptopid); 
        $cloned->setPptoid($this->pptoid); 
        $cloned->setFecha($this->fecha); 
        $cloned->setIdconcepto($this->idconcepto); 
        $cloned->setDetallemov($this->detallemov); 
        $cloned->setTipomov($this->tipomov); 
        $cloned->setVlrmovimiento($this->vlrmovimiento); 
        $cloned->setObs($this->obs); 
        $cloned->setCitid($this->citid); 
        $cloned->setCustomerid($this->customerid); 
        $cloned->setUsrid($this->usrid); 
        $cloned->setEstado($this->estado); 
        $cloned->setMotivoAnula($this->motivoAnula); 
        return $cloned;
    }

    // Relations with tables Many to one

    // Table [cit]
    // Retorna un objeto de tipo Cit (Relación de asociación)
    // Ok. Ing. FMBM 02.NOV.2018 
    
    public function getObjectCit(&$conn) {
          $arrayObjetos = [];
          $cit = new Cit();
          $cCit = new CitDao(); 

          $cit->setCitid($this->citid);

          if( $cCit->load($conn, $cit) ) {
              $arrayObjetos[]=$cit;
              return $arrayObjetos;
          } else {
              return null;
          }
    }

    // Table [ppto]
    // Retourne un objet de type ppto (Relation d'association)
    // Ok. Ing. FMBM 2018.11.18
    
    public function getObjectPpto(&$conn) {
          $arrayObjetos = [];
          $ppto  = new Ppto();
          $cPpto = new PptoDao(); 

          $ppto->setPptoid($this->pptoid);

          if($cPpto->load($conn, $ppto)) {
              $arrayObjetos[]=$ppto;
              return $arrayObjetos;
          } else {
              return null;
          }
    }

}

?>