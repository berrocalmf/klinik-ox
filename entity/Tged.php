<?php

 /**
  * Tged Value Object.
  * Ing. FBERROCALM - 30/11/2018
    Reviewed: Ok. Ing. FMBM 2018/12/20 10:37  
  */

class Tged {
   
    private $tgedid;
    private $tgeid;
    private $campo;
    private $codigo;
    private $descripcion;

    public function __construct(){}

    function getTgedid() {
          return $this->tgedid;
    }
    function setTgedid($tgedidIn) {
          $this->tgedid = $tgedidIn;
    }

    function getTgeid() {
          return $this->tgeid;
    }
    function setTgeid($tgeidIn) {
          $this->tgeid = $tgeidIn;
    }

    function getCampo() {
          return $this->campo;
    }
    function setCampo($campoIn) {
          $this->campo = $campoIn;
    }

    function getCodigo() {
          return $this->codigo;
    }
    function setCodigo($codigoIn) {
          $this->codigo = $codigoIn;
    }

    function getDescripcion() {
          return $this->descripcion;
    }
    function setDescripcion($descripcionIn) {
          $this->descripcion = $descripcionIn;
    }

    function setAll($tgedidIn,
          $tgeidIn,
          $campoIn,
          $codigoIn,
          $descripcionIn) {
          $this->tgedid = $tgedidIn;
          $this->tgeid = $tgeidIn;
          $this->campo = $campoIn;
          $this->codigo = $codigoIn;
          $this->descripcion = $descripcionIn;
    }

    function hasEqualMapping($valueObject) {

          if ($valueObject->getTgedid() != $this->tgedid) {
                    return(false);
          }
          if ($valueObject->getTgeid() != $this->tgeid) {
                    return(false);
          }
          if ($valueObject->getCampo() != $this->campo) {
                    return(false);
          }
          if ($valueObject->getCodigo() != $this->codigo) {
                    return(false);
          }
          if ($valueObject->getDescripcion() != $this->descripcion) {
                    return(false);
          }

          return true;
    }

    function toString() {
        $out = "";
        $out = $out."\nclass Tged, mapping to table tged\n";
        $out = $out."Persistent attributes: \n"; 
        $out = $out."tgedid = ".$this->tgedid."\n"; 
        $out = $out."tgeid = ".$this->tgeid."\n"; 
        $out = $out."campo = ".$this->campo."\n"; 
        $out = $out."codigo = ".$this->codigo."\n"; 
        $out = $out."descripcion = ".$this->descripcion."\n"; 
        return $out;
    }

    function clone() {
        $cloned = new Tged();
        $cloned->setTgedid($this->tgedid); 
        $cloned->setTgeid($this->tgeid); 
        $cloned->setCampo($this->campo); 
        $cloned->setCodigo($this->codigo); 
        $cloned->setDescripcion($this->descripcion); 
        return $cloned;
    }

}

?>