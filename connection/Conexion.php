<?php
class Conexion {

	private $conexion;
	private $server;
	private $user;
	private $password;
	private $dataBase;

	public function __construct() {
		
		$this->server   ="localhost";
		$this->user     ="root";
		$this->password ="M4st3r99";
		$this->dataBase ="klinike";

		try {
			$this->conexion=new PDO('mysql:host='.$this->server.';dbname='.$this->dataBase.';charset=utf8', $this->user, $this->password);
			$this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch(PDOException $ex) {
			echo $ex->getMessage();
		}

	}

	// Retorna la conexión establecida
	public function getConexion() {
		return $this->conexion;
	}

	// Cierra la conexión establecida
	public function close_conn() {
		$this->conexion = null;
	}

}
?>