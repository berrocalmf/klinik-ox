// Cambia el estilo (CSS) de los Div diente, al pasar el mouse sobre su caja de texto
function hoverTxtDiente(idTxtDiente) {
	var idDiente = idTxtDiente.substring(3, 6);
	var css = {
		"box-shadow": "0px 0px 10px blue"
	}
	$("#"+idDiente).css(css);
}

// Se deja el estilo (CSS) por defeto de los Div diente, al momento de retirar el mouse de la caja de texto relacionada
function outTxtDiente(idTxtDiente) {
	var idDiente = idTxtDiente.substring(3, 6);
	var css = {
		"box-shadow": "none"
	}
	$("#"+idDiente).css(css);
}

// Al dar click sobre una cara del diente general, se selecciona y se muestra en la caja de texto
function seleccionarCara(idCaraDiente) {
	$("#txtCaraTratada").val(idCaraDiente);
}

// Al dar clic sobre el link de la pieza dental, su identificador se muestra en el formulario de registro de estados dentales (diente General)
function seleccionarDiente(idDiente) {
	$("#txtIdentificadorDienteGeneral").val(idDiente);
	$("#txtDienteTratado").val(idDiente);
}

// ===========================================================================
// Agrega un tratamiento: En una cara del diente, un Estado determinado.
// Agrega una marca al diente seleccionado.  Agrega el registro en la tabla de
// tratamientos o estados indicados por el odontólogo.
// ===========================================================================
// Modificado 16.Oct.2018 guardar en tablas ox (Ing. FMBM)

function agregarTratamiento(diente, cara, estado) {
	
	if(diente == "" || cara == "") {
			alert("Debe seleccionar el diente y la cara de dicho diente para agregar un Tratamiento");
			return;
	}

	if(estado == "") {
			alert("Debe seleccionar el Estado o Marca para agregar!");
			return;	
	}

	var agregarFila = true;

	$("#tablaTratamiento").find("tr").each( function(index, elemento) {
	    	var dienteAsignado;
	    	var caraSel;

	    	if(!agregarFila) {
	    		return false;
	    	}

	        $(elemento).find("td").each(function(index2, elemento2) {        	
		        	if(index2 == 0) {
		        		dienteAsignado=$(elemento2).text();
		        	}

		        	if(index2 == 1) {
		        		caraSel=$(elemento2).text();	
		        	}
		    		
		        	switch(index2) {
		        		case 2:
		        			// var partesEstado=$(elemento2).text().split("-");
		        			var partesEstado=$(elemento2).text();
      
		        			// Para Marcas que sean únicas :
		        			// if(partesEstado[0]!="15" && partesEstado[0]!="16" && partesEstado[0]!="17" && partesEstado[0]!="18") {		        			
		        			// if(partesEstado!="15" && partesEstado!="16" && partesEstado!="17" && partesEstado!="18") {	
		        			// 	// if((partesEstado[1]==estado.split("-")[1]) && dienteAsignado==diente) {
		        			// 	if((partesEstado==estado) && dienteAsignado==diente) {	
		        			// 		alert("El tratamiento ya fue asignado");
		        			// 		agregarFila=false;
		        			// 	}
		        			// } else {
		        			// 	// Para marcas que no sean únicas, pero, la cara seleccionada debe ser única
		        			// 	if((partesEstado[1]==estado.split("-")[1]) && dienteAsignado==diente && caraSel==cara){
		        			// 		alert("La Marca ya fue asignada.");
		        			// 		agregarFila=false;
		        			// 	}
		        			// }

		        			if((partesEstado == estado) && dienteAsignado == diente && caraSel == cara){
		        					alert("La Marca ya fue asignada.");
		        					agregarFila=false;
		        			}
		        			break;
		        	}
	        });
    });

	// [ Agregando una fila a tabla html desde Javascript ]
	if(agregarFila) {
			var filaHtml="<tr><td>"+diente+"</td><td>"+cara+"</td><td>"+estado+"</td><td class='widthEditarTable'><input type='button' class='btn btn-primary' value='Eliminar' onclick='quitarTratamiento(this);'></td></tr>";
			$("#tablaTratamiento > tbody").append(filaHtml);
			$("#divTratamiento").scrollTop($("#tablaTratamiento").height()); // Desplaza la tabla hasta la parte superior
	}
}

// Elimina un tratamiento de la tabla de tratamientos seleccioandos por el 
// Odontólogo que aun no han sido guardados en la Base de Datos.  (Tabla en memoria RAM)
function quitarTratamiento(elemento) {
	$(elemento).parent().parent().remove();
}

// Función de tipo callback.  Carga los valores necesarios para realizar el registro de los tratamientos en la Base de Datos.
function recuperarDatosTratamiento(callback) {
		var codigoPaciente;
		var estados = "";
		var descripcion;

		codigoPaciente = $("#txtCodigoPaciente").val();

		$("#tablaTratamiento").find("tr").each(function(index, elemento) {
	        $(elemento).find("td").each(function(index2, elemento2) {
	        	estados += $(elemento2).text()+"_";
	        });
	    });

	    descripcion = $("#txtDescripcion").val();
	    estados     = estados.substring(0, estados.length-2);

	    callback(codigoPaciente, estados, descripcion);
}

// Función de tipo callback.  Recoge datos de las marcas de la interfaz HTML.
function recuperarDatosMarcas(callback) {
	var idOdontograma = 0;
	var estados       = "";
	var caras         = "";
	var codDiente     = "";
	var descripcion   = "";

	idOdontograma=$("#txtCodigoOdx").val();

	$("#tablaTratamiento").find("tr").each(function(index, elemento) {
        $(elemento).find("td").each(function(index2, elemento2) {
        	if(index2==1) {
        		caras+=$(elemento2).text()+"_";	
        	}
        	if(index2==2) {
        		estados+=$(elemento2).text()+"_";
        	}
        	
        });
    });

	codDiente   = $("#txtDienteTratado").val();
    codDiente   = codDiente.substring(1, codDiente.length); // substr(codDiente,-2);
    descripcion = $("#txtDescripcion").val();
    
    callback(idOdontograma, estados, descripcion, codDiente, caras);
}

// Función para tomar los datos del Odontograma del formulario HTML para el registro del tratamiento.
function recogerDatosOdontograma(callback) {
		var medid;
		var tratid;
		var afiid;
		var hcpoxid;
		var consecutivo;
		var denttemporal;
		var indiceCPOD;
		var indiceCEOD;
		var customerid;

		medid        = $("#medid").val();
		tratid       = $("#tratid").val();
		afiid        = $("#afiid").val();
		hcpoxid      = $("#hcpoxid").val();
		consecutivo  = $("#consecutivo").val();
		denttemporal = $("#denttemporal").val();
		indiceCPOD   = $("#indiceCPOD").val();
		indiceCEOD   = $("#indiceCEOD").val();
		customerid   = $("#customerid").val();

	    callback(medid, tratid, afiid, hcpoxid, consecutivo, denttemporal, indiceCPOD, indiceCEOD, customerid);
}

// =================================================================================================
// Guardar tratamiento: realiza el registro del tratamiento en la base de datos en la tabla oxodon.
// =================================================================================================
function guardarTratamiento() {
	recuperarDatosTratamiento(function(codigoPaciente, estados, descripcion) {
		if(estados=="") {
			alert("Ud. debe agregar algún Tratamiento");
			return;
		}

		$.post(
			"registrar.php",
			{
				codigoPaciente: codigoPaciente,
				estados: estados,
				descripcion: descripcion
			}, 
			function(pagina) {
				limpiarDatosTratamiento();
				$("#seccionPaginaAjax").html(pagina);
				setTimeout(function()
				{
					$("#seccionPaginaAjax").html("");
				}, 7000);
	        }
		).done(function(){
	    	 cargarTratamientos('seccionTablaTratamientos', 'verodontograma.php', codigoPaciente); 
	    	 cargarDientes('seccionDientes', 'dientes.php', '', codigoPaciente);
	    });

	});
}

// ==================================================================================================
// Guardar Marcas: realiza el registro de las marcas dentales en la base de datosen la tabla oxodond.
// ==================================================================================================
function guardarMarcas() {
	recuperarDatosMarcas(function(idOdontograma, estados, descripcion , codDiente, caras) {		
			if(estados=="") {
				alert("Debe agregar alguna Marca Odontológica.");
				return;
			}
			if(idOdontograma==0 || idOdontograma=="") {
				alert("Debe seleccionar un Odontograma.");
				return;
			}
			if(codDiente=="") {
				alert("Debe seleccionar una Pieza Dental.");
				return;
			}
			$.post("registrarEstados.php",
		    {
		    	idOdontograma: idOdontograma,
		    	estados: estados,
		    	descripcion: descripcion,
		    	codDiente: codDiente,
		    	caras: caras
		    }, 
		    function(pagina)
		    {
		    	limpiarDatosTratamiento();
		    	$("#seccionPaginaAjax").html(pagina);
		    	setTimeout(function()
		    	{
		    		$("#seccionPaginaAjax").html("");
		    	}, 7000);
		    }).done(function(){
		    	cargarTratamientos('seccionTablaTratamientos', 'verodontograma.php', idOdontograma); 
		    	cargarDientes('seccionDientes', 'dientes.php', '', idOdontograma);
		    });
	});
}

// ==================================================================================================
// Guardar tratamiento: realiza el registro del tratamiento en la base de datos en la tabla oxodon.
// ==================================================================================================
function crearOdontograma() {
		recogerDatosOdontograma(function(medid,tratid,afiid,hcpoxid,consecutivo,denttemporal,indiceCPOD,indiceCEOD,customerid) {
		
		if(consecutivo=="") {
			alert("Consecutivo de HC no especificado.");
			return;
		}
		$.post("registrarOx.php",
	    {
	    	medid: medid,
			tratid: tratid,
			afiid: afiid,
			hcpoxid: hcpoxid,
			consecutivo: consecutivo,
			denttemporal: denttemporal,
			indiceCPOD: indiceCPOD,
			indiceCEOD: indiceCEOD,
			customerid: customerid
	    }, 
	    function(pagina)
	    {
	    	limpiarFormOdontograma();
	    	$("#seccionPaginaAjax").html(pagina);

	    }).done(function(){
	    });
	});
}

// Limpia la forma para registro de Marcas Odontológicas
function limpiarDatosTratamiento() {
		$("#txtIdentificadorDienteGeneral").val("DXX");
		$("#txtDienteTratado").val("");
		$("#txtCaraTratada").val("");
		$("#txtDescripcion").val("");
		$("#tablaTratamiento").find("tr").each(function(index, row) {
				$(row).remove();
		});
}

// Función para limpiar la interfaz (Temporal)
function limpiarFormOdontograma() {
		$("#medid").val("");
		$("#tratid").val("");
		$("#afiid").val("");
		$("#hcpoxid").val("");
		$("#consecutivo").val("");
		$("#denttemporal").val("");
		$("#indiceCPOD").val("");
		$("#indiceCEOD").val("");
		$("#customerid").val("");
}

// Muestra en la pantalla la página del Odontograma
function cargarDientes(idSeccion, url, estados, codigoOdx) {
	$.ajax(
    {
    	async: true,	
        url: url,
        type: "POST",
        data: {codigoOdx: codigoOdx, estados: estados},
        cache: true
    }).done(function(pagina) 
    {
        $("#"+idSeccion).html(pagina);
    });
}

// Muestra en pantalla el listado de los estados definidos para el Odontograma del Paciente. 
function cargarTratamientos(idSeccion, url, codigoOdx) {	
	$.ajax(
    {
    	async: true,	
        url: url,
        type: "POST",
        data: {codigoOdx: codigoOdx},
        cache: true
    }).done(function(pagina) 
    {
        $("#"+idSeccion).html(pagina);
    });
}

function cargarProcedimientos(idSeccion, accion, url, codigoOdx, codPpto) {
	if(idSeccion=="") {
		alert("Sección no especificado.");
		return;
	}
	if(accion=="") {
		alert("Acción no especificada.");
		return;
	}
	if(url=="") {
		alert("Archivo PHP no especificado.");
		return;
	}
	if(codigoOdx=="" || codigoOdx==0) {
		alert("Id. de Odontograma no especificado.");
		return;
	}
	if(codPpto=="" || codPpto==0) {
		alert("Id. de Presupuesto sin valor.");
		return;
	}
	$.ajax(
    {
    	async: true,	
        url: url,
        type: "POST",
        data: {codigoOdx: codigoOdx, accion: accion, codPpto: codPpto},
        cache: true
    }).done(function(pagina) 
    {
        $("#"+idSeccion).html(pagina);
    });
}

// Prepara la impresión
function prepararImpresion() {
	$("body #seccionTablaTratamientos").css({"display": "none"});
	$("body #seccionRegistrarTratamiento").css({"display": "none"});
}

// Termina la Impresión
function terminarImpresion() {
	$("body #seccionTablaTratamientos").css({"display": "inline-block"});
	$("body #seccionRegistrarTratamiento").css({"display": "inline-block"});
}

// Muestra en la pantalla la página del Odontograma
function cargarOdontograma(idSeccion, url, idOdontograma, codigoPaciente, idAtencion) {
	 $.ajax({
	 	 async: true,
         url: url,
         type: "POST",
         data: {codigoPaciente: codigoPaciente, idOdontograma: idOdontograma, idAtencion: idAtencion},
         cache: true
     }).done(function(pagina) {
      	$("#"+idSeccion).html(pagina);
     });

	 // Obs: No se está controlando el error
}
